# Team 11 OOP Teamwork - Task Managment System


## Getting started

This is very mazohistic and even more sadistic Task Management System that has some new ways to f*ck up your life if you try to use it. Enjoy! 

## How to use commands - Creation Commands

- [ ] CREATEPERSON [name]

  Creates person; requires 1 parameter 
  * name; must be unique; spaces are allowed by entagling with {{   }} symbols 
```
CREATEPERSON PRaykov
Person "PRaykov" with ID 1 was created.
CREATEPERSON PRaykov
Person with name "PRaykov" already exists! Choose different name!
CREATEPERSON P Raykov
Invalid number of arguments. Expected: 1, Received: 2
```
***
- [ ] CREATETEAM [teamName]

  Creates team; requires 1 parameter 
  * teamName; must be unique; spaces are allowed by entagling with {{   }} symbols 
```
CREATETEAM PRaykovTeam
Team "PRaykovTeam" with TeamID 1 was created.
CREATETEAM PRaykovTeam
Team with name "PRaykovTeam" already exists! Choose different name!
CREATETEAM PRaykov Team
Invalid number of arguments. Expected: 1, Received: 2
```
***

- [ ] CREATEBOARD [BoardName BelongsToTeamName]

  Creates team; requires 2 parameters 
  
  * BoardName; must be unique for each team; spaces are allowed by entagling with {{   }} symbols 
  * BelongsToTeamName; to what team should this board be added
```
CREATEBOARD Telerissue TeamPRaykovv
Board with name "Telerissue" and BoardID 1 was created.
CREATEBOARD Telerissue TeamPRaykovv
Board with name "Telerissue" already exists for team with name TeamPRaykovv! Choose different name!
CREATEBOARD Telerissue TeamChavo
Board with name "Telerissue" and BoardID 2 was created.

```
***

- [ ] CREATEFEEDBACK [title description rating belongsToBoard]

  Creates feedback on board; requires 4 parameters:

  * title; spaces are allowed by entagling with {{   }} symbols
  * description; spaces are allowed by entagling with {{   }} symbols, so just you know the functionality is really f*cked up
  * rating; from 0 to 10; spaces are allowed by entagling with {{   }} symbols
  * belongsToBoard; to what board should this feedback be added; spaces are allowed by entagling with {{   }} symbols
  
```
CREATEFEEDBACK TelerikFeedback Spliiting_by_spaces_is_real_pain_in_the_arse 9 Telerissue
Feedback "TelerikFeedback" with rating 9 and FeedbackID 1 was created on Board "Telerissue"
```

***

- [ ] CREATESTORY [title description priority size status belongsToBoard]

  Creates feedback on board; requires 6 parameters:

  * title; spaces are allowed by entagling with {{   }} symbols
  * description; spaces are allowed by entagling with {{   }} symbols, so just you know the functionality is really f*cked up
  * priority;can be  High, Medium or Low
  * size;can be Large, Medium or Small.
  * status; can be Not Done, InProgress or Done
  * belongsToBoard; to what board should this feedback be added; spaces are allowed by entagling with {{   }} symbols
  
```
CREATESTORY GiveUsBetterWorkshops {{Give us better workshops to copy from please we are struggling!For real!}} HIGH LARGE INPROGRESS PRaykov TeleriboardK
Story with StoryID 1, Priority "HIGH" was created on board "TeleriboardK".
```

***
- [ ] CREATEBUG [title description priority severity status stepsToReproduce belongsToBoard]

  Creates feedback on board; requires 7 parameters:

  * title; spaces are allowed by entagling with {{   }} symbols
  * description; spaces are allowed by entagling with {{   }} symbols, so just you know the functionality is really f*cked up
  * priority;can be  High, Medium or Low
  * severity;can be Critical, Major or Minor
  * status; can be Active or Fixed
  * stepsToReproduce; should contain steps to reproduce the bug; spaces are allowed by entagling with {{   }} symbols
  * belongsToBoard; to what board should this feedback be added; spaces are allowed by entagling with {{   }} symbols
  
```
CREATEBUG JoinedTelerikAcademy {{Joined the academy and got insufficient knowledge but not yet removed!Wai?}} HIGH CRITICAL ACTIVE 1.Join_Telerik_again;2.Trainers_fail_to_teach_again;3.Recieve_inssuficient_again; TeleriboardK
Bug "JoinedTelerikAcademy" with BugID 1 was created on board "TeleriboardK".
```

***
- [ ] CREATECOMMENT [target description]

  Creates comment for feedback/story/bug; requires user to be logged in; requires 2 parameters:

  * target; target name of feedback/story/bug;
  * description; spaces are allowed by entagling with {{   }} symbols, so just you know the functionality is really f*cked up
  
```
CREATEPERSON Chavo
Person "Chavo" with ID 1 was created.
####################
CREATECOMMENT TelerikFeedback {{We should really speak for this comment stuff}}
There is no feedback/story/bug named "TelerikFeedback"!
####################
CREATEFEEDBACK TelerikFeedback {{Some generic description}} 9 TelerikBoard
Feedback "TelerikFeedback" with rating 9 and FeedbackID 3 was created on Board "TelerikBoard"
####################
CREATECOMMENT TelerikFeedback {{We should really speak for this comment stuff}}
There is no logged in user.
####################
LOGIN Chavo
You are now interacting as Chavo!
####################
CREATECOMMENT TelerikFeedback {{We should really speak for this comment stuff}}
Chavo added comment successfully to feedback with name "TelerikFeedback"!;
####################
SHOWALLFEEDBACK
[####################] 100% complete retrieving data
---- Feedback Entity Listing ----
FeedbackID:           3
Feedback title:       TelerikFeedback
Feedback rating:      9
Feedback description: Some generic description
Feedback status:      New
Belongs to board:     TelerikB
*** Comment for this Feedback *** 
Comment author:       ---- Person ----
PersonID:    1
Person name: Chavo

Comment content:      We should really speak for this comment stuff
************************************
####################
```

***

## How to use commands - Add Commands

- [ ] ADDPERSONTOTEAM [teamName personName]

  Creates feedback on board; requires 2 parameters:

  * teamName; name of the team to be added to, should exist
  * personName; name of the person to be added, should exist
  
```
CREATEPERSON PRaykov
Person "Chavo" with ID 1 was create
CREATEPERSON Chavo
Person "Chavo" with ID 2 was created.
CREATETEAM Telerik
Team "Telerik" with TeamID 1 was created.
ADDPERSONTOTEAM Telerik Chavo
```

***

## How to use commands - Show Commands

- [ ] SHOWALLPERSONS

  Shows list of all registered persons in the system; does not require any parameter 
  
```
CREATEPERSON PRaykov
Person "PRaykov" with ID 1 was created.
CREATEPERSON Chavo
Person "Chavo" with ID 2 was created.
CREATEPERSON Elitsa
Person "Elitsa" with ID 3 was created.
SHOWALLPERSONS
[####################] 100% complete retrieving data
---- Person ----
PersonID:    1
Person name: PRaykov

#################### 

---- Person ----
PersonID:    2
Person name: Chavo

#################### 

---- Person ----
PersonID:    3
Person name: Elitsa
```

***

- [ ] SHOWALLFEEDBACKSORTEDBYRATING

  Shows list of all feedbacks sorted by rating in the system; does not require any parameter 
  
```
CREATEFEEDBACK TelerikFeedback Spliiting_by_spaces_is_real_pain_in_the_arse 9 Telerissue
Feedback "TelerikFeedback" with rating 9 and FeedbackID 1 was created on Board "Telerissue"
####################
CREATEFEEDBACK SplitterFeedback {{Spliiting by spaces is now easier}} 2 Telerissue
Feedback "SplitterFeedback" with rating 2 and FeedbackID 2 was created on Board "Telerissue"
####################
CREATEFEEDBACK HappyFeedback {{Adapting parts from other wokshops is really fun}} 5 Telerissue
Feedback "HappyFeedback" with rating 5 and FeedbackID 3 was created on Board "Telerissue"
####################
SHOWALLFEEDBACKSORTEDBYRATING
[####################] 100% complete retrieving data
---- Feedback Entity Listing ----
FeedbackID:           2
Feedback title:       SplitterFeedback
Feedback rating:      2
Feedback description: Spliiting by spaces is now easier
Feedback status:      New
Belongs to board:     Telerissue

#################### 

---- Feedback Entity Listing ----
FeedbackID:           3
Feedback title:       HappyFeedback
Feedback rating:      5
Feedback description: Adapting parts from other wokshops is really fun
Feedback status:      New
Belongs to board:     Telerissue

#################### 

---- Feedback Entity Listing ----
FeedbackID:           1
Feedback title:       TelerikFeedback
Feedback rating:      9
Feedback description: Spliiting_by_spaces_is_real_pain_in_the_arse
Feedback status:      New
Belongs to board:     Telerissue

####################
```

- [ ] SHOWALLFEEDBACKSORTEDBYTITLE

  Shows list of all feedbacks sorted by title in the system; does not require any parameter 
  
```
CREATEFEEDBACK TelerikFeedback Spliiting_by_spaces_is_real_pain_in_the_arse 9 Telerissue
Feedback "TelerikFeedback" with rating 9 and FeedbackID 1 was created on Board "Telerissue"
####################
CREATEFEEDBACK SplitterFeedback {{Spliiting by spaces is now easier}} 2 Telerissue
Feedback "SplitterFeedback" with rating 2 and FeedbackID 2 was created on Board "Telerissue"
####################
CREATEFEEDBACK HappyFeedback {{Adapting parts from other wokshops is really fun}} 5 Telerissue
Feedback "HappyFeedback" with rating 5 and FeedbackID 3 was created on Board "Telerissue"
####################
SHOWALLFEEDBACKSORTEDBYTITLE
[####################] 100% complete retrieving data
---- Feedback Entity Listing ----
FeedbackID:           2
Feedback title:       SplitterFeedback
Feedback rating:      2
Feedback description: Spliiting by spaces is now easier
Feedback status:      New
Belongs to board:     Telerissue

#################### 

---- Feedback Entity Listing ----
FeedbackID:           3
Feedback title:       HappyFeedback
Feedback rating:      5
Feedback description: Adapting parts from other wokshops is really fun
Feedback status:      New
Belongs to board:     Telerissue

#################### 

---- Feedback Entity Listing ----
FeedbackID:           1
Feedback title:       TelerikFeedback
Feedback rating:      9
Feedback description: Spliiting_by_spaces_is_real_pain_in_the_arse
Feedback status:      New
Belongs to board:     Telerissue

####################
```

***

- [ ] LISTALLSTORYHISTORY

  Shows list of all history stored in the system sorted by time; does not require any parameter 
  
```
LISTALLBUGHISTORY
[####################] 100% complete retrieving data
[09-май-2023 04:42:39] Story: Story with StoryID 6, Priority "HIGH" was created on board "TeleriB".

[09-май-2023 04:43:20] Story: Story "GiveUsBetterWorkshops" was assigned to PRaykov!
#################### 

[09-май-2023 04:43:20] Bug: Bug "Buggy buggidy Bug" was assigned to PRaykov!
#################### 

[09-май-2023 04:44:51] Story: Story "GiveUsBetterWorkshops" was assigned to Not Assigned!
####################
```

***


- [ ] LISTALLBUGHISTORY

  Shows list of all history stored, related to bugs, in the system sorted by time; does not require any parameter 
  
```
LISTALLBUGHISTORY

[09-май-2023 04:43:20] Bug: Bug "Buggy buggidy Bug" was assigned to PRaykov!
#################### 
```

***

- [ ] LISTALLFEEDBACKHISTORY

  Shows list of all history stored, related to stories, in the system sorted by time; does not require any parameter 
  
```
LISTALLFEEDBACKHISTORY
[####################] 100% complete retrieving data
History of feedbacks is empty
####################
```

***

- [ ] LISTALLSTORYHISTORY

  Shows list of all history stored, related to stories, in the system sorted by time; does not require any parameter 
  
```
LISTALLSTORYHISTORY
[####################] 100% complete retrieving data
[09-май-2023 04:42:39] Story: Story with StoryID 6, Priority "HIGH" was created on board "TeleriB".
#################### 

[09-май-2023 04:43:20] Story: Story "GiveUsBetterWorkshops" was assigned to PRaykov!
#################### 

[09-май-2023 04:44:51] Story: Story "GiveUsBetterWorkshops" was assigned to Not Assigned!
####################
```

***




## How to use commands - Set Commands

- [ ] FEEDBACKSETRATING [feedbackName feedbackRating]

  Sets a new rating for feeback; requires 2 parameters:

  * feedbackName; the name of the feedback you want to change rating for(Why? Whyyyyyyyy? Why will you want to change it?!? Just leave it alone, ffs)
  * feedbackRating; new rating to be set; integer from 0 to 10; if input for rating is more than 10 it will counted as 10 and if less than 0 it will be counted as 0;

```
CREATEFEEDBACK TelerikFeedback Spliiting_by_spaces_is_real_pain_in_the_arse 9 Telerissue
Feedback "TelerikFeedback" with rating 9 and FeedbackID 3 was created on Board "Telerissue"
FEEDBACKSETRATING TelerikFeedback 5
Rating 5 for Feedback "TelerikFeedback" with FeedbackID 3 was set.
```

***

- [ ] FEEDBACKSETSTATUS [feedbackName feedbackStatus]

  Sets a new rating for feeback; requires 2 parameters:

  * feedbackName; the name of the feedback you want to change Status for(Damn it, Кольо, not again with this changing...)
  * feedbackStatus; set one of the following: NEW, UNSCHEDULED, SCHEDULED, DONE
              
```
CREATEFEEDBACK TelerikFeedback Spliiting_by_spaces_is_real_pain_in_the_arse 9 Telerissue
Feedback "TelerikFeedback" with rating 9 and FeedbackID 4 was created on Board "Telerissue"
FEEDBACKSETSTATUS TelerikFeedback DONE
Status DONE for Feedback "TelerikFeedback" with FeedbackID 4 was set.
```

***

- [ ] SETSTORYPRIORITY [storyName storyPriority]

  Sets a new priority for story; requires 2 parameters:

  * storyName; the name of the story you want to change status for (We are still changing? Srsly?!?)
  * storyPriority; set one of the following: HIGH, MEDIUM, LOW
  
```
CREATESTORY GiveUsBetterWorkshops {{Give us better workshops to copy from please we are struggling!For real!}} HIGH LARGE INPROGRESS PRaykov TeleriboardK
Story with StoryID 1, Priority "HIGH" was created on board "TeleriboardK".
SETSTORYPRIORITY GiveUsBetterWorkshops LOW
Story with StoryID 1 changed priority to Low
```

***


- [ ] SETSTORYSIZE [storyName storySize]

  Sets a new size for story; requires 2 parameters:

  * storyName; the name of the story you want to change status for (Oh, come on...)
  * storySize; set one of the following: LARGE, MEDIUM, SMALL
  
```
CREATESTORY GiveUsBetterWorkshops {{Give us better workshops to copy from please we are struggling!For real!}} HIGH LARGE INPROGRESS PRaykov TeleriboardK
Story with StoryID 1, Priority "HIGH" was created on board "TeleriboardK".
SETSTORYSIZE GiveUsBetterWorkshops SMALL
Size for Story with StoryID 1 was set to Small.
```

***

- [ ] SETSTORYSTATUS [storyName storyStatus]

  Sets a new status for story; requires 2 parameters:

  * storyName; the name of the story you want to change status for (Loosing hope here...)
  * storyStatus; set one of the following: NOTDONE, INPROGRESS, DONE
  
```
CREATESTORY GiveUsBetterWorkshops {{Give us better workshops to copy from please we are struggling!For real!}} HIGH LARGE INPROGRESS PRaykov TeleriboardK
Story with StoryID 1, Priority "HIGH" was created on board "TeleriboardK".
SETSTORYSTATUS GiveUsBetterWorkshops NOTDONE
Status for Story with StoryID 1 was set to "Not Done".
```

***

- [ ] BUGSETPRIORITY [bugName BugPriority]

  Sets a new status for bug; requires 2 parameters:

  * bugName; the name of the story you want to change status for (This is officially sadistic!)
  * BugPriority; set one of the following: HIGH, MEDIUM, LOW
  
```
CREATEBUG JoinedTelerikAcademy {{Joined the academy and got insufficient knowledge but not yet removed!Wai?}} ? HIGH CRITICAL ACTIVE PRaykov 1.Join_Telerik_again;2.Trainers_fail_to_teach_again;3.Recieve_inssuficient_again; TeleriboardK
Bug "JoinedTelerikAcademy" with BugID 1 was created on board "TeleriboardK".
BUGSETPRIORITY JoinedTelerikAcademy LOW
Priority for Bug with BugID 1 was set to Low
```

***

- [ ] BUGSETSEVERITY [bugName BugSeverity]

  Sets a new status for bug; requires 2 parameters:

  * bugName; the name of the story you want to change status for (Never gonna give you up, yet...)
  * BugSeverity; set one of the following: CRITICAL, MAJOR, LOW
```
CREATEBUG JoinedTelerikAcademy {{Joined the academy and got insufficient knowledge but not yet removed!Wai?}} ? HIGH CRITICAL ACTIVE PRaykov 1.Join_Telerik_again;2.Trainers_fail_to_teach_again;3.Recieve_inssuficient_again; TeleriboardK
Bug "JoinedTelerikAcademy" with BugID 1 was created on board "TeleriboardK".
BUGSETSEVERITY JoinedTelerikAcademy MAJOR
Severity for Bug with BugID 1 was set to Major
```

***
    
  - [ ] BUGSETSTATUS [bugName BugStatus]

  Sets a new status for bug; requires 2 parameters:

  * bugName; the name of the story you want to change status for (It is time to reconsider why we are doing this)
  * BugSeverity; set one of the following:  ACTIVE, FIXED
```
CREATEBUG JoinedTelerikAcademy {{Joined the academy and got insufficient knowledge but not yet removed!Wai?}}  HIGH CRITICAL ACTIVE PRaykov 1.Join_Telerik_again;2.Trainers_fail_to_teach_again;3.Recieve_inssuficient_again; TeleriboardK
Bug "JoinedTelerikAcademy" with BugID 1 was created on board "TeleriboardK".
BUGSETSTATUS JoinedTelerikAcademy FIXED
BugStatus for Bug with BugID 1 was set to Fixed
```

***  

  - [ ] SETBUGASSIGNEE [bugName assignee]

  Sets a new assignee for bug; requires 2 parameters:

  * bugName; the name of the bug you want to change status for (Like a troller-coaster, god damn it)
  * assignee; set assignee, without any mercy on him
```
Createperson PRaykov
Person "PRaykov" with ID 1 was created.
CreateTEAM Telerik
Team "Telerik" with TeamID 2 was created.
CREATEBOARD TeleriB Telerik
Board with name "TeleriB" and BoardID 3 was created.
CREATEBUG JoinedTelerikAcademy {{Joined the academy and got insufficient knowledge but not yet removed!Wai?}} HIGH CRITICAL ACTIVE 1.Join_Telerik_again;2.Trainers_fail_to_teach_again;3.Recieve_inssuficient_again; TeleriB
Bug "JoinedTelerikAcademy" with BugID 4 was created on board "TeleriB".
SETBUGASSIGNEE JoinedTelerikAcademy PRaykov
Bug "JoinedTelerikAcademy" was assigned to PRaykov!
```

***

  - [ ] UNASSIGNBUG [bugName]

  Sets a new assignee for bug; requires 1 parameters:

  * bugName; the name of the bug you want to change status for (so here we go backwards, like we are in a trap of some sort)
```
Createperson PRaykov
Person "PRaykov" with ID 1 was created.
CreateTEAM Telerik
Team "Telerik" with TeamID 2 was created.
CREATEBOARD TeleriB Telerik
Board with name "TeleriB" and BoardID 3 was created.
CREATEBUG JoinedTelerikAcademy {{Joined the academy and got insufficient knowledge but not yet removed!Wai?}} HIGH CRITICAL ACTIVE 1.Join_Telerik_again;2.Trainers_fail_to_teach_again;3.Recieve_inssuficient_again; TeleriB
Bug "JoinedTelerikAcademy" with BugID 4 was created on board "TeleriB".
SETBUGASSIGNEE JoinedTelerikAcademy PRaykov
Bug "JoinedTelerikAcademy" was assigned to PRaykov!
UNASSIGNBUG JoinedTelerikAcademy
Bug JoinedTelerikAcademy was assigned to Not Assigned!
```

***  

  - [ ] SETSTORYASSIGNEE [storyName]

  Sets a new assignee for story; requires 2 parameters:

  * storyName; the name of the story you want to change status for (rinse and repeat)
  * assignee; set assignee, eat his soul
  
```
Createperson PRaykov
Person "PRaykov" with ID 1 was created.
CreateTEAM Telerik
Team "Telerik" with TeamID 2 was created.
CREATEBOARD TeleriB Telerik
Board with name "TeleriB" and BoardID 3 was created.
CREATESTORY GiveUsBetterWorkshops {{Give us better workshops to copy from please we are struggling!For real!}} HIGH LARGE INPROGRESS TeleriB
Story with StoryID 6, Priority "HIGH" was created on board "TeleriB".
SETSTORYASSIGNEE GiveUsBetterWorkshops PRaykov
Story "GiveUsBetterWorkshops" was assigned to PRaykov!
```

***  

  - [ ] UNASSIGNSTORY [storyName]

  Sets a new assignee for story; requires 1 parameters:

  * storyName; the name of the story you want to change status for (hopefully final...)
  
```
Createperson PRaykov
Person "PRaykov" with ID 1 was created.
CreateTEAM Telerik
Team "Telerik" with TeamID 2 was created.
CREATEBOARD TeleriB Telerik
Board with name "TeleriB" and BoardID 3 was created.
CREATESTORY GiveUsBetterWorkshops {{Give us better workshops to copy from please we are struggling!For real!}} HIGH LARGE INPROGRESS TeleriB
Story with StoryID 6, Priority "HIGH" was created on board "TeleriB".
SETSTORYASSIGNEE GiveUsBetterWorkshops PRaykov
Story "GiveUsBetterWorkshops" was assigned to PRaykov!
UNASSIGNSTORY GiveUsBetterWorkshops
Story "GiveUsBetterWorkshops" was assigned to Not Assigned!
```

***  

## How to use commands - Interaction commands

Because the program is so lame, we decided to implement interaction commands, which allow each registered person to quickly understand what data concerns him

- [ ] LOGIN [PerosnName]

  Logs in as user; no password required; requires 1 parameters:

  * PerosnName; the name of the person you want to log in as; must exist

```
LOGIN PRaykov
There is no person named "PRaykov"!
CREATEPERSON PRaykov
Person "PRaykov" with ID 1 was created.
LOGIN PRaykov
You are now interating as PRaykov!
```

- [ ] LOGOUT

  Logs out the user to quit interracting as him; no parameters required

```
LOGIN PRaykov
You are now interating as PRaykov!
LOGOUT
You are now interacting as anonymous
```

***

- [ ] LISTMYTASKS

  Shows list of all task assigned ot the logged in user; does not require any parameter; requires logged in user
  
```
LISTMYTASKS
There is no logged in user.
####################
LOGIN PRaykov
You are now interacting as PRaykov!
####################
LISTMYTASKS
[####################] 100% complete retrieving data
------ Stories -----
List of bugs matching you as assignee is empty
------ Bugs -----
List of bugs matching you as assignee is empty

```

***

- [ ] LISTMYTEAMS

  Lists all teams for which the interacting user is member;  no parameters required

```
Not properly implemented yet
```

***

## Easter eggs

- [ ] TICTACTOE - launches a Tic-Tac-Toe game to be played in order to determine who will buy the drinks. Without drinks you'll get depressed using this program and most likely will kill yourself.

- [ ] PRINT [String] - Tries to print out an ASCII block of the given string. Ugly piece of sh*t, couldn`t make it work properly, but we`re lazy enough not to remove it.

***

## Known Issues

- [ ] IDs for obejcts are jumping up upon unsuccesful creation of object. Please forgive us for copying from Telerik, we should have copied from somewhere else. Avoid creating mistakes, or the program may f*ck up really bad. 

- [ ] Each person can be a member of only one team. Is this how it is supposed to work?

## Usage cases


- [ ] Use case #1

One of the developers has noticed a bug in the company’s product. He starts the application and goes on to create a new Task for it. He creates a new Bug and gives it the title "The program freezes when the Log In button is clicked." For the description he adds "This needs to be fixed quickly!", he marks the Bug as High priority and gives it Critical severity. Since it is a new bug, it gets the Active status. The developer also assigns it to the senior developer in the team. To be able to fix the bug, the senior developer needs to know how to reproduce it, so the developer who logged the bug adds a list of steps to reproduce: "1. Open the application; 2. Click "Log In"; 3. The application freezes!" The bug is saved to the application and is ready to be fixed.


```
CREATEPERSON PRaykov
Person "PRaykov" with ID 1 was created.
####################

CREATETEAM Developers
Team "Developers" with TeamID 2 was created.
####################

ADDPERSONTOTEAM Developers PRaykov
Person with name "PRaykov" was added to Team "Developers" with TeamID 2.
####################

CREATEBOARD WhiteBoard Developers
Board with name "WhiteBoard" and BoardID 3 was created.
####################

CREATEBUG The program freezes when the Log In button is clicked. This needs to be fixed quickly! HIGH CRITICAL ACTIVE "1. Open the application; 2. Click "Log In"; 3. The application freezes!" WhiteBoard
The title should be 10 symbols minimum and 50 symbols maximum.

CREATEBUG Program freezes when the LogIn button is clicked. This needs to be fixed quickly! HIGH CRITICAL ACTIVE "1. Open the application; 2. Click "Log In"; 3. The application freezes!" WhiteBoard
Bug "Program freezes when the LogIn button is clicked." with BugID 5 was created on board "WhiteBoard".
####################

SETBUGASSIGNEE Program freezes when the LogIn button is clicked. PRaykov
Bug "Program freezes when the LogIn button is clicked." was assigned to PRaykov!
####################

LOGIN PRaykov
You are now interacting as PRaykov!
####################
LISTMYTASKS
[####################] 100% complete retrieving data
------ Stories ----- 
List of bugs matching you as assignee is empty 
------ Bugs ----- 
Bug (Update after Kaloyan writes out his getAsString; method)
####################
LOGOUT
You are now interacting as anonymous
####################
```

***

- [ ] Use case #2

A new developer has joined the team. One of the other developers starts the application and creates a new team member. After that, he adds the new team member to one of the existing teams and assigns all Critical bugs to him.


```
CREATEPERSON Chavo
Person "Chavo" with ID 6 was created.
####################

ADDPERSONTOTEAM Developers Chavo
Person with name "Chavo" was added to Team "Developers" with TeamID 2.
####################

SHOWALLBUGS
[####################] 100% complete retrieving data
Bug (Update after Kaloyan writes out his getAsString; method)
####################

SETBUGASSIGNEE Program freezes when the LogIn button is clicked. Elitsa
There is no person named "Elitsa"!
####################

SETBUGASSIGNEE Program freezes when the LogIn button is clicked. Chavo
Bug "Program freezes when the LogIn button is clicked." was assigned to Chavo!
####################

```

***

- [ ] Use case #3

One of the developers has fixed a bug that was assigned to him. He adds a comment to that bug, saying "This one took me a while, but it is fixed now!", and then changes the status of the bug to Fixed. Just to be sure, he checks the changes history list of the bug and sees that the last entry in the list says, "The status of item with ID 42 switched from Active to Done."


```
CREATEPERSON Elitsa
Person "Elitsa" with ID 1 was created.
####################
CREATETEAM Developers
Team "Developers" with TeamID 2 was created.
####################
ADDPERSONTOTEAM Developers Elitsa
Person with name "Elitsa" was added to Team "Developers" with TeamID 2.
####################
SHOWALLBUGS
[####################] 100% complete retrieving data
There are no registered bugs.
####################
CREATEBOARD TeleriKbb Developers
Board with name "TeleriKbb" and BoardID 3 was created.
####################

CREATEBUG JoinedTelerikAcademy {{Got insufficient knowledge but not yet removed!Wai?}} HIGH CRITICAL ACTIVE 1.Join_Telerik_again;2.Trainers_fail_to_teach_again;3.Recieve_inssuficient_again; TeleriKbb
Bug "JoinedTelerikAcademy" with BugID 9 was created on board "TeleriKbb".
####################
SETBUGASSIGNEE JoinedTelerikAcademy PRaykov
There is no person named "PRaykov"!
####################
SETBUGASSIGNEE JoinedTelerikAcademy Elitsa
Bug "JoinedTelerikAcademy" was assigned to Elitsa!
####################
CREATECOMMENT JoinedTelerikAcademy {{This one took me a while, but it is fixed now!}}
There is no logged in user.
####################
LOGIN Elitsa
You are now interacting as Elitsa!
####################
CREATECOMMENT JoinedTelerikAcademy {{This one took me a while, but it is fixed now!}}
Elitsa added comment successfully to bug with name "JoinedTelerikAcademy"!
####################
BUGSETSTATUS JoinedTelerikAcademy FIXED
BugStatus for Bug with BugID 9 was set to FIXED
LISTALLBUGHISTORY
[####################] 100% complete retrieving data
[12-май-2023 00:08:33] Bug: Bug "JoinedTelerikAcademy" with BugID 9 was created on board "TeleriKbb".
#################### 

[12-май-2023 00:08:41] Bug: Bug "JoinedTelerikAcademy" was assigned to Elitsa!
#################### 

[12-май-2023 00:09:27] Bug: BugStatus for Bug with BugID 9 was set to FIXED
####################
***