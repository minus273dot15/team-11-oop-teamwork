package com.company.oop.taskmanagement.tests.utils;

import com.company.oop.taskmanagement.exceptions.InvalidUserInputException;
import com.company.oop.taskmanagement.models.items.enums.Status;
import com.company.oop.taskmanagement.utils.ParsingHelpers;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static com.company.oop.taskmanagement.models.PersonImpl.NAME_MAX_LENGTH;
import static com.company.oop.taskmanagement.tests.utils.TestUtilities.getString;


public class ParsingHelpersTests {


    public static final String INVALID_INTEGER_NAME = getString(NAME_MAX_LENGTH + 1);
    public static final String INVALID_DOUBLE_NAME = getString(NAME_MAX_LENGTH + 1);
    public static final String INVALID_BOOLEAN_NAME = getString(NAME_MAX_LENGTH + 1);
    public static final String VALID_BOOLEAN_NAME = "True";
    public static final String VALID_INTEGER_STRING = "1234";
    public static final String VALID_DOUBLE_STRING = "1234.56";
    public static final String ERR_MSG = getString(NAME_MAX_LENGTH + 2);

    @Test
    public void tryParseBoolean_Should_ThrowException_When_NotBoolean() {
        // Arrange, Act, Assert
        Assertions.assertThrows(InvalidUserInputException.class,
                () -> ParsingHelpers.tryParseBoolean(INVALID_BOOLEAN_NAME, ERR_MSG));
    }

    @Test
    public void tryParseBoolean_Should_returnTrue_When_BooleanPassed() {
        // Arrange, Act, Assert
        Assertions.assertTrue(() -> ParsingHelpers.tryParseBoolean(VALID_BOOLEAN_NAME, ERR_MSG));
    }

    @Test
    public void tryParseInteger_Should_ThrowException_When_NotInteger() {
        // Arrange, Act, Assert
        Assertions.assertThrows(InvalidUserInputException.class,
                () -> ParsingHelpers.tryParseBoolean(INVALID_INTEGER_NAME, ERR_MSG));
    }

    @Test
    public void tryParseInteger_Should_returnInteger_When_ValidStringPassed() {
        // Arrange, Act, Assert
        Assertions.assertEquals(
                Integer.valueOf(VALID_INTEGER_STRING),
                ParsingHelpers.tryParseInteger(VALID_INTEGER_STRING, ERR_MSG)
        );
    }
    @Test
    public void tryParseDouble_Should_ThrowException_When_NotDouble() {
        // Arrange, Act, Assert
        Assertions.assertThrows(InvalidUserInputException.class,
                () -> ParsingHelpers.tryParseBoolean(INVALID_DOUBLE_NAME, ERR_MSG));
    }

    @Test
    public void tryParseDouble_Should_returnDouble_When_ValidStringPassed() {
        // Arrange, Act, Assert
        Assertions.assertEquals(
                Double.valueOf(VALID_DOUBLE_STRING),
                ParsingHelpers.tryParseDouble(VALID_DOUBLE_STRING, ERR_MSG)
        );
    }

    @Test
    public void tryParseEnum_Should_ThrowException_When_NotEnum() {
        // Arrange, Act, Assert
        Assertions.assertThrows(IllegalArgumentException.class,
                () -> ParsingHelpers.tryParseEnum("NICHTSVERSTEEN", Status.class, ERR_MSG));
    }
    @Test
    public void tryParseEnum_Should_returnEnum_When_ValidStringPassed() {
        // Arrange, Act, Assert
        Assertions.assertEquals(
                Status.DONE,
                ParsingHelpers.tryParseEnum("DONE", Status.class, ERR_MSG)
        );
    }


}
