package com.company.oop.taskmanagement.tests.utils;

import com.company.oop.taskmanagement.models.contracts.Person;
import com.company.oop.taskmanagement.models.items.HistoryImpl;
import com.company.oop.taskmanagement.models.items.contracts.Bugs;
import com.company.oop.taskmanagement.models.items.contracts.Feedback;
import com.company.oop.taskmanagement.models.items.contracts.History;
import com.company.oop.taskmanagement.models.items.contracts.Story;
import com.company.oop.taskmanagement.models.items.enums.BugsStatus;
import com.company.oop.taskmanagement.models.items.enums.FeedbackStatus;
import com.company.oop.taskmanagement.models.items.enums.Status;
import com.company.oop.taskmanagement.tests.models.PersonImplTests;
import com.company.oop.taskmanagement.tests.models.items.BugsImplTests;
import com.company.oop.taskmanagement.tests.models.items.FeedbackImplTests;
import com.company.oop.taskmanagement.tests.models.items.StoryImplTests;
import com.company.oop.taskmanagement.utils.ListingHelpers;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class ListingHelpersTests {

    @Test
    public void feedbacksToStringShould_ListFeedbacks_When_ArgumentsAreValid() {
        // Arrange
        Feedback feedback = FeedbackImplTests.initializeFeedback();
        final List<Feedback> feedbacks = new ArrayList<>();

        // Act
        feedbacks.add(feedback);
        String toString = ListingHelpers.feedbacksToString(feedbacks);
        // Assert
        Assertions.assertDoesNotThrow(() -> ListingHelpers.feedbacksToString(feedbacks));
        assertTrue(toString.contains("Feedback"));
    }

    @Test
    public void storiesToStringShould_ListStories_When_ArgumentsAreValid() {
        // Arrange  storiesToString
        Story story = StoryImplTests.initializeStory();
        final List<Story> stories = new ArrayList<>();

        // Act
        stories.add(story);
        String toString = ListingHelpers.storiesToString(stories);
        // Assert
        Assertions.assertDoesNotThrow(() -> ListingHelpers.storiesToString(stories));
        assertTrue(toString.contains("Story"));
    }
    @Test
    public void personsToStringShould_ListPersons_When_ArgumentsAreValid() {
        // Arrange  storiesToString
        Person person = PersonImplTests.initializePerson();
        final List<Person> personList = new ArrayList<>();

        // Act
        personList.add(person);
        String toString = ListingHelpers.personsToString(personList);
        // Assert
        Assertions.assertDoesNotThrow(() -> ListingHelpers.personsToString(personList));
        assertTrue(toString.contains("Person"));
    }

    /** Test should be okay, but BugImpl is not finished by Kaloyan yet, check later */
    @Test
    public void bugsToStringShould_ListBugs_When_ArgumentsAreValid() {
        // Arrange  storiesToString
        Bugs bug = BugsImplTests.initializeBug();
        final List<Bugs> bugs = new ArrayList<>();

        // Act
        bugs.add(bug);
        String toString = ListingHelpers.bugsToString(bugs);
        // Assert
        Assertions.assertDoesNotThrow(() -> ListingHelpers.bugsToString(bugs));
        assertTrue(toString.contains("Bug"));//Printing on bug not implemented yet?
    }
    @Test
    public void feedbacksToStringFilteredShould_ListFeedbacks_When_ArgumentsAreValid() {
        // Arrange
        Feedback feedback = FeedbackImplTests.initializeFeedback();
        final List<Feedback> feedbacks = new ArrayList<>();

        // Act
        feedbacks.add(feedback);
        String toString = ListingHelpers.feedbacksToStringFiltered(feedbacks, 9);
        // Assert
        Assertions.assertDoesNotThrow(() -> ListingHelpers.feedbacksToStringFiltered(feedbacks, 9));
        assertTrue(toString.contains("Feedback"));
        assertTrue(toString.contains(FeedbackImplTests.FEEDBACK_VALID_TITLE));
        assertTrue(toString.contains(FeedbackImplTests.FEEDBACK_VALID_DESCRIPTION));
    }

    @Test
    public void feedbacksToStringSortedByRatingShould_ListFeedbacks_When_ArgumentsAreValid() {
        // Arrange
        Feedback feedback = FeedbackImplTests.initializeFeedback();
        final List<Feedback> feedbacks = new ArrayList<>();

        // Act
        feedbacks.add(feedback);
        String toString = ListingHelpers.feedbacksToStringSortedByRating(feedbacks);
        // Assert
        Assertions.assertDoesNotThrow(() -> ListingHelpers.feedbacksToStringSortedByRating(feedbacks));
        assertTrue(toString.contains("Feedback"));
        assertTrue(toString.contains(FeedbackImplTests.FEEDBACK_VALID_TITLE));
        assertTrue(toString.contains(FeedbackImplTests.FEEDBACK_VALID_DESCRIPTION));
    }
    @Test
    public void feedbacksToStringSortedByTitleShould_ListFeedbacks_When_ArgumentsAreValid() {
        // Arrange
        Feedback feedback = FeedbackImplTests.initializeFeedback();
        final List<Feedback> feedbacks = new ArrayList<>();

        // Act
        feedbacks.add(feedback);
        String toString = ListingHelpers.feedbacksToStringSortedByTitle(feedbacks);
        // Assert
        Assertions.assertDoesNotThrow(() -> ListingHelpers.feedbacksToStringSortedByTitle(feedbacks));
        assertTrue(toString.contains("Feedback"));
        assertTrue(toString.contains(FeedbackImplTests.FEEDBACK_VALID_TITLE));
        assertTrue(toString.contains(FeedbackImplTests.FEEDBACK_VALID_DESCRIPTION));
    }
    @Test
    public void feedbacksToStringFilteredStatusShould_ListFeedbacks_When_ArgumentsAreValid() {
        // Arrange
        Feedback feedback = FeedbackImplTests.initializeFeedback();
        final List<Feedback> feedbacks = new ArrayList<>();

        // Act
        feedbacks.add(feedback);
        String toString = ListingHelpers.feedbacksToStringFilteredStatus(feedbacks, FeedbackStatus.NEW);
        // Assert
        Assertions.assertDoesNotThrow(() -> ListingHelpers.feedbacksToStringFilteredStatus(feedbacks, FeedbackStatus.NEW));
        assertTrue(toString.contains("Feedback"));
        assertTrue(toString.contains(FeedbackImplTests.FEEDBACK_VALID_TITLE));
        assertTrue(toString.contains(FeedbackImplTests.FEEDBACK_VALID_DESCRIPTION));
    }
    @Test
    public void storiesToStringFilteredShould_ListStories_When_ArgumentsAreValid() {
        // Arrange  storiesToString
        Story story = StoryImplTests.initializeStory();
        final List<Story> stories = new ArrayList<>();

        // Act
        stories.add(story);
        String toString = ListingHelpers.storiesToStringFiltered(stories, Status.INPROGRESS);
        // Assert
        Assertions.assertDoesNotThrow(() -> ListingHelpers.storiesToStringFiltered(stories, Status.INPROGRESS));
        assertTrue(toString.contains("Story"));
    }
    @Test
    public void storiesToStringSortedBySizeShould_ListStories_When_ArgumentsAreValid() {
        // Arrange
        Story story = StoryImplTests.initializeStory();
        final List<Story> stories = new ArrayList<>();

        // Act
        stories.add(story);
        String toString = ListingHelpers.storiesToStringSortedBySize(stories);
        // Assert
        Assertions.assertDoesNotThrow(() -> ListingHelpers.storiesToStringSortedBySize(stories));
        assertTrue(toString.contains("Story"));
    }
    @Test
    public void storiesToStringSortedByPriorityShould_ListStories_When_ArgumentsAreValid() {
        // Arrange
        Story story = StoryImplTests.initializeStory();
        final List<Story> stories = new ArrayList<>();

        // Act
        stories.add(story);
        String toString = ListingHelpers.storiesToStringSortedByPriority(stories);
        // Assert
        Assertions.assertDoesNotThrow(() -> ListingHelpers.storiesToStringSortedByPriority(stories));
        assertTrue(toString.contains("Story"));
    }

    @Test
    public void storiesToStringSortedByTitleShould_ListStories_When_ArgumentsAreValid() {
        // Arrange
        Story story = StoryImplTests.initializeStory();
        final List<Story> stories = new ArrayList<>();

        // Act
        stories.add(story);
        String toString = ListingHelpers.storiesToStringSortedByTitle(stories);
        // Assert
        Assertions.assertDoesNotThrow(() -> ListingHelpers.storiesToStringSortedByTitle(stories));
        assertTrue(toString.contains("Story"));
    }

    @Test
    public void storiesToStringFilteredByAssigneeShould_ListStories_When_ArgumentsAreValid() {
        // Arrange
        Story story = StoryImplTests.initializeStory();
        final List<Story> stories = new ArrayList<>();

        // Act
        stories.add(story);
        String toString = ListingHelpers.storiesToStringFilteredByAssignee(stories, "Not Assigned");
        // Assert
        Assertions.assertDoesNotThrow(() -> ListingHelpers.storiesToStringFilteredByAssignee(stories, "Not Assigned"));
        assertTrue(toString.contains("Story"));
    }
    @Test
    public void bugsToStringFilteredShould_ListBugs_When_ArgumentsAreValid() {
        // Arrange  storiesToString
        Bugs bug = BugsImplTests.initializeBug();
        final List<Bugs> bugs = new ArrayList<>();

        // Act
        bugs.add(bug);
        String toString = ListingHelpers.bugsToStringFiltered(bugs, BugsStatus.ACTIVE);
        // Assert
        Assertions.assertDoesNotThrow(() -> ListingHelpers.bugsToStringFiltered(bugs, BugsStatus.ACTIVE));
        assertTrue(toString.contains("Bug"));
    }
    @Test
    public void bugsToStringSortedByTitleShould_ListBugs_When_ArgumentsAreValid() {
        // Arrange  storiesToString
        Bugs bug = BugsImplTests.initializeBug();
        final List<Bugs> bugs = new ArrayList<>();

        // Act
        bugs.add(bug);
        String toString = ListingHelpers.bugsToStringSortedByTitle(bugs);
        // Assert
        Assertions.assertDoesNotThrow(() -> ListingHelpers.bugsToStringSortedByTitle(bugs));
        assertTrue(toString.contains("Bug"));
    }
    @Test
    public void bugsToStringSortedByPriorityShould_ListBugs_When_ArgumentsAreValid() {
        // Arrange  storiesToString
        Bugs bug = BugsImplTests.initializeBug();
        final List<Bugs> bugs = new ArrayList<>();

        // Act
        bugs.add(bug);
        String toString = ListingHelpers.bugsToStringSortedByPriority(bugs);
        // Assert
        Assertions.assertDoesNotThrow(() -> ListingHelpers.bugsToStringSortedByPriority(bugs));
        assertTrue(toString.contains("Bug"));
    }
    @Test
    public void bugsToStringSortedBySeverityShould_ListBugs_When_ArgumentsAreValid() {
        // Arrange  storiesToString
        Bugs bug = BugsImplTests.initializeBug();
        final List<Bugs> bugs = new ArrayList<>();

        // Act
        bugs.add(bug);
        String toString = ListingHelpers.bugsToStringSortedBySeverity(bugs);
        // Assert
        Assertions.assertDoesNotThrow(() -> ListingHelpers.bugsToStringSortedBySeverity(bugs));
        assertTrue(toString.contains("Bug"));
    }
    @Test
    public void bugsToStringFilteredByAssigneeShould_ListBugs_When_ArgumentsAreValid() {
        // Arrange  storiesToString
        Bugs bug = BugsImplTests.initializeBug();
        final List<Bugs> bugs = new ArrayList<>();

        // Act
        bugs.add(bug);
        String toString = ListingHelpers.bugsToStringFilteredByAssignee(bugs, "Not Assigned");
        // Assert
        Assertions.assertDoesNotThrow(() -> ListingHelpers.bugsToStringFilteredByAssignee(bugs, "Not Assigned"));
        assertTrue(toString.contains("Bug"));
    }

    @Test
    public void bugsHistoryToStringShould_ListBugsHistory_When_ArgumentsAreValid() {
        // Arrange  storiesToString
        Bugs bug = BugsImplTests.initializeBug();
        final List<Bugs> bugs = new ArrayList<>();
        final List<HistoryImpl> histories = new ArrayList<>();

        // Act
        bugs.add(bug);
        String toString = ListingHelpers.bugsHistoryToString(histories);
        // Assert
        Assertions.assertDoesNotThrow(() -> ListingHelpers.bugsHistoryToString(histories));
        assertTrue(toString.contains("History"));
    }

    @Test
    public void storiesHistoryToStringShould_ListStoriesHistory_When_ArgumentsAreValid() {
        // Arrange  storiesToString
        Story story = StoryImplTests.initializeStory();
        final List<Story> stories = new ArrayList<>();
        final List<HistoryImpl> histories = new ArrayList<>();

        // Act
        stories.add(story);
        String toString = ListingHelpers.storiesHistoryToString(histories);
        // Assert
        Assertions.assertDoesNotThrow(() -> ListingHelpers.storiesHistoryToString(histories));
        assertTrue(toString.contains("History"));
    }

    @Test
    public void feedbacksHistoryToStringShould_ListFeedbackHistory_When_ArgumentsAreValid() {
        // Arrange  storiesToString
        Feedback feedback = FeedbackImplTests.initializeFeedback();
        final List<Feedback> feedbacks = new ArrayList<>();
        final List<HistoryImpl> histories = new ArrayList<>();

        // Act
        feedbacks.add(feedback);
        String toString = ListingHelpers.feedbacksHistoryToString(histories);
        // Assert
        Assertions.assertDoesNotThrow(() -> ListingHelpers.feedbacksHistoryToString(histories));
        assertTrue(toString.contains("History"));
    }
}
