package com.company.oop.taskmanagement.tests.core;

import com.company.oop.taskmanagement.core.TaskManagmentRepositoryImpl;
import com.company.oop.taskmanagement.core.contracts.TaskManagmentRepository;
import com.company.oop.taskmanagement.exceptions.ElementNotFoundException;
import com.company.oop.taskmanagement.exceptions.InvalidUserInputException;
import com.company.oop.taskmanagement.models.contracts.Board;
import com.company.oop.taskmanagement.models.contracts.Person;
import com.company.oop.taskmanagement.models.contracts.Team;
import com.company.oop.taskmanagement.models.items.contracts.Bugs;
import com.company.oop.taskmanagement.models.items.contracts.Comments;
import com.company.oop.taskmanagement.models.items.contracts.Feedback;
import com.company.oop.taskmanagement.models.items.contracts.Story;
import com.company.oop.taskmanagement.models.items.enums.*;
import com.company.oop.taskmanagement.tests.models.BoardImplTests;
import com.company.oop.taskmanagement.tests.models.PersonImplTests;
import com.company.oop.taskmanagement.tests.models.TeamImplTests;
import com.company.oop.taskmanagement.tests.models.items.BugsImplTests;
import com.company.oop.taskmanagement.tests.models.items.FeedbackImplTests;
import com.company.oop.taskmanagement.tests.models.items.StoryImplTests;
import com.company.oop.taskmanagement.utils.ParsingHelpers;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import javax.xml.stream.events.Comment;
import java.util.List;

import static com.company.oop.taskmanagement.models.items.TaskImpl.*;
import static com.company.oop.taskmanagement.tests.utils.TestUtilities.getString;
import static com.company.oop.taskmanagement.utils.DummyArrayLists.*;
import static org.junit.jupiter.api.Assertions.*;

public class TaskManagementRepositoryTests {

    public static final String VALID_STORY_TITLE = getString(TITLE_MIN_LENGTH + 1);
    public static final String VALID_STORY_DESCRIPTION = getString(DESCRIPTION_MIN_LENGTH + 1);

    TaskManagmentRepository repository;

    @BeforeEach
    public void beforeEach() {
        repository = new TaskManagmentRepositoryImpl();
    }

    @Test
    public void constructor_Should_InitializeAllCollections() {
        // Arrange, Act, Assert
        assertAll(
                () -> Assertions.assertNotNull(repository.getBugs()),
                () -> Assertions.assertNotNull(repository.getFeedbacks()),
                () -> Assertions.assertNotNull(repository.getStories())
        );
    }

    @Test
    public void getFeedbackShould_ReturnCopyOfCollection() {
        // Arrange
        List<Feedback> categoriesReference = repository.getFeedbacks();
        List<Feedback> sameReference = repository.getFeedbacks();

        // Act, Assert
        Assertions.assertNotSame(categoriesReference, sameReference);
    }

    @Test
    public void getBugsShould_ReturnCopyOfCollection() {
        // Arrange
        List<Bugs> categoriesReference = repository.getBugs();
        List<Bugs> sameReference = repository.getBugs();
        // Act, Assert
        Assertions.assertNotSame(categoriesReference, sameReference);
    }

    @Test
    public void getStoriesShould_ReturnCopyOfCollection() {
        // Arrange
        List<Story> categoriesReference = repository.getStories();
        List<Story> sameReference = repository.getStories();
        // Act, Assert
        Assertions.assertNotSame(categoriesReference, sameReference);
    }

    @Test
    public void getPersonsShould_ReturnCopyOfCollection() {
        // Arrange
        List<Person> categoriesReference = repository.getPersons();
        List<Person> sameReference = repository.getPersons();
        // Act, Assert
        Assertions.assertNotSame(categoriesReference, sameReference);
    }

    @Test
    public void getBoardsShould_ReturnCopyOfCollection() {
        // Arrange
        List<Board> categoriesReference = repository.getBoards();
        List<Board> sameReference = repository.getBoards();
        // Act, Assert
        Assertions.assertNotSame(categoriesReference, sameReference);
    }

    @Test
    public void getTeamsShould_ReturnCopyOfCollection() {
        // Arrange
        List<Team> categoriesReference = repository.getTeams();
        List<Team> sameReference = repository.getTeams();
        // Act, Assert
        Assertions.assertNotSame(categoriesReference, sameReference);
    }


    @Test
    public void findStoryById_Should_ThrowException_When_StoryDoesNotExist() {
        //Arrange, Act, Assert
        assertThrows(ElementNotFoundException.class, () -> repository.findStoryById(1));
    }

    @Test
    public void findFeedbackById_Should_ThrowException_When_FeedbackDoesNotExist() {
        //Arrange, Act, Assert
        assertThrows(ElementNotFoundException.class, () -> repository.findFeedbackById(1));
    }

    @Test
    public void findBugsById_Should_ThrowException_When_BugsDoesNotExist() {
        //Arrange, Act, Assert
        assertThrows(ElementNotFoundException.class, () -> repository.findBugsById(1));
    }

    @Test
    public void findBoardById_Should_ThrowException_When_BoardDoesNotExist() {
        //Arrange, Act, Assert
        assertThrows(ElementNotFoundException.class, () -> repository.findBoardById(1));
    }

    @Test
    public void findPersonById_Should_ThrowException_When_PersonDoesNotExist() {
        //Arrange, Act, Assert
        assertThrows(ElementNotFoundException.class, () -> repository.findPersonById(1));
    }

    @Test
    public void findTeamById_Should_ThrowException_When_PersonDoesNotExist() {
        //Arrange, Act, Assert
        assertThrows(ElementNotFoundException.class, () -> repository.findTeamById(1));
    }

    @Test
    public void findStoryById_Should_ReturnStory_When_StoryExists() {
        //Arrange
        repository.createStory(
                VALID_STORY_TITLE,
                VALID_STORY_DESCRIPTION,
                Priority.HIGH,
                Size.LARGE,
                Status.INPROGRESS,
                "PRaykov",
                FeedbackImplTests.VALID_BOARD_NAME
        );

        // Act
        Story story = repository.findStoryById(1);

        // Assert
        assertAll(
                () -> assertEquals(1, story.getId()),
                () -> Assertions.assertEquals(VALID_STORY_DESCRIPTION, story.getDescription()),
                () -> Assertions.assertEquals(VALID_STORY_TITLE, story.getTitle())
        );
    }

    @Test
    public void findStoryByName_Should_ReturnStory_When_StoryExists() {
        //Arrange
        repository.createStory(
                VALID_STORY_TITLE,
                VALID_STORY_DESCRIPTION,
                Priority.HIGH,
                Size.LARGE,
                Status.INPROGRESS,
                "PRaykov",
                FeedbackImplTests.VALID_BOARD_NAME
        );

        // Act
        Story story = repository.findStoryByName(VALID_STORY_TITLE);

        // Assert
        assertAll(
                () -> assertEquals(1, story.getId()),
                () -> Assertions.assertEquals(VALID_STORY_DESCRIPTION, story.getDescription()),
                () -> Assertions.assertEquals(VALID_STORY_TITLE, story.getTitle())
        );
    }

    @Test
    public void findStoryByName_Should_ThrowException_When_StoryDoesNotExist() {
        //Arrange, Act, Assert
        assertThrows(ElementNotFoundException.class, () -> repository.findStoryByName("NoComprehend"));
    }

    @Test
    public void findFeedbackById_Should_ReturnFeedback_When_FeedbackExists() {
        //Arrange
        repository.createFeedback(
                FeedbackImplTests.FEEDBACK_VALID_TITLE,
                FeedbackImplTests.FEEDBACK_VALID_DESCRIPTION,
                9,
                FeedbackStatus.NEW,
                FeedbackImplTests.VALID_BOARD_NAME
        );

        // Act
        Feedback feedback = repository.findFeedbackById(1);

        // Assert
        assertAll(
                () -> assertEquals(1, feedback.getId()),
                () -> Assertions.assertEquals(FeedbackImplTests.FEEDBACK_VALID_DESCRIPTION, feedback.getDescription()),
                () -> Assertions.assertEquals(FeedbackImplTests.FEEDBACK_VALID_TITLE, feedback.getTitle())
        );
    }

    @Test
    public void findFeedbackByName_Should_ReturnFeedback_When_FeedbackExists() {
        //Arrange
        repository.createFeedback(
                FeedbackImplTests.FEEDBACK_VALID_TITLE,
                FeedbackImplTests.FEEDBACK_VALID_DESCRIPTION,
                9,
                FeedbackStatus.NEW,
                FeedbackImplTests.VALID_BOARD_NAME
        );

        // Act
        Feedback feedback = repository.findFeedbackByName(FeedbackImplTests.FEEDBACK_VALID_TITLE);

        // Assert
        assertAll(
                () -> assertEquals(1, feedback.getId()),
                () -> Assertions.assertEquals(FeedbackImplTests.FEEDBACK_VALID_DESCRIPTION, feedback.getDescription()),
                () -> Assertions.assertEquals(FeedbackImplTests.FEEDBACK_VALID_TITLE, feedback.getTitle())
        );
    }

    @Test
    public void findFeedbackByName_Should_ThrowException_When_FeedbackDoesNotExist() {
        //Arrange, Act, Assert
        assertThrows(ElementNotFoundException.class, () -> repository.findFeedbackByName("NoComprehend"));
    }

    @Test
    public void findBugById_Should_ReturnBug_When_BugExists() {
        //Arrange
        repository.createNewBug(
                BugsImplTests.VALID_BUG_TITLE,
                BugsImplTests.VALID_BUG_DESCRIPTION,
                Priority.HIGH,
                Severity.CRITICAL,
                BugsStatus.ACTIVE,
                "PRaykov",
                stepsToReproduce,
                "Telerik"
        );

        // Act
        Bugs bug = repository.findBugsById(1);

        // Assert
        assertAll(
                () -> assertEquals(1, bug.getId()),
                () -> Assertions.assertEquals(BugsImplTests.VALID_BUG_DESCRIPTION, bug.getDescription()),
                () -> Assertions.assertEquals(BugsImplTests.VALID_BUG_TITLE, bug.getTitle())
        );
    }

    @Test
    public void findBugByName_Should_ReturnBug_When_BugExists() {
        //Arrange
        repository.createNewBug(
                BugsImplTests.VALID_BUG_TITLE,
                BugsImplTests.VALID_BUG_DESCRIPTION,
                Priority.HIGH,
                Severity.CRITICAL,
                BugsStatus.ACTIVE,
                "PRaykov",
                stepsToReproduce,
                "Telerik"
        );

        // Act
        Bugs bug = repository.findBugByName(BugsImplTests.VALID_BUG_TITLE);

        // Assert
        assertAll(
                () -> assertEquals(1, bug.getId()),
                () -> Assertions.assertEquals(BugsImplTests.VALID_BUG_DESCRIPTION, bug.getDescription()),
                () -> Assertions.assertEquals(BugsImplTests.VALID_BUG_TITLE, bug.getTitle())
        );
    }

    @Test
    public void findBugByName_Should_ThrowException_When_BugDoesNotExist() {
        //Arrange, Act, Assert
        assertThrows(ElementNotFoundException.class, () -> repository.findBugByName("NoComprehend"));
    }

    @Test
    public void findBoardByName_Should_ReturnBoard_When_BoardExists() {
        //Arrange
        repository.createBoard(
                BoardImplTests.VALID_BOARD_NAME,
                BoardImplTests.VALID_BOARD_TEAM
        );

        // Act
        Board board = repository.findBoardByName(BoardImplTests.VALID_BOARD_NAME);

        // Assert
        assertAll(
                () -> assertEquals(1, board.getId()),
                () -> Assertions.assertEquals(BoardImplTests.VALID_BOARD_NAME, board.getName()),
                () -> Assertions.assertEquals(BoardImplTests.VALID_BOARD_TEAM, board.getBelongsToTeam())
        );
    }

    @Test
    public void findPersonByName_Should_ReturnPerson_When_PersonExists() {
        //Arrange
        repository.createNewPerson(
                PersonImplTests.VALID_PERSON_NAME,
                dummyActivityHistory,
                dummyTasks
        );

        // Act
        Person person = repository.findPersonByName(PersonImplTests.VALID_PERSON_NAME);

        // Assert
        assertAll(
                () -> assertEquals(1, person.getId()),
                () -> Assertions.assertEquals(PersonImplTests.VALID_PERSON_NAME, person.getName()),
                () -> Assertions.assertEquals(dummyActivityHistory, person.getActivityHistory()),
                () -> Assertions.assertEquals(dummyTasks, person.getTasks())
        );
    }
    @Test
    public void findPersonByName_Should_ThrowException_When_PersonDoesNotExist() {
        //Arrange, Act, Assert
        assertThrows(ElementNotFoundException.class, () -> repository.findPersonByName("NoComprehend"));
    }


    @Test
    public void findTeamByName_Should_ReturnTeam_When_TeamExists() {
        //Arrange
        repository.createTeam(
                TeamImplTests.VALID_TEAM_NAME
        );

        // Act
        Team team = repository.findTeamByName(TeamImplTests.VALID_TEAM_NAME);

        // Assert
        assertAll(
                () -> assertEquals(1, team.getId())
        );
    }

    @Test
    public void findTeamByName_Should_ThrowException_When_TeamDoesNotExist() {
        //Arrange, Act, Assert
        assertThrows(ElementNotFoundException.class, () -> repository.findTeamByName("NoComprehend"));
    }

    @Test
    public void createTeam_Should_ThrowException_When_TeamAlreadyExists() {
        //Arrange
        repository.createTeam(
                TeamImplTests.VALID_TEAM_NAME
        );
        // Act, Assert
        assertThrows(IllegalArgumentException.class, () -> repository.createTeam(TeamImplTests.VALID_TEAM_NAME));
    }

    @Test
    public void addPersonToTeam_Should_AddPersonToTeam_When_ArgumentsAreValid() {
        //Arrange
        repository.createTeam(
                TeamImplTests.VALID_TEAM_NAME
        );
        repository.createNewPerson(PersonImplTests.VALID_PERSON_NAME, dummyActivityHistory, dummyTasks);
        // Act, Assert
        assertDoesNotThrow(() -> repository.addPersonToTeam(PersonImplTests.VALID_PERSON_NAME, TeamImplTests.VALID_TEAM_NAME));
    }
    @Test
    public void setBugAssignee_Should_setBugAssignee_When_ArgumentsAreValid() {
        //Arrange
        repository.createNewPerson(PersonImplTests.VALID_PERSON_NAME, dummyActivityHistory, dummyTasks);
        repository.createTeam(TeamImplTests.VALID_TEAM_NAME);
        repository.createBoard(BoardImplTests.VALID_BOARD_NAME, TeamImplTests.VALID_TEAM_NAME);
        repository.createNewBug(BugsImplTests.VALID_BUG_TITLE,
                BugsImplTests.VALID_BUG_DESCRIPTION,
                Priority.HIGH,
                Severity.CRITICAL,
                BugsStatus.ACTIVE,
                "Not Assigned",
                stepsToReproduce,
                BoardImplTests.VALID_BOARD_NAME);
        repository.createNewPerson(PersonImplTests.VALID_PERSON_NAME, dummyActivityHistory, dummyTasks);
        // Act, Assert
        assertDoesNotThrow(() -> repository.setBugAssignee(BugsImplTests.VALID_BUG_TITLE, PersonImplTests.VALID_PERSON_NAME));
    }
    @Test
    public void setBugStatus_Should_setBugStatus_When_ArgumentsAreValid() {
        //Arrange
        repository.createNewPerson(PersonImplTests.VALID_PERSON_NAME, dummyActivityHistory, dummyTasks);
        repository.createTeam(TeamImplTests.VALID_TEAM_NAME);
        repository.createBoard(BoardImplTests.VALID_BOARD_NAME, TeamImplTests.VALID_TEAM_NAME);
        repository.createNewBug(BugsImplTests.VALID_BUG_TITLE,
                BugsImplTests.VALID_BUG_DESCRIPTION,
                Priority.HIGH,
                Severity.CRITICAL,
                BugsStatus.ACTIVE,
                "Not Assigned",
                stepsToReproduce,
                BoardImplTests.VALID_BOARD_NAME);
        repository.createNewPerson(PersonImplTests.VALID_PERSON_NAME, dummyActivityHistory, dummyTasks);
        // Act, Assert
        assertDoesNotThrow(() -> repository.setBugStatus(BugsImplTests.VALID_BUG_TITLE, BugsStatus.FIXED));
    }

    @Test
    public void setBugsSeverity_Should_setBugSeverity_When_ArgumentsAreValid() {
        //Arrange
        repository.createNewPerson(PersonImplTests.VALID_PERSON_NAME, dummyActivityHistory, dummyTasks);
        repository.createTeam(TeamImplTests.VALID_TEAM_NAME);
        repository.createBoard(BoardImplTests.VALID_BOARD_NAME, TeamImplTests.VALID_TEAM_NAME);
        repository.createNewBug(BugsImplTests.VALID_BUG_TITLE,
                BugsImplTests.VALID_BUG_DESCRIPTION,
                Priority.HIGH,
                Severity.CRITICAL,
                BugsStatus.ACTIVE,
                "Not Assigned",
                stepsToReproduce,
                BoardImplTests.VALID_BOARD_NAME);
        repository.createNewPerson(PersonImplTests.VALID_PERSON_NAME, dummyActivityHistory, dummyTasks);
        // Act, Assert
        assertDoesNotThrow(() -> repository.setBugsSeverity(BugsImplTests.VALID_BUG_TITLE, Severity.LOW));
    }
    @Test
    public void setBugsPriority_Should_setBugPriority_When_ArgumentsAreValid() {
        //Arrange
        repository.createNewPerson(PersonImplTests.VALID_PERSON_NAME, dummyActivityHistory, dummyTasks);
        repository.createTeam(TeamImplTests.VALID_TEAM_NAME);
        repository.createBoard(BoardImplTests.VALID_BOARD_NAME, TeamImplTests.VALID_TEAM_NAME);
        repository.createNewBug(BugsImplTests.VALID_BUG_TITLE,
                BugsImplTests.VALID_BUG_DESCRIPTION,
                Priority.HIGH,
                Severity.CRITICAL,
                BugsStatus.ACTIVE,
                "Not Assigned",
                stepsToReproduce,
                BoardImplTests.VALID_BOARD_NAME);
        repository.createNewPerson(PersonImplTests.VALID_PERSON_NAME, dummyActivityHistory, dummyTasks);
        // Act, Assert
        assertDoesNotThrow(() -> repository.setBugsPriority(BugsImplTests.VALID_BUG_TITLE, Priority.LOW));
    }

    @Test
    public void setStoryAssignee_Should_SetStoryAssignee_When_ArgumentsAreValid() {
        //Arrange
        repository.createNewPerson(PersonImplTests.VALID_PERSON_NAME, dummyActivityHistory, dummyTasks);
        repository.createTeam(TeamImplTests.VALID_TEAM_NAME);
        repository.createBoard(BoardImplTests.VALID_BOARD_NAME, TeamImplTests.VALID_TEAM_NAME);
        repository.createStory(StoryImplTests.VALID_STORY_TITLE,
                StoryImplTests.VALID_STORY_DESCRIPTION,
                Priority.HIGH,
                Size.LARGE,
                Status.INPROGRESS,
                "Not Assigned",
                BoardImplTests.VALID_BOARD_NAME);
        repository.createNewPerson(PersonImplTests.VALID_PERSON_NAME, dummyActivityHistory, dummyTasks);
        // Act, Assert
        assertDoesNotThrow(() -> repository.setStoryAssignee(StoryImplTests.VALID_STORY_TITLE, PersonImplTests.VALID_PERSON_NAME));
    }

    @Test
    public void setStoryStatus_Should_SetStoryStatus_When_ArgumentsAreValid() {
        //Arrange
        repository.createNewPerson(PersonImplTests.VALID_PERSON_NAME, dummyActivityHistory, dummyTasks);
        repository.createTeam(TeamImplTests.VALID_TEAM_NAME);
        repository.createBoard(BoardImplTests.VALID_BOARD_NAME, TeamImplTests.VALID_TEAM_NAME);
        repository.createStory(StoryImplTests.VALID_STORY_TITLE,
                StoryImplTests.VALID_STORY_DESCRIPTION,
                Priority.HIGH,
                Size.LARGE,
                Status.INPROGRESS,
                "Not Assigned",
                BoardImplTests.VALID_BOARD_NAME);
        repository.createNewPerson(PersonImplTests.VALID_PERSON_NAME, dummyActivityHistory, dummyTasks);
        // Act, Assert
        assertDoesNotThrow(() -> repository.setStoryStatus(StoryImplTests.VALID_STORY_TITLE, Status.DONE));
    }

    @Test
    public void setStorySize_Should_SetStorySize_When_ArgumentsAreValid() {
        //Arrange
        repository.createNewPerson(PersonImplTests.VALID_PERSON_NAME, dummyActivityHistory, dummyTasks);
        repository.createTeam(TeamImplTests.VALID_TEAM_NAME);
        repository.createBoard(BoardImplTests.VALID_BOARD_NAME, TeamImplTests.VALID_TEAM_NAME);
        repository.createStory(StoryImplTests.VALID_STORY_TITLE,
                StoryImplTests.VALID_STORY_DESCRIPTION,
                Priority.HIGH,
                Size.LARGE,
                Status.INPROGRESS,
                "Not Assigned",
                BoardImplTests.VALID_BOARD_NAME);
        repository.createNewPerson(PersonImplTests.VALID_PERSON_NAME, dummyActivityHistory, dummyTasks);
        // Act, Assert
        assertDoesNotThrow(() -> repository.setStorySize(StoryImplTests.VALID_STORY_TITLE, Size.SMALL));
    }

    @Test
    public void setStoryPriority_Should_SetStoryPriority_When_ArgumentsAreValid() {
        //Arrange
        repository.createNewPerson(PersonImplTests.VALID_PERSON_NAME, dummyActivityHistory, dummyTasks);
        repository.createTeam(TeamImplTests.VALID_TEAM_NAME);
        repository.createBoard(BoardImplTests.VALID_BOARD_NAME, TeamImplTests.VALID_TEAM_NAME);
        repository.createStory(StoryImplTests.VALID_STORY_TITLE,
                StoryImplTests.VALID_STORY_DESCRIPTION,
                Priority.HIGH,
                Size.LARGE,
                Status.INPROGRESS,
                "Not Assigned",
                BoardImplTests.VALID_BOARD_NAME);
        repository.createNewPerson(PersonImplTests.VALID_PERSON_NAME, dummyActivityHistory, dummyTasks);
        // Act, Assert
        assertDoesNotThrow(() -> repository.setStoryPriority(StoryImplTests.VALID_STORY_TITLE, Priority.LOW));
    }

    @Test
    public void setFeedbackStatus_Should_SetFeedbackStatus_When_ArgumentsAreValid() {
        //Arrange
        repository.createNewPerson(PersonImplTests.VALID_PERSON_NAME, dummyActivityHistory, dummyTasks);
        repository.createTeam(TeamImplTests.VALID_TEAM_NAME);
        repository.createBoard(BoardImplTests.VALID_BOARD_NAME, TeamImplTests.VALID_TEAM_NAME);
        repository.createFeedback(FeedbackImplTests.FEEDBACK_VALID_TITLE,
                FeedbackImplTests.FEEDBACK_VALID_DESCRIPTION,
                9,
                FeedbackStatus.DONE,
                BoardImplTests.VALID_BOARD_NAME);
        repository.createNewPerson(PersonImplTests.VALID_PERSON_NAME, dummyActivityHistory, dummyTasks);
        // Act, Assert
        assertDoesNotThrow(() -> repository.setFeedbackStatus(FeedbackImplTests.FEEDBACK_VALID_TITLE, FeedbackStatus.UNSCHEDULED));
    }

    @Test
    public void setFeedbackRating_Should_SetFeedbackRating_When_ArgumentsAreValid() {
        //Arrange
        repository.createNewPerson(PersonImplTests.VALID_PERSON_NAME, dummyActivityHistory, dummyTasks);
        repository.createTeam(TeamImplTests.VALID_TEAM_NAME);
        repository.createBoard(BoardImplTests.VALID_BOARD_NAME, TeamImplTests.VALID_TEAM_NAME);
        repository.createFeedback(FeedbackImplTests.FEEDBACK_VALID_TITLE,
                FeedbackImplTests.FEEDBACK_VALID_DESCRIPTION,
                9,
                FeedbackStatus.DONE,
                BoardImplTests.VALID_BOARD_NAME);
        repository.createNewPerson(PersonImplTests.VALID_PERSON_NAME, dummyActivityHistory, dummyTasks);
        // Act, Assert
        assertDoesNotThrow(() -> repository.setFeedbackRating(FeedbackImplTests.FEEDBACK_VALID_TITLE, 8));
    }
    @Test
    public void addFeedbackComment_Should_AddFeedbackComment_When_ArgumentsAreValid() {
        //Arrange
        repository.createNewPerson(PersonImplTests.VALID_PERSON_NAME, dummyActivityHistory, dummyTasks);
        repository.createTeam(TeamImplTests.VALID_TEAM_NAME);
        repository.createBoard(BoardImplTests.VALID_BOARD_NAME, TeamImplTests.VALID_TEAM_NAME);
        repository.createFeedback(FeedbackImplTests.FEEDBACK_VALID_TITLE,
                FeedbackImplTests.FEEDBACK_VALID_DESCRIPTION,
                9,
                FeedbackStatus.DONE,
                BoardImplTests.VALID_BOARD_NAME);
        repository.createNewPerson(PersonImplTests.VALID_PERSON_NAME, dummyActivityHistory, dummyTasks);
        Comments comment = repository.createComment(repository.findPersonByName(PersonImplTests.VALID_PERSON_NAME), FeedbackImplTests.FEEDBACK_VALID_DESCRIPTION);
        // Act, Assert
//        assertDoesNotThrow(() -> repository.addFeedbackComment(FeedbackImplTests.FEEDBACK_VALID_TITLE, comment));
        Assertions.assertThrows(IllegalArgumentException.class,
                () -> repository.addFeedbackComment(FeedbackImplTests.FEEDBACK_VALID_TITLE, comment));
    }


//    @Test
//    public void setFeedbackRatingShould_SetNewRating() {
//        // Arrange
//        Feedback feedback = new FeedbackImpl(
//                1,
//                FeedbackImplTests.FEEDBACK_VALID_TITLE,
//                FeedbackImplTests.FEEDBACK_VALID_DESCRIPTION,
//                FeedbackImplTests.FEEDBACK_VALID_RATING,
//                FeedbackStatus.NEW,
//                FeedbackImplTests.VALID_BOARD_NAME);
//        // Act
//        feedback.setRating(3);
//
//        //Assert
//        Assertions.assertNotSame(FeedbackImplTests.FEEDBACK_VALID_RATING, feedback.getRating());
//    }
//    @Test
//    public void createAirplane_Should_AddAirplaneToList() {
//        // Arrange
//        repository.createAirplane(
//                AirplaneTests.VALID_AIRPLANE_PASSENGER_CAPACITY,
//                AirplaneTests.VALID_AIRPLANE_PRICE,
//                true);
//
//        // Act, Assert
//        assertEquals(1, repository.getVehicles().size());
//    }
//
//    @Test
//    public void createBus_Should_AddBusToList() {
//        // Arrange
//        repository.createBus(
//                BusTests.VALID_BUS_PASSENGER_CAPACITY,
//                BusTests.VALID_BUS_PRICE
//        );
//
//        // Act, Assert
//        assertEquals(1, repository.getVehicles().size());
//    }
//
//    @Test
//    public void createTrain_Should_AddTrainToList() {
//        // Arrange
//        repository.createTrain(
//                TrainTests.VALID_TRAIN_PASSENGER_CAPACITY,
//                TrainTests.VALID_TRAIN_PRICE,
//                TrainTests.VALID_CARTS);
//
//        // Act, Assert
//        assertEquals(1, repository.getVehicles().size());
//    }
//
//    @Test
//    public void createJourney_Should_AddJourneyToList() {
//        // Arrange
//        repository.createJourney(
//                JourneyTests.VALID_START_LOCATION_NAME,
//                JourneyTests.VALID_DESTINATION_NAME,
//                JourneyTests.VALID_DISTANCE,
//                AirplaneTests.initializeVehicle()
//        );
//
//        // Act, Assert
//        assertEquals(1, repository.getJourneys().size());
//    }
//
//    @Test
//    public void createTicket_Should_AddTicketToList() {
//        // Arrange
//        repository.createTicket(
//                JourneyTests.initializeJourney(),
//                2.5
//        );
//
//        // Act, Assert
//        assertEquals(1, repository.getTickets().size());
//    }
}
