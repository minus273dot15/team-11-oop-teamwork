package com.company.oop.taskmanagement.tests.core;

import com.company.oop.taskmanagement.commands.contracts.Command;
import com.company.oop.taskmanagement.commands.enums.CommandType;
import com.company.oop.taskmanagement.core.CommandFactoryImpl;
import com.company.oop.taskmanagement.core.TaskManagmentRepositoryImpl;
import com.company.oop.taskmanagement.core.contracts.TaskManagmentRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CommandFactoryImplTest {

    private CommandFactoryImpl factory;

    private TaskManagmentRepository repository;

    @BeforeEach
    void setUp() {
        this.repository = new TaskManagmentRepositoryImpl();
        factory = new CommandFactoryImpl();
    }

//    @Test
//    void testCreateCommandFromCommandName() {
//        Command command = factory.createCommandFromCommandName("CREATEPERSON", repository);
//        assertNotNull(command);
//        assertEquals(CommandType.CREATEPERSON, command.getname());
//    }

    @Test
    void testCreateCommandFromInvalidCommandName() {
        assertThrows(IllegalArgumentException.class, () -> factory.createCommandFromCommandName("INVALIDCOMMAND", repository));
    }

    @Test
    void testCreateCommandFromNullCommandName() {
        assertThrows(NullPointerException.class, () -> factory.createCommandFromCommandName(null, repository));
    }

}
