package com.company.oop.taskmanagement.tests.commands.setters;

import com.company.oop.taskmanagement.commands.contracts.Command;
import com.company.oop.taskmanagement.commands.setters.SetFeedbackStatusCommand;
import com.company.oop.taskmanagement.core.TaskManagmentRepositoryImpl;
import com.company.oop.taskmanagement.core.contracts.TaskManagmentRepository;
import com.company.oop.taskmanagement.models.items.FeedbackImpl;
import com.company.oop.taskmanagement.models.items.contracts.Feedback;
import com.company.oop.taskmanagement.models.items.enums.FeedbackStatus;
import com.company.oop.taskmanagement.tests.models.BoardImplTests;
import com.company.oop.taskmanagement.tests.models.items.FeedbackImplTests;
import com.company.oop.taskmanagement.tests.utils.TestUtilities;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class SetFeedbackStatusCommandTests {
    public static final int EXPECTED_NUMBER_OF_ARGUMENTS = 2;//id ,status expected
    private Command command;
    private TaskManagmentRepository repository;
    @BeforeEach
    public void before() {
        this.repository = new TaskManagmentRepositoryImpl();
        this.command = new SetFeedbackStatusCommand(repository);
        repository.createFeedback(FeedbackImplTests.FEEDBACK_VALID_TITLE,
                FeedbackImplTests.FEEDBACK_VALID_DESCRIPTION,
                9,
                FeedbackStatus.DONE,
                BoardImplTests.VALID_BOARD_NAME);
    }

    @Test
    public void should_ThrowException_When_ArgumentCountDifferentThanExpected() {
        // Arrange
        List<String> params = TestUtilities.getList(EXPECTED_NUMBER_OF_ARGUMENTS - 1);

        // Act, Assert
        assertThrows(IllegalArgumentException.class, () -> command.execute(params));
    }

    @Test
    public void execute_Should_ThrowException_When_enumNotEnum() {
        // Arrange
        List<String> params = List.of(FeedbackImplTests.FEEDBACK_VALID_TITLE, "NotEnum");
        // Act, Assert
        Assertions.assertThrows(IllegalArgumentException.class, () -> command.execute(params));
    }

    @Test
    void execute_withValidParameters_returnsSuccessMessage() {
        // Arrange
        List<String> parameters = Arrays.asList(FeedbackImplTests.FEEDBACK_VALID_TITLE, String.valueOf(FeedbackStatus.UNSCHEDULED));

        // Act
        String result = command.execute(parameters);

        // Assert

        assertEquals(String.format("Status Unscheduled for Feedback \"%s\" with FeedbackID 1 was set.", FeedbackImplTests.FEEDBACK_VALID_TITLE), result);
    }

}
