package com.company.oop.taskmanagement.tests.commands.setters;

import com.company.oop.taskmanagement.commands.setters.SetBugsPriorityCommand;
import com.company.oop.taskmanagement.commands.setters.SetStoryPriorityCommand;
import com.company.oop.taskmanagement.core.TaskManagmentRepositoryImpl;
import com.company.oop.taskmanagement.core.contracts.TaskManagmentRepository;
import com.company.oop.taskmanagement.models.items.enums.*;
import com.company.oop.taskmanagement.tests.models.BoardImplTests;
import com.company.oop.taskmanagement.tests.models.items.BugsImplTests;
import com.company.oop.taskmanagement.tests.models.items.StoryImplTests;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

import static com.company.oop.taskmanagement.utils.DummyArrayLists.stepsToReproduce;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class SetStoryPriorityCommandTests {


    private TaskManagmentRepository taskManagmentRepository;
    private SetStoryPriorityCommand setStoryPriorityCommand;

    @BeforeEach
    void setUp() {
        taskManagmentRepository = new TaskManagmentRepositoryImpl();
        setStoryPriorityCommand = new SetStoryPriorityCommand(taskManagmentRepository);
        taskManagmentRepository.createStory(StoryImplTests.VALID_STORY_TITLE,
                StoryImplTests.VALID_STORY_DESCRIPTION,
                Priority.HIGH,
                Size.LARGE,
                Status.INPROGRESS,
                "Not Assigned",
                BoardImplTests.VALID_BOARD_NAME);
    }

    @Test
    void execute_withValidParameters_returnsSuccessMessage() {
        // Arrange
        List<String> parameters = Arrays.asList(StoryImplTests.VALID_STORY_TITLE, String.valueOf(Priority.LOW));

        // Act
        String result = setStoryPriorityCommand.execute(parameters);

        // Assert

        assertEquals(String.format("Story with StoryID %d changed priority to %s", 1, String.valueOf(Priority.LOW)), result);
    }

    @Test
    void execute_withInvalidNumberOfArguments_throwsIllegalArgumentException() {
        // Arrange
        List<String> parameters = Arrays.asList(BugsImplTests.VALID_BUG_TITLE);

        // Act & Assert
        assertThrows(IllegalArgumentException.class,
                () -> setStoryPriorityCommand.execute(parameters));
    }

    @Test
    void execute_withInvalidSeverity_throwsIllegalArgumentException() {
        // Arrange
        List<String> parameters = Arrays.asList(BugsImplTests.VALID_BUG_TITLE, "invalid_severity");

        // Act & Assert
        assertThrows(IllegalArgumentException.class,
                () -> setStoryPriorityCommand.execute(parameters));
    }
}
