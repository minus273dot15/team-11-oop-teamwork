package com.company.oop.taskmanagement.tests.commands.setters;

import com.company.oop.taskmanagement.commands.contracts.Command;
import com.company.oop.taskmanagement.commands.setters.SetStoryAssigneeCommand;
import com.company.oop.taskmanagement.core.TaskManagmentRepositoryImpl;
import com.company.oop.taskmanagement.core.contracts.TaskManagmentRepository;
import com.company.oop.taskmanagement.exceptions.ElementNotFoundException;
import com.company.oop.taskmanagement.models.items.enums.*;
import com.company.oop.taskmanagement.tests.models.BoardImplTests;
import com.company.oop.taskmanagement.tests.models.PersonImplTests;
import com.company.oop.taskmanagement.tests.models.items.BugsImplTests;
import com.company.oop.taskmanagement.tests.models.items.StoryImplTests;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static com.company.oop.taskmanagement.utils.DummyArrayLists.*;
import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class SetStoryAssigneeCommandTests {
    private TaskManagmentRepository repository;
    private Command command;

    @BeforeEach
    void setUp() {
        repository = new TaskManagmentRepositoryImpl();
        command = new SetStoryAssigneeCommand(repository);
        repository.createStory(StoryImplTests.VALID_STORY_TITLE,
                StoryImplTests.VALID_STORY_DESCRIPTION,
                Priority.HIGH,
                Size.LARGE,
                Status.INPROGRESS,
                "Not Assigned",
                BoardImplTests.VALID_BOARD_NAME);
        repository.createNewPerson(PersonImplTests.VALID_PERSON_NAME, dummyActivityHistory, dummyTasks);
    }

    @Test
    void execute_whenGivenValidParameters_shouldSetAssigneeAndLogEvent() {
        // Arrange
        List<String> parameters = Arrays.asList(StoryImplTests.VALID_STORY_TITLE, PersonImplTests.VALID_PERSON_NAME);

        // Act
        String result = command.execute(parameters);

        // Assert
        assertEquals(String.format("Story \"%s\" was assigned to %s!", StoryImplTests.VALID_STORY_TITLE, PersonImplTests.VALID_PERSON_NAME), result);
        assertEquals(PersonImplTests.VALID_PERSON_NAME, repository.findStoryByName(StoryImplTests.VALID_STORY_TITLE).getAssignee());
        assertEquals(1, repository.getHistory().size());
        assertEquals(repository.findPersonByName(PersonImplTests.VALID_PERSON_NAME).getName(), PersonImplTests.VALID_PERSON_NAME);
        assertEquals(repository.findStoryByName(StoryImplTests.VALID_STORY_TITLE).getTitle(), StoryImplTests.VALID_STORY_TITLE);
    }

    @Test
    void execute_whenBugDoesNotExist_shouldThrowException() {
        // Arrange
        List<String> parameters = Arrays.asList("non_existing_bug", PersonImplTests.VALID_PERSON_NAME);

        // Act & Assert
        assertThrows(ElementNotFoundException.class, () -> command.execute(parameters));
    }

    @Test
    void execute_whenAssigneeDoesNotExist_shouldThrowException() {
        // Arrange
        List<String> parameters = Arrays.asList(BugsImplTests.VALID_BUG_TITLE, "invalid_assignee_name");

        // Act & Assert
        assertThrows(ElementNotFoundException.class, () -> command.execute(parameters));
    }

    @Test
    void execute_whenCalledWithIncorrectNumberOfArguments_shouldThrowException() {
        // Arrange
        List<String> parameters = new ArrayList<>();

        // Act & Assert
        assertThrows(IllegalArgumentException.class, () -> command.execute(parameters));

        parameters.add(BugsImplTests.VALID_BUG_TITLE);
        assertThrows(IllegalArgumentException.class, () -> command.execute(parameters));

        parameters.add(PersonImplTests.VALID_PERSON_NAME);
        parameters.add("extra_argument");
        assertThrows(IllegalArgumentException.class, () -> command.execute(parameters));
    }
}
