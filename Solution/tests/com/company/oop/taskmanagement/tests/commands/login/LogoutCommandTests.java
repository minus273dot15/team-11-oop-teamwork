package com.company.oop.taskmanagement.tests.commands.login;

import com.company.oop.taskmanagement.commands.contracts.Command;
import com.company.oop.taskmanagement.commands.login.LoginCommand;
import com.company.oop.taskmanagement.commands.login.LogoutCommand;
import com.company.oop.taskmanagement.core.TaskManagmentRepositoryImpl;
import com.company.oop.taskmanagement.core.contracts.TaskManagmentRepository;
import com.company.oop.taskmanagement.models.contracts.Person;
import com.company.oop.taskmanagement.models.items.FeedbackImpl;
import com.company.oop.taskmanagement.models.items.contracts.Feedback;
import com.company.oop.taskmanagement.models.items.enums.FeedbackStatus;
import com.company.oop.taskmanagement.tests.models.PersonImplTests;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static com.company.oop.taskmanagement.utils.DummyArrayLists.dummyActivityHistory;
import static com.company.oop.taskmanagement.utils.DummyArrayLists.dummyTasks;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class LogoutCommandTests {
    private Command command, firstcommand;
    private TaskManagmentRepository repository;

    @BeforeEach
    public void before() {
        this.repository = new TaskManagmentRepositoryImpl();
        this.firstcommand = new LoginCommand(repository);
        this.command = new LogoutCommand(repository);
    }

    @Test
    public void should_ThrowException_When_UserNotLoggedIn() {
        // Arrange
        List<String> params = new ArrayList<>();
        params.add(PersonImplTests.VALID_PERSON_NAME);
        repository.createNewPerson(
                PersonImplTests.VALID_PERSON_NAME,
                dummyActivityHistory,
                dummyTasks);
        // Act, Assert
        assertThrows(IllegalArgumentException.class, () -> command.execute(params));
    }
    @Test
    public void should_ReturnSuccessMsg_When_UserLoggedOut() {
        // Arrange
        List<String> params = new ArrayList<>();
        params.add(PersonImplTests.VALID_PERSON_NAME);
        repository.createNewPerson(
                PersonImplTests.VALID_PERSON_NAME,
                dummyActivityHistory,
                dummyTasks);
        firstcommand.execute(params);
        // Act
        String result = command.execute(params);
        // Assert
        assertTrue(result.contains("You are now interacting as anonymous"));
    }


}
