package com.company.oop.taskmanagement.tests.commands.creation;

import com.company.oop.taskmanagement.commands.contracts.Command;
import com.company.oop.taskmanagement.commands.creation.CreateFeedbackCommand;
import com.company.oop.taskmanagement.core.TaskManagmentRepositoryImpl;
import com.company.oop.taskmanagement.core.contracts.TaskManagmentRepository;
import com.company.oop.taskmanagement.exceptions.InvalidUserInputException;
import com.company.oop.taskmanagement.models.items.enums.FeedbackStatus;
import com.company.oop.taskmanagement.tests.models.items.FeedbackImplTests;
import com.company.oop.taskmanagement.tests.utils.TestUtilities;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class CreateFeedbackCommandTest {

    public static final int EXPECTED_NUMBER_OF_params = 5;

    private Command command;
    private TaskManagmentRepository repository;

    @BeforeEach
    public void before() {
        this.repository = new TaskManagmentRepositoryImpl();
        this.command = new CreateFeedbackCommand(repository);
    }

    @Test
    public void should_ThrowException_When_ArgumentCountDifferentThanExpected() {
        // Arrange
        List<String> params = TestUtilities.getList(EXPECTED_NUMBER_OF_params - 1);

        // Act, Assert
        assertThrows(InvalidUserInputException.class, () -> command.execute(params));
    }

    @Test
    public void execute_Should_ThrowException_When_RatingNotNumber() {
        // Arrange
        List<String> params = List.of(
                FeedbackImplTests.FEEDBACK_VALID_TITLE,
                FeedbackImplTests.FEEDBACK_VALID_DESCRIPTION,
                FeedbackImplTests.FEEDBACK_INVALID_RATING,
                String.valueOf(FeedbackStatus.NEW),
                FeedbackImplTests.VALID_BOARD_NAME);

        // Act, Assert
        Assertions.assertThrows(IllegalArgumentException.class, () -> command.execute(params));
    }

    @Test
    public void execute_Should_AddNewFeedback_When_PassedValidInput() {
        // Arrange
        List<String> params = List.of(
                FeedbackImplTests.FEEDBACK_VALID_TITLE,
                FeedbackImplTests.FEEDBACK_VALID_DESCRIPTION,
                String.valueOf(FeedbackImplTests.FEEDBACK_VALID_RATING),
                FeedbackImplTests.VALID_BOARD_NAME);

        // Act
        command.execute(params);

        // Assert
        Assertions.assertEquals(1, repository.getFeedbacks().size());
    }

}