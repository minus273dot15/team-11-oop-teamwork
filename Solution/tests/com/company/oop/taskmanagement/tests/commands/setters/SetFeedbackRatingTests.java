package com.company.oop.taskmanagement.tests.commands.setters;

import com.company.oop.taskmanagement.commands.contracts.Command;
import com.company.oop.taskmanagement.commands.setters.SetFeedbackRatingCommand;
import com.company.oop.taskmanagement.core.TaskManagmentRepositoryImpl;
import com.company.oop.taskmanagement.core.contracts.TaskManagmentRepository;
import com.company.oop.taskmanagement.exceptions.InvalidUserInputException;
import com.company.oop.taskmanagement.models.items.enums.FeedbackStatus;
import com.company.oop.taskmanagement.tests.models.BoardImplTests;
import com.company.oop.taskmanagement.tests.models.items.FeedbackImplTests;
import com.company.oop.taskmanagement.tests.utils.TestUtilities;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class SetFeedbackRatingTests {
    public static final int EXPECTED_NUMBER_OF_ARGUMENTS = 2;
    private Command command;
    private TaskManagmentRepository repository;
    @BeforeEach
    public void before() {
        this.repository = new TaskManagmentRepositoryImpl();
        this.command = new SetFeedbackRatingCommand(repository);
        repository.createFeedback(FeedbackImplTests.FEEDBACK_VALID_TITLE,
                FeedbackImplTests.FEEDBACK_VALID_DESCRIPTION,
                9,
                FeedbackStatus.DONE,
                BoardImplTests.VALID_BOARD_NAME);
    }

    @Test
    void execute_withValidParameters_returnsSuccessMessage() {
        // Arrange
        List<String> parameters = Arrays.asList(FeedbackImplTests.FEEDBACK_VALID_TITLE, String.valueOf(5));

        // Act
        String result = command.execute(parameters);

        // Assert

        assertEquals(String.format("Rating %d for Feedback \"%s\" with FeedbackID 1 was set.", 5, FeedbackImplTests.FEEDBACK_VALID_TITLE), result);
    }

    @Test
    public void should_ThrowException_When_ArgumentCountDifferentThanExpected() {
        // Arrange
        List<String> params = TestUtilities.getList(EXPECTED_NUMBER_OF_ARGUMENTS - 1);

        // Act, Assert
        assertThrows(IllegalArgumentException.class, () -> command.execute(params));
    }

    @Test
    public void execute_Should_ThrowException_When_ratingNotNumber() {
        // Arrange
        List<String> params = List.of(FeedbackImplTests.FEEDBACK_VALID_TITLE, "NAWNAWNAW");
        // Act, Assert
        Assertions.assertThrows(InvalidUserInputException.class, () -> command.execute(params));
    }


}
