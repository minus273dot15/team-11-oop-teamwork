package com.company.oop.taskmanagement.tests.commands.setters;

import com.company.oop.taskmanagement.commands.setters.SetBugSeverityCommand;
import com.company.oop.taskmanagement.core.TaskManagmentRepositoryImpl;
import com.company.oop.taskmanagement.core.contracts.TaskManagmentRepository;
import com.company.oop.taskmanagement.models.items.enums.BugsStatus;
import com.company.oop.taskmanagement.models.items.enums.Priority;
import com.company.oop.taskmanagement.models.items.enums.Severity;
import com.company.oop.taskmanagement.tests.models.BoardImplTests;
import com.company.oop.taskmanagement.tests.models.items.BugsImplTests;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

import static com.company.oop.taskmanagement.utils.DummyArrayLists.stepsToReproduce;
import static org.junit.jupiter.api.Assertions.*;

class SetBugSeverityCommandTests {

    private TaskManagmentRepository taskManagmentRepository;
    private SetBugSeverityCommand setBugSeverityCommand;

    @BeforeEach
    void setUp() {
        taskManagmentRepository = new TaskManagmentRepositoryImpl();
        setBugSeverityCommand = new SetBugSeverityCommand(taskManagmentRepository);
        taskManagmentRepository.createNewBug(BugsImplTests.VALID_BUG_TITLE,
                BugsImplTests.VALID_BUG_DESCRIPTION,
                Priority.HIGH,
                Severity.CRITICAL,
                BugsStatus.ACTIVE,
                "Not Assigned",
                stepsToReproduce,
                BoardImplTests.VALID_BOARD_NAME);
    }

    @Test
    void execute_withValidParameters_returnsSuccessMessage() {
        // Arrange
        List<String> parameters = Arrays.asList(BugsImplTests.VALID_BUG_TITLE, String.valueOf(Severity.LOW));

        // Act
        String result = setBugSeverityCommand.execute(parameters);

        // Assert

        assertEquals("Severity for Bug with BugID 1 was set to LOW", result);
    }

    @Test
    void execute_withInvalidNumberOfArguments_throwsIllegalArgumentException() {
        // Arrange
        List<String> parameters = Arrays.asList("Bug1");

        // Act & Assert
        assertThrows(IllegalArgumentException.class,
                () -> setBugSeverityCommand.execute(parameters));
    }

    @Test
    void execute_withInvalidSeverity_throwsIllegalArgumentException() {
        // Arrange
        List<String> parameters = Arrays.asList("Bug1", "invalid_severity");

        // Act & Assert
        assertThrows(IllegalArgumentException.class,
                () -> setBugSeverityCommand.execute(parameters));
    }
}
