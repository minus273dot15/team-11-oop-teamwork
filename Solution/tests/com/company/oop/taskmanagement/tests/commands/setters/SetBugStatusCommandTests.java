package com.company.oop.taskmanagement.tests.commands.setters;

import com.company.oop.taskmanagement.commands.setters.SetBugStatusCommand;
import com.company.oop.taskmanagement.core.TaskManagmentRepositoryImpl;
import com.company.oop.taskmanagement.core.contracts.TaskManagmentRepository;
import com.company.oop.taskmanagement.models.items.enums.BugsStatus;
import com.company.oop.taskmanagement.models.items.enums.Priority;
import com.company.oop.taskmanagement.models.items.enums.Severity;
import com.company.oop.taskmanagement.tests.models.BoardImplTests;
import com.company.oop.taskmanagement.tests.models.items.BugsImplTests;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

import static com.company.oop.taskmanagement.utils.DummyArrayLists.stepsToReproduce;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class SetBugStatusCommandTests {

    private TaskManagmentRepository taskManagmentRepository;
    private SetBugStatusCommand setBugStatusCommand;

    @BeforeEach
    void setUp() {
        taskManagmentRepository = new TaskManagmentRepositoryImpl();
        setBugStatusCommand = new SetBugStatusCommand(taskManagmentRepository);
        taskManagmentRepository.createNewBug(BugsImplTests.VALID_BUG_TITLE,
                BugsImplTests.VALID_BUG_DESCRIPTION,
                Priority.HIGH,
                Severity.CRITICAL,
                BugsStatus.ACTIVE,
                "Not Assigned",
                stepsToReproduce,
                BoardImplTests.VALID_BOARD_NAME);
    }

    @Test
    void execute_withValidParameters_returnsSuccessMessage() {
        // Arrange
        List<String> parameters = Arrays.asList(BugsImplTests.VALID_BUG_TITLE, String.valueOf(BugsStatus.FIXED));

        // Act
        String result = setBugStatusCommand.execute(parameters);

        // Assert

        assertEquals("BugStatus for Bug with BugID 1 was set to FIXED", result);
    }

    @Test
    void execute_withInvalidNumberOfArguments_throwsIllegalArgumentException() {
        // Arrange
        List<String> parameters = Arrays.asList(BugsImplTests.VALID_BUG_TITLE);

        // Act & Assert
        assertThrows(IllegalArgumentException.class,
                () -> setBugStatusCommand.execute(parameters));
    }

    @Test
    void execute_withInvalidSeverity_throwsIllegalArgumentException() {
        // Arrange
        List<String> parameters = Arrays.asList(BugsImplTests.VALID_BUG_TITLE, "invalid_severity");

        // Act & Assert
        assertThrows(IllegalArgumentException.class,
                () -> setBugStatusCommand.execute(parameters));
    }
}
