package com.company.oop.taskmanagement.tests.commands.login;

import com.company.oop.taskmanagement.commands.contracts.Command;
import com.company.oop.taskmanagement.commands.login.LoginCommand;
import com.company.oop.taskmanagement.core.TaskManagmentRepositoryImpl;
import com.company.oop.taskmanagement.core.contracts.TaskManagmentRepository;
import com.company.oop.taskmanagement.tests.models.PersonImplTests;
import com.company.oop.taskmanagement.tests.utils.TestUtilities;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static com.company.oop.taskmanagement.utils.DummyArrayLists.dummyActivityHistory;
import static com.company.oop.taskmanagement.utils.DummyArrayLists.dummyTasks;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class LoginCommandTests {

    public static final int EXPECTED_NUMBER_OF_ARGUMENTS = 1;

    private Command command;
    private TaskManagmentRepository repository;

    @BeforeEach
    public void before() {
        this.repository = new TaskManagmentRepositoryImpl();
        this.command = new LoginCommand(repository);
    }

    @Test
    public void should_ThrowException_When_ArgumentCountDifferentThanExpected() {
        // Arrange
        List<String> params = TestUtilities.getList(EXPECTED_NUMBER_OF_ARGUMENTS - 1);

        // Act, Assert
        assertThrows(IllegalArgumentException.class, () -> command.execute(params));
    }

    @Test
    public void should_ThrowException_When_UserAlreadyLogged() {
        // Arrange
        List<String> params = new ArrayList<>();
        params.add(PersonImplTests.VALID_PERSON_NAME);
        repository.createNewPerson(
                PersonImplTests.VALID_PERSON_NAME,
                dummyActivityHistory,
                dummyTasks);
        command.execute(params);
        // Act, Assert
        assertThrows(IllegalArgumentException.class, () -> command.execute(params));
    }
}
