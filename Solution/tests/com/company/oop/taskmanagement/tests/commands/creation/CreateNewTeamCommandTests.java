package com.company.oop.taskmanagement.tests.commands.creation;

import com.company.oop.taskmanagement.commands.contracts.Command;
import com.company.oop.taskmanagement.commands.creation.CreateNewPersonCommand;
import com.company.oop.taskmanagement.commands.creation.CreateNewTeamCommand;
import com.company.oop.taskmanagement.core.TaskManagmentRepositoryImpl;
import com.company.oop.taskmanagement.core.contracts.TaskManagmentRepository;
import com.company.oop.taskmanagement.tests.models.PersonImplTests;
import com.company.oop.taskmanagement.tests.models.TeamImplTests;
import com.company.oop.taskmanagement.tests.utils.TestUtilities;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertThrows;

public class CreateNewTeamCommandTests {

    public static final int EXPECTED_NUMBER_OF_params = 1;

    private Command command;
    private TaskManagmentRepository repository;

    @BeforeEach
    public void before() {
        this.repository = new TaskManagmentRepositoryImpl();
        this.command = new CreateNewTeamCommand(repository);
    }

    @Test
    public void should_ThrowException_When_ArgumentCountDifferentThanExpected() {
        // Arrange
        List<String> params = TestUtilities.getList(EXPECTED_NUMBER_OF_params - 1);

        // Act, Assert
        assertThrows(IllegalArgumentException.class, () -> command.execute(params));
    }

    @Test
    public void execute_Should_ThrowException_When_TeamAlreadyExists() {
        // Arrange
        List<String> params = List.of(
                TeamImplTests.VALID_TEAM_NAME);

        // Act
        command.execute(params);
        // Assert
        Assertions.assertThrows(IllegalArgumentException.class, () -> command.execute(params));
    }

    @Test
    public void execute_Should_CreateNewTeam_When_PassedValidInput() {
        // Arrange
        List<String> params = List.of(
                TeamImplTests.VALID_TEAM_NAME);

        // Act
        command.execute(params);

        // Assert
        Assertions.assertEquals(1, repository.getTeams().size());
    }
}
