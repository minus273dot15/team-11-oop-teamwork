package com.company.oop.taskmanagement.tests.commands.adders;

import com.company.oop.taskmanagement.core.TaskManagmentRepositoryImpl;
import com.company.oop.taskmanagement.core.contracts.TaskManagmentRepository;
import com.company.oop.taskmanagement.models.contracts.Person;
import com.company.oop.taskmanagement.models.contracts.Team;
import com.company.oop.taskmanagement.tests.models.PersonImplTests;
import com.company.oop.taskmanagement.tests.models.TeamImplTests;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;

import static com.company.oop.taskmanagement.tests.utils.TestUtilities.getList;

public class AddPersonToTeamCommandTests {
    public static final int EXPECTED_NUMBER_OF_ARGUMENTS = 2;
    private TaskManagmentRepository repository;
    private Person person;
    private Team team;

    @BeforeEach
    public void before() {
        repository = new TaskManagmentRepositoryImpl();
        person = PersonImplTests.initializePerson();
        team = TeamImplTests.initializeTeam();
    }

//    @Test
//    public void should_ThrowException_When_ArgumentCountDifferentThanExpected() {
//        // Arrange
//        List<String> params = getList(EXPECTED_NUMBER_OF_ARGUMENTS + 1);
//
//        // Act, Assert
//        Assertions.assertThrows(IllegalArgumentException.class, () -> repository.addPersonToTeam(params));
//    }
}
