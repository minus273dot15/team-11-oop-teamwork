package com.company.oop.taskmanagement.tests.commands.listing;

import com.company.oop.taskmanagement.commands.listing.ListAllFeedbacksCommand;
import com.company.oop.taskmanagement.core.TaskManagmentRepositoryImpl;
import com.company.oop.taskmanagement.core.contracts.TaskManagmentRepository;
import com.company.oop.taskmanagement.models.items.contracts.Feedback;
import com.company.oop.taskmanagement.models.items.enums.FeedbackStatus;
import com.company.oop.taskmanagement.tests.models.BoardImplTests;
import com.company.oop.taskmanagement.tests.models.TeamImplTests;
import com.company.oop.taskmanagement.tests.models.items.FeedbackImplTests;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class ListAllFeedbacksCommandTests {

    private ListAllFeedbacksCommand listAllFeedbacksCommand;
    private TaskManagmentRepository repository;

    @BeforeEach
    public void before() {
        repository = new TaskManagmentRepositoryImpl();
        listAllFeedbacksCommand = new ListAllFeedbacksCommand(repository);
    }

    @Test
    public void should_Execute_When_ArgumentsAreValid() {
        // Arrange
        Feedback feedback = FeedbackImplTests.initializeFeedback();

        // Act, Assert
        assertDoesNotThrow(() -> listAllFeedbacksCommand.execute(List.of(feedback.getTitle())));
    }

    @Test
    public void should_ReturnStreamToString_When_ArgumentsAreValid() {
        // Arrange
        repository.createBoard(BoardImplTests.VALID_BOARD_NAME, TeamImplTests.VALID_TEAM_NAME);
        repository.createFeedback(
                FeedbackImplTests.FEEDBACK_VALID_TITLE,
                FeedbackImplTests.FEEDBACK_VALID_DESCRIPTION,
                9,
                FeedbackStatus.DONE,
                BoardImplTests.VALID_BOARD_NAME);
        String result = listAllFeedbacksCommand.execute(List.of(repository.findFeedbackByName(FeedbackImplTests.FEEDBACK_VALID_TITLE).getTitle()));
        // Act, Assert
        assertTrue(result.contains("feedback"));
    }

}
