package com.company.oop.taskmanagement.tests.commands.setters;

import com.company.oop.taskmanagement.commands.setters.UnassignBugCommand;
import com.company.oop.taskmanagement.core.TaskManagmentRepositoryImpl;
import com.company.oop.taskmanagement.core.contracts.TaskManagmentRepository;
import com.company.oop.taskmanagement.exceptions.ElementNotFoundException;
import com.company.oop.taskmanagement.models.items.enums.BugsStatus;
import com.company.oop.taskmanagement.models.items.enums.Priority;
import com.company.oop.taskmanagement.models.items.enums.Severity;
import com.company.oop.taskmanagement.tests.models.BoardImplTests;
import com.company.oop.taskmanagement.tests.models.items.BugsImplTests;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static com.company.oop.taskmanagement.utils.DummyArrayLists.*;
import static org.junit.jupiter.api.Assertions.assertEquals;


class UnassignBugCommandTests {

    private TaskManagmentRepository taskManagmentRepository;
    private UnassignBugCommand unassignBugCommand;

    @BeforeEach
    public void setUp() {
        taskManagmentRepository = new TaskManagmentRepositoryImpl();
        taskManagmentRepository.createNewBug(BugsImplTests.VALID_BUG_TITLE,
                BugsImplTests.VALID_BUG_DESCRIPTION,
                Priority.HIGH,
                Severity.CRITICAL,
                BugsStatus.ACTIVE,
                "Not Assigned",
                stepsToReproduce,
                BoardImplTests.VALID_BOARD_NAME);
        unassignBugCommand = new UnassignBugCommand(taskManagmentRepository);
    }

    @Test
    public void execute_shouldThrowIllegalArgumentException_whenBugNameIsInvalid() {
        List<String> parameters = new ArrayList<>(Arrays.asList("NonExistentBug"));
        Assertions.assertThrows(ElementNotFoundException.class, () -> unassignBugCommand.execute(parameters));
    }

}
