package com.company.oop.taskmanagement.tests.models;

import com.company.oop.taskmanagement.models.BoardImpl;
import com.company.oop.taskmanagement.models.TeamsImpl;
import com.company.oop.taskmanagement.models.contracts.Board;
import com.company.oop.taskmanagement.models.contracts.Team;
import com.company.oop.taskmanagement.models.items.BugsImpl;
import com.company.oop.taskmanagement.models.items.FeedbackImpl;
import com.company.oop.taskmanagement.models.items.StoryImpl;
import com.company.oop.taskmanagement.models.items.contracts.Bugs;
import com.company.oop.taskmanagement.models.items.contracts.Feedback;
import com.company.oop.taskmanagement.models.items.contracts.History;
import com.company.oop.taskmanagement.models.items.contracts.Story;
import com.company.oop.taskmanagement.models.items.enums.*;
import com.company.oop.taskmanagement.tests.models.items.BugsImplTests;
import com.company.oop.taskmanagement.tests.models.items.FeedbackImplTests;
import com.company.oop.taskmanagement.tests.models.items.StoryImplTests;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static com.company.oop.taskmanagement.models.BoardImpl.BOARD_NAME_MAX_LENGTH;
import static com.company.oop.taskmanagement.models.BoardImpl.BOARD_NAME_MIN_LENGTH;
import static com.company.oop.taskmanagement.models.items.TaskImpl.DESCRIPTION_MAX_LENGTH;
import static com.company.oop.taskmanagement.tests.utils.TestUtilities.getString;
import static com.company.oop.taskmanagement.utils.DummyArrayLists.*;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class BoardImplTests {

    public static final String VALID_BOARD_NAME = getString(BOARD_NAME_MIN_LENGTH + 1);
    public static final String INVALID_BOARD_NAME = getString(BOARD_NAME_MAX_LENGTH + 1);
    public static final String VALID_BOARD_TEAM = "Telerik";
    public static final String INVALID_BOARD_TEAM = getString(DESCRIPTION_MAX_LENGTH + 1);


    @Test
    public void getActivityHistoryShould_ReturnCopyOfCollection() {
        // Arrange
        Board board = new BoardImpl(
                1,
                VALID_BOARD_NAME,
                dummyFeedbacks,
                dummyStories,
                dummyBugs,
                TeamImplTests.VALID_TEAM_NAME);

        List<String> categoriesReference = board.getActivityHistory();
        List<String> sameReference = board.getActivityHistory();

        // Act, Assert
        Assertions.assertNotSame(categoriesReference, sameReference);
    }

    @Test
    public void getTasksShould_ReturnCopyOfCollection() {
        // Arrange
        Board board = new BoardImpl(
                1,
                VALID_BOARD_NAME,
                dummyFeedbacks,
                dummyStories,
                dummyBugs,
                TeamImplTests.VALID_TEAM_NAME);

        List<String> categoriesReference = board.getTasks();
        List<String> sameReference = board.getTasks();

        // Act, Assert
        Assertions.assertNotSame(categoriesReference, sameReference);
    }

    @Test
    public void getStories_ReturnCopyOfCollection() {
        // Arrange
        Board board = new BoardImpl(
                1,
                VALID_BOARD_NAME,
                dummyFeedbacks,
                dummyStories,
                dummyBugs,
                TeamImplTests.VALID_TEAM_NAME);

        List<Story> categoriesReference = board.getStories();
        List<Story> sameReference = board.getStories();

        // Act, Assert
        Assertions.assertNotSame(categoriesReference, sameReference);
    }

    @Test
    public void getBugs_ReturnCopyOfCollection() {
        // Arrange
        Board board = new BoardImpl(
                1,
                VALID_BOARD_NAME,
                dummyFeedbacks,
                dummyStories,
                dummyBugs,
                TeamImplTests.VALID_TEAM_NAME);

        List<Bugs> categoriesReference = board.getBugs();
        List<Bugs> sameReference = board.getBugs();

        // Act, Assert
        Assertions.assertNotSame(categoriesReference, sameReference);
    }

    @Test
    public void getFeedbacks_ReturnCopyOfCollection() {
        // Arrange
        Board board = new BoardImpl(
                1,
                VALID_BOARD_NAME,
                dummyFeedbacks,
                dummyStories,
                dummyBugs,
                TeamImplTests.VALID_TEAM_NAME);

        List<Feedback> categoriesReference = board.getFeedbacks();
        List<Feedback> sameReference = board.getFeedbacks();

        // Act, Assert
        Assertions.assertNotSame(categoriesReference, sameReference);
    }

    @Test
    public void should_Return_Proper_Values_When_getAsString_initiated() {
        // Arrange
        List<Feedback> boardFeedbacks = new ArrayList<>();
        List<Bugs> boardBugs = new ArrayList<>();
        List<Story> boardStories= new ArrayList<>();

        Feedback feedback = new FeedbackImpl(
                1,
                FeedbackImplTests.FEEDBACK_VALID_TITLE,
                FeedbackImplTests.FEEDBACK_VALID_DESCRIPTION,
                FeedbackImplTests.FEEDBACK_VALID_RATING,
                FeedbackStatus.NEW,
                VALID_BOARD_NAME);
        boardFeedbacks.add(feedback);

        Bugs bug = new BugsImpl(
                1,
                BugsImplTests.VALID_BUG_TITLE,
                BugsImplTests.VALID_BUG_DESCRIPTION,
                Priority.HIGH,
                Severity.CRITICAL,
                BugsStatus.ACTIVE,
                "PRaykov",
                stepsToReproduce,
                "Telerik");

        boardBugs.add(bug);

        Story story = new StoryImpl(
                1,
                StoryImplTests.VALID_STORY_TITLE,
                StoryImplTests.VALID_STORY_DESCRIPTION,
                Priority.HIGH,
                Size.LARGE,
                Status.INPROGRESS,
                "PRaykov",
                FeedbackImplTests.VALID_BOARD_NAME);

        boardStories.add(story);

        Board board = new BoardImpl(
                1,
                VALID_BOARD_NAME,
                boardFeedbacks,
                boardStories,
                boardBugs,
                TeamImplTests.VALID_TEAM_NAME);


        String toString = board.toString();

        // Act, Assert
        assertTrue(toString.contains("----- Board"));
        assertTrue(toString.contains("Bug name:"));
        assertTrue(toString.contains("Story name:"));
        assertTrue(toString.contains("Feedback name:"));
    }

    public static Board initializeBoard() {
        return new BoardImpl(
                1,
                VALID_BOARD_NAME,
                dummyFeedbacks,
                dummyStories,
                dummyBugs,
                TeamImplTests.VALID_TEAM_NAME);
    }
}


