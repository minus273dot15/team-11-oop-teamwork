package com.company.oop.taskmanagement.tests.models;

import com.company.oop.taskmanagement.models.PersonImpl;
import com.company.oop.taskmanagement.models.TeamsImpl;
import com.company.oop.taskmanagement.models.contracts.Board;
import com.company.oop.taskmanagement.models.contracts.Person;
import com.company.oop.taskmanagement.models.contracts.Team;
import com.company.oop.taskmanagement.models.items.FeedbackImpl;
import com.company.oop.taskmanagement.models.items.contracts.Feedback;
import com.company.oop.taskmanagement.models.items.enums.FeedbackStatus;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static com.company.oop.taskmanagement.models.TeamsImpl.TEAM_NAME_MAX_LENGTH;
import static com.company.oop.taskmanagement.models.TeamsImpl.TEAM_NAME_MIN_LENGTH;
import static com.company.oop.taskmanagement.tests.models.BoardImplTests.initializeBoard;
import static com.company.oop.taskmanagement.tests.models.PersonImplTests.initializePerson;
import static com.company.oop.taskmanagement.tests.utils.TestUtilities.getString;
import static com.company.oop.taskmanagement.utils.DummyArrayLists.*;
import static org.junit.jupiter.api.Assertions.*;

public class TeamImplTests {

    public static final String VALID_TEAM_NAME = getString(TEAM_NAME_MIN_LENGTH + 1);
    public static final String INVALID_TEAM_NAME = getString(TEAM_NAME_MAX_LENGTH + 1);


    @Test
    public void constructor_Should_CreateNewTeam_When_ParametersAreCorrect() {
        // Arrange, Act
        Team team = initializeTeam();

        // Assert
        assertAll(
                () -> assertEquals(1, team.getId()),
                () -> assertEquals(VALID_TEAM_NAME, team.getName())
        );
    }

    @Test
    public void constructor_Should_ThrowException_When_NameOutOfBounds() {
        // Arrange, Act, Assert
        Assertions.assertThrows(IllegalArgumentException.class,
                () -> new TeamsImpl(
                        1,
                        INVALID_TEAM_NAME,
                        dummyMembers,
                        dummyBoards));
    }

    @Test
    public void getMembersShould_ReturnCopyOfCollection() {
        // Arrange
        Team team = new TeamsImpl(
                1,
                VALID_TEAM_NAME,
                dummyMembers,
                dummyBoards);

        List<Person> categoriesReference = team.getMembers();
        List<Person> sameReference = team.getMembers();

        // Act, Assert
        Assertions.assertNotSame(categoriesReference, sameReference);
    }

    @Test
    public void getTasksHistoryShould_ReturnCopyOfCollection() {
        // Arrange
        Team team = new TeamsImpl(
                1,
                VALID_TEAM_NAME,
                dummyMembers,
                dummyBoards);

        List<Board> categoriesReference = team.getBoards();
        List<Board> sameReference = team.getBoards();

        // Act, Assert
        Assertions.assertNotSame(categoriesReference, sameReference);
    }

    @Test
    public void should_Return_Proper_Values_When_getAsString_initiated() {
        // Arrange

        dummyMembers.add(initializePerson());
        dummyBoards.add(initializeBoard());
        Team team = new TeamsImpl(
                1,
                VALID_TEAM_NAME,
                dummyMembers,
                dummyBoards);

        String toString = team.toString();

        // Act, Assert
        assertTrue(toString.contains("Team Entity Listing"));
        assertTrue(toString.contains("Team members:"));
        assertTrue(toString.contains("Team Boards:"));
    }
    @Test
    public void TeamImpl_Should_ImplementTeamInterface() {
        // Arrange, Act
        TeamsImpl team = initializeTeam();
        // Assert
        assertTrue(team instanceof Team);
    }

//

    @Test
    public void setMembersShould_AddNewMember() {
        // Arrange
        Team team = new TeamsImpl(
                1,
                VALID_TEAM_NAME,
                dummyMembers,
                dummyBoards);
        // Act

        dummyMembers.add(initializePerson());
        team.setMembers(dummyMembers);

        //Assert
        Assertions.assertNotSame(dummyMembers, team.getMembers());
    }

    public static TeamsImpl initializeTeam() {
        return new TeamsImpl(
                1,
                VALID_TEAM_NAME,
                dummyMembers,
                dummyBoards);
    }
}
