package com.company.oop.taskmanagement.tests.models.items;

import com.company.oop.taskmanagement.models.contracts.Person;
import com.company.oop.taskmanagement.models.items.CommentsImpl;
import com.company.oop.taskmanagement.models.items.contracts.Comments;
import com.company.oop.taskmanagement.tests.models.PersonImplTests;
import org.junit.jupiter.api.Test;

import static com.company.oop.taskmanagement.tests.utils.TestUtilities.getString;
import static org.junit.jupiter.api.Assertions.*;

public class CommentsImplTests {

    public static final String COMMENT_VALID_MESSAGE = getString(259);

    @Test
    public void should_Create_Comment_When_ValidValuesArePassed() {
        Person person = PersonImplTests.initializePerson();
        // Arrange
        Comments comments = new CommentsImpl(
                person,
                COMMENT_VALID_MESSAGE);

        // Act, Assert
        assertAll(
                () -> assertEquals(person, comments.getAuthor()),
                () -> assertEquals(COMMENT_VALID_MESSAGE, comments.getMessage())
        );
    }

    @Test
    public void CommentsImpl_Should_ImplementCommentsInterface() {
        // Arrange, Act
        CommentsImpl comments = initializeComment();
        // Assert
        assertTrue(comments instanceof Comments);
    }

    public static CommentsImpl initializeComment() {
        Person person = PersonImplTests.initializePerson();
        return new CommentsImpl(
                person,
                COMMENT_VALID_MESSAGE);
    }


}
