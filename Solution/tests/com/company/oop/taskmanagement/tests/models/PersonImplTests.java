package com.company.oop.taskmanagement.tests.models;

import com.company.oop.taskmanagement.models.PersonImpl;
import com.company.oop.taskmanagement.models.contracts.Person;
import com.company.oop.taskmanagement.models.items.FeedbackImpl;
import com.company.oop.taskmanagement.models.items.contracts.Feedback;
import com.company.oop.taskmanagement.models.items.enums.FeedbackStatus;
import com.company.oop.taskmanagement.tests.models.items.FeedbackImplTests;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.List;

import static com.company.oop.taskmanagement.models.PersonImpl.NAME_MAX_LENGTH;
import static com.company.oop.taskmanagement.models.PersonImpl.NAME_MIN_LENGTH;
import static com.company.oop.taskmanagement.tests.utils.TestUtilities.getString;
import static com.company.oop.taskmanagement.utils.DummyArrayLists.*;
import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class PersonImplTests {

    public static final String VALID_PERSON_NAME = getString(NAME_MIN_LENGTH + 1);
    public static final String INVALID_PERSON_NAME = getString(NAME_MAX_LENGTH + 1);

    @Test
    public void constructor_Should_CreateNewPerson_When_ParametersAreCorrect() {
        // Arrange, Act
        Person person = initializePerson();

        // Assert
        assertAll(
                () -> assertEquals(VALID_PERSON_NAME, person.getName()),
                () -> assertEquals(1, person.getId()),
                () -> Assertions.assertEquals(dummyActivityHistory, person.getActivityHistory()),
                () -> Assertions.assertEquals(dummyTasks, person.getTasks())
        );
    }

    @Test
    public void constructor_Should_ThrowException_When_NameOutOfBounds() {
        // Arrange, Act, Assert
        Assertions.assertThrows(IllegalArgumentException.class,
                () -> new PersonImpl(
                        1,
                        INVALID_PERSON_NAME,
                        dummyActivityHistory,
                        dummyTasks));
    }
    @Test
    public void should_Return_Proper_Values_When_getAsString_initiated() {
        // Arrange

        Person person = new PersonImpl(
                1,
                VALID_PERSON_NAME,
                dummyActivityHistory,
                dummyTasks);

        String toString = person.toString();

        // Act, Assert
        assertTrue(toString.contains("Person name:"));
    }
    @Test
    public void getActivityHistoryShould_ReturnCopyOfCollection() {
        // Arrange
        Person person = new PersonImpl(
                1,
                VALID_PERSON_NAME,
                dummyActivityHistory,
                dummyTasks);

        List<String> categoriesReference = person.getActivityHistory();
        List<String> sameReference = person.getActivityHistory();

        // Act, Assert
        Assertions.assertNotSame(categoriesReference, sameReference);
    }

    @Test
    public void getTasksHistoryShould_ReturnCopyOfCollection() {
        // Arrange
        Person person = new PersonImpl(
                1,
                VALID_PERSON_NAME,
                dummyActivityHistory,
                dummyTasks);

        List<String> categoriesReference = person.getTasks();
        List<String> sameReference = person.getTasks();

        // Act, Assert
        Assertions.assertNotSame(categoriesReference, sameReference);
    }

    public static Person initializePerson() {
        return new PersonImpl(
                1,
                VALID_PERSON_NAME,
                dummyActivityHistory,
                dummyTasks
        );
    }
}
