package com.company.oop.taskmanagement.tests.models.items;


import com.company.oop.taskmanagement.models.items.BugsImpl;
import com.company.oop.taskmanagement.models.items.contracts.Bugs;
import com.company.oop.taskmanagement.models.items.enums.*;

import static com.company.oop.taskmanagement.models.items.TaskImpl.*;
import static com.company.oop.taskmanagement.models.items.TaskImpl.DESCRIPTION_MAX_LENGTH;
import static com.company.oop.taskmanagement.tests.utils.TestUtilities.getString;
import static com.company.oop.taskmanagement.utils.DummyArrayLists.stepsToReproduce;

public class BugsImplTests {

    public static final String VALID_BUG_TITLE = getString(TITLE_MIN_LENGTH + 1);
    public static final String INVALID_BUG_TITLE = getString(TITLE_MAX_LENGTH + 1);
    public static final String VALID_BUG_DESCRIPTION = getString(DESCRIPTION_MIN_LENGTH + 1);
    public static final String INVALID_BUG_DESCRIPTION = getString(DESCRIPTION_MAX_LENGTH + 1);

    public static Bugs initializeBug() {
        return new BugsImpl(
                1,
                VALID_BUG_TITLE,
                VALID_BUG_DESCRIPTION,
                Priority.HIGH,
                Severity.CRITICAL,
                BugsStatus.ACTIVE,
                "PRaykov",
                stepsToReproduce,
                "TeleriK"
        );
    }

}
