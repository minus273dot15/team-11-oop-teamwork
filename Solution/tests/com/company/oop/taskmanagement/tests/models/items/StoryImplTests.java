package com.company.oop.taskmanagement.tests.models.items;

import com.company.oop.taskmanagement.models.items.StoryImpl;
import com.company.oop.taskmanagement.models.items.contracts.Story;
import com.company.oop.taskmanagement.models.items.enums.Priority;
import com.company.oop.taskmanagement.models.items.enums.Size;
import com.company.oop.taskmanagement.models.items.enums.Status;

import static com.company.oop.taskmanagement.models.items.TaskImpl.*;
import static com.company.oop.taskmanagement.models.items.TaskImpl.DESCRIPTION_MAX_LENGTH;
import static com.company.oop.taskmanagement.tests.utils.TestUtilities.getString;
public class StoryImplTests {

    public static final String VALID_STORY_TITLE = getString(TITLE_MIN_LENGTH + 1);
    public static final String INVALID_STORY_TITLE = getString(TITLE_MAX_LENGTH + 1);
    public static final String VALID_STORY_DESCRIPTION = getString(DESCRIPTION_MAX_LENGTH - 1);
    public static final String INVALID_STORY_DESCRIPTION = getString(DESCRIPTION_MAX_LENGTH + 1);



    public static Story initializeStory() {
        return new StoryImpl(
                1,
                VALID_STORY_TITLE,
                VALID_STORY_DESCRIPTION,
                Priority.HIGH,
                Size.MEDIUM,
                Status.INPROGRESS,
                "PRaykov",
                "TeleriK"
        );
    }
}
