package com.company.oop.taskmanagement.tests.models.items;

import com.company.oop.taskmanagement.models.items.FeedbackImpl;
import com.company.oop.taskmanagement.models.items.contracts.Comments;
import com.company.oop.taskmanagement.models.items.contracts.History;
import com.company.oop.taskmanagement.models.items.enums.FeedbackStatus;
import com.company.oop.taskmanagement.models.items.contracts.Feedback;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.List;

import static com.company.oop.taskmanagement.models.items.FeedbackImpl.RATING_MAX_VALUE;
import static com.company.oop.taskmanagement.models.items.TaskImpl.*;
import static com.company.oop.taskmanagement.tests.utils.TestUtilities.getString;
import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class FeedbackImplTests {

    public static final String FEEDBACK_VALID_TITLE = getString(TITLE_MIN_LENGTH + 1);
    public static final String FEEDBACK_INVALID_TITLE = getString(TITLE_MAX_LENGTH + 1);
    public static final String FEEDBACK_VALID_DESCRIPTION = getString(DESCRIPTION_MAX_LENGTH - 1);
    public static final String FEEDBACK_INVALID_DESCRIPTION = getString(DESCRIPTION_MAX_LENGTH + 1);
    public static final String VALID_BOARD_NAME = getString(TITLE_MAX_LENGTH - 1);
    public static final int FEEDBACK_VALID_RATING = RATING_MAX_VALUE - 1;
    public static final String FEEDBACK_INVALID_RATING = "Good Luck";


    @Test
    public void constructor_Should_CreateNewFeedback_When_ParametersAreCorrect() {
        // Arrange, Act
        Feedback feedback = initializeFeedback();

        // Assert
        assertAll(
                () -> assertEquals(1, feedback.getId()),
                () -> assertEquals(FEEDBACK_VALID_TITLE, feedback.getTitle()),
                () -> Assertions.assertEquals(FEEDBACK_VALID_DESCRIPTION, feedback.getDescription()),
                () -> Assertions.assertEquals(FEEDBACK_VALID_RATING, feedback.getRating()),
                () -> Assertions.assertEquals(FeedbackStatus.NEW, feedback.getStatus()),
                () -> Assertions.assertEquals(VALID_BOARD_NAME, feedback.getBelongsToBoard())
        );
    }
    @Test
    public void constructor_Should_ThrowException_When_RatingOutOfBounds() {
        // Arrange, Act, Assert
        Assertions.assertThrows(IllegalArgumentException.class,
                () -> new FeedbackImpl(
                        1,
                        FEEDBACK_VALID_TITLE,
                        FEEDBACK_VALID_DESCRIPTION,
                        Integer.valueOf(FEEDBACK_INVALID_RATING),
                        FeedbackStatus.NEW,
                        VALID_BOARD_NAME));
    }

    @Test
    public void should_Create_Feedback_When_ValidValuesArePassed() {
        // Arrange
        Feedback feedback = new FeedbackImpl(
                1,
                FEEDBACK_VALID_TITLE,
                FEEDBACK_VALID_DESCRIPTION,
                FEEDBACK_VALID_RATING,
                FeedbackStatus.NEW,
                VALID_BOARD_NAME);

        // Act, Assert
        assertAll(
                () -> assertEquals(1, feedback.getId()),
                () -> assertEquals(FEEDBACK_VALID_TITLE, feedback.getTitle()),
                () -> assertEquals(FEEDBACK_VALID_DESCRIPTION, feedback.getDescription()),
                () -> assertEquals(FEEDBACK_VALID_RATING, feedback.getRating()),
                () -> assertEquals(FeedbackStatus.NEW, feedback.getStatus())
        );
    }
    @Test
    public void should_Return_Proper_Values_When_getAsString_initiated() {
        // Arrange

        Feedback feedback = new FeedbackImpl(
                1,
                FEEDBACK_VALID_TITLE,
                FEEDBACK_VALID_DESCRIPTION,
                FEEDBACK_VALID_RATING,
                FeedbackStatus.NEW,
                VALID_BOARD_NAME);

        String toString = feedback.toString();

        // Act, Assert
        assertTrue(toString.contains("Feedback Entity Listing"));
    }

    @Test
    public void getHistoryShould_ReturnCopyOfCollection() {
        // Arrange
        Feedback feedback = new FeedbackImpl(
                1,
                FEEDBACK_VALID_TITLE,
                FEEDBACK_VALID_DESCRIPTION,
                FEEDBACK_VALID_RATING,
                FeedbackStatus.NEW,
                VALID_BOARD_NAME);

        List<History> categoriesReference = feedback.getHistory();
        List<History> sameReference = feedback.getHistory();

        // Act, Assert
        Assertions.assertNotSame(categoriesReference, sameReference);
    }

    @Test
    public void getCommentsShould_ReturnCopyOfCollection() {
        // Arrange
        Feedback feedback = new FeedbackImpl(
                1,
                FEEDBACK_VALID_TITLE,
                FEEDBACK_VALID_DESCRIPTION,
                FEEDBACK_VALID_RATING,
                FeedbackStatus.NEW,
                VALID_BOARD_NAME);

        List<Comments> categoriesReference = feedback.getComments();
        List<Comments> sameReference = feedback.getComments();

        // Act, Assert
        Assertions.assertNotSame(categoriesReference, sameReference);
    }

    @Test
    public void setFeedbackStatusShould_SetNewStatus() {
        // Arrange
        Feedback feedback = new FeedbackImpl(
                1,
                FEEDBACK_VALID_TITLE,
                FEEDBACK_VALID_DESCRIPTION,
                FEEDBACK_VALID_RATING,
                FeedbackStatus.NEW,
                VALID_BOARD_NAME);
        // Act
        feedback.setFeedbackStatus(FeedbackStatus.DONE);

        //Assert
        Assertions.assertNotSame(FeedbackStatus.NEW, feedback.getStatus());
    }

    @Test
    public void FeedbackImpl_Should_ImplementFeedbackInterface() {
        // Arrange, Act
        FeedbackImpl feedback = initializeFeedback();
        // Assert
        assertTrue(feedback instanceof Feedback);
    }

    public static FeedbackImpl initializeFeedback() {
        return new FeedbackImpl(
                1,
                FEEDBACK_VALID_TITLE,
                FEEDBACK_VALID_DESCRIPTION,
                FEEDBACK_VALID_RATING,
                FeedbackStatus.NEW,
                FeedbackImplTests.VALID_BOARD_NAME
                );
    }
}
