package com.company.oop.taskmanagement.core;

import com.company.oop.taskmanagement.core.contracts.TaskManagmentRepository;
import com.company.oop.taskmanagement.exceptions.ElementNotFoundException;
import com.company.oop.taskmanagement.models.BoardImpl;
import com.company.oop.taskmanagement.models.PersonImpl;
import com.company.oop.taskmanagement.models.TeamsImpl;
import com.company.oop.taskmanagement.models.contracts.Board;
import com.company.oop.taskmanagement.models.contracts.Identifiable;
import com.company.oop.taskmanagement.models.contracts.Person;
import com.company.oop.taskmanagement.models.contracts.Team;
import com.company.oop.taskmanagement.models.items.*;
import com.company.oop.taskmanagement.models.items.contracts.*;
import com.company.oop.taskmanagement.models.items.enums.*;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static com.company.oop.taskmanagement.commands.CommandsConstants.*;
import static com.company.oop.taskmanagement.utils.DummyArrayLists.*;

public class TaskManagmentRepositoryImpl implements TaskManagmentRepository {


    private int nextId;

    private Person loggedUser;

    private List<Bugs> bugs = new ArrayList<>();
    private List<Story> stories = new ArrayList<>();
    private List<Feedback> feedbacks = new ArrayList<>();
    private final List<Person> persons = new ArrayList<>();
    private final List<Team> teams = new ArrayList<>();
    private final List<Board> boards = new ArrayList<>();
    private final List<HistoryImpl> history = new ArrayList<>();

    public TaskManagmentRepositoryImpl() {
        nextId = 0;
        this.loggedUser = null;
    }

    @Override
    public void logEvent(String event) {
        history.add(new HistoryImpl(event));
    }

    /**
     * ----------- GETTERS START -----------
     */
    @Override
    public List<Bugs> getBugs() {
        return new ArrayList<>(bugs);
    }

    @Override
    public List<Story> getStories() {
        return new ArrayList<>(stories);
    }

    @Override
    public List<Feedback> getFeedbacks() {
        return new ArrayList<>(feedbacks);
    }

    public List<Person> getPersons() {
        return new ArrayList<>(persons);
    }

    public List<Board> getBoards() {
        return new ArrayList<>(boards);
    }

    public List<Team> getTeams() {
        return new ArrayList<>(teams);
    }
    @Override
    public List<HistoryImpl> getHistory() { return new ArrayList<>(history); }

    @Override
    public String getLoggedInUserName() {
        if (loggedUser == null) {
            throw new IllegalArgumentException(NO_LOGGED_IN_USER);
        }
        return loggedUser.getName();
    }

    @Override
    public Person getLoggedInUser() {
        if (loggedUser == null) {
            throw new IllegalArgumentException(NO_LOGGED_IN_USER);
        }
        return loggedUser;
    }


    /** ----------- GETTERS END ----------- */


    /**
     * ----------- FINDERS START -----------
     */

    @Override
    public Story findStoryById(int id) {
        return findElementById(getStories(), id);
    }

    @Override
    public Feedback findFeedbackById(int id) {
        return findElementById(getFeedbacks(), id);
    }

    @Override
    public Bugs findBugsById(int id) {
        return findElementById(getBugs(), id);
    }

    @Override
    public Board findBoardById(int id) {
        return findElementById(getBoards(), id);
    }

    @Override
    public Person findPersonById(int id) {
        return findElementById(getPersons(), id);
    }

    @Override
    public Team findTeamById(int id) {
        return findElementById(getTeams(), id);
    }

    @Override
    public Team findTeamByName(String teamToFind) {
        return teams
                .stream()
                .filter(u -> u.getName().equalsIgnoreCase(teamToFind))
                .findFirst()
                .orElseThrow(() -> new ElementNotFoundException(String.format(NO_SUCH_TEAM, teamToFind)));
    }

    @Override
    public Person findPersonByName(String personToFind) {
        return persons
                .stream()
                .filter(u -> u.getName().equalsIgnoreCase(personToFind))
                .findFirst()
                .orElseThrow(() -> new ElementNotFoundException(String.format(NO_SUCH_PERSON, personToFind)));
    }

    @Override
    public Board findBoardByName(String boardToFind) {
        return boards
                .stream()
                .filter(u -> u.getName().equalsIgnoreCase(boardToFind))
                .findFirst()
                .orElseThrow(() -> new ElementNotFoundException(String.format(NO_SUCH_BOARD, boardToFind)));
    }

    @Override
    public Feedback findFeedbackByName(String feedbackToFind) {
        return feedbacks
                .stream()
                .filter(u -> u.getTitle().equalsIgnoreCase(feedbackToFind))
                .findFirst()
                .orElseThrow(() -> new ElementNotFoundException(String.format(NO_SUCH_FEEDBACK, feedbackToFind)));
    }

    @Override
    public Bugs findBugByName(String bugToFind) {
        return bugs
                .stream()
                .filter(u -> u.getTitle().equalsIgnoreCase(bugToFind))
                .findFirst()
                .orElseThrow(() -> new ElementNotFoundException(String.format(NO_SUCH_BUG, bugToFind)));
    }

    @Override
    public Story findStoryByName(String storyToFind) {
        return stories
                .stream()
                .filter(u -> u.getTitle().equalsIgnoreCase(storyToFind))
                .findFirst()
                .orElseThrow(() -> new ElementNotFoundException(String.format(NO_SUCH_STORY, storyToFind)));
    }

    private <T extends Identifiable> T findElementById(List<T> elements, int id) {
        for (T element : elements) if (element.getId() == id) return element;
        throw new ElementNotFoundException(String.format("No record with ID %d", id));
    }

    /** ----------- FINDERS END ----------- */


    /**
     * ----------- CREATION METHODS START -----------
     */
    @Override
    public Feedback createFeedback(String title, String description, int rating, FeedbackStatus status, String belongsToBoard) {
        Feedback feedback = new FeedbackImpl(++nextId, title, description, rating, status, belongsToBoard);
        this.feedbacks.add(feedback);
        return feedback;
    }

    @Override
    public Person createNewPerson(String personName, List<String> activityHistory, List<String> tasks) {
        Person person = new PersonImpl(++nextId, personName, activityHistory, tasks);
        this.persons.add(person);
        return person;
    }

    @Override
    public Bugs createNewBug(String title, String description, Priority priority, Severity severity, BugsStatus status, String assignee, List<String> stepsToReproduce, String belongsToBoard) {
        Bugs newBug = new BugsImpl(++nextId, title, description, priority, severity, status, assignee, stepsToReproduce, belongsToBoard);
        this.bugs.add(newBug);
        return newBug;
    }

    @Override
    public Story createStory(String title, String description, Priority priority, Size size, Status status, String assignee, String belongsToBoard) {
        Story story = new StoryImpl(++nextId, title, description, priority, size, status, assignee, belongsToBoard);
        this.stories.add(story);
        return story;
    }

    @Override
    public Board createBoard(String boardName, String teamIdToBeAddedTo) {
        Board board = new BoardImpl(++nextId, boardName, dummyFeedbacks, dummyStories, dummyBugs, teamIdToBeAddedTo);
        this.boards.add(board);
        return board;
    }

    @Override
    public Team createTeam(String teamName) {
        for (Team teams : getTeams())
            if (teams.getName().equals(teamName))
                throw new IllegalArgumentException(String.format(TEAM_ALREADY_EXISTS, teamName));
        Team team = new TeamsImpl(++nextId, teamName, dummyMembers, dummyBoards);
        this.teams.add(team);
        return team;
    }

    @Override
    public Comments createComment(Person author, String content) {
        return new CommentsImpl(author, content);
    }

    /** ----------- CREATION METHODS END ----------- */


    /**
     * ----------- SETTER METHODS START -----------
     */
    @Override
    public Feedback setFeedbackRating(String feedbackName, int rating) {
        feedbacks = feedbacks.stream()
                .filter(obj -> obj.getTitle().equals(feedbackName))
                .peek(i -> i.setRating(rating)).collect(Collectors.toList());
        return findFeedbackByName(feedbackName);
    }

    @Override
    public Feedback addFeedbackComment(String feedbackName, Comments comments) {//TODO Params!
        feedbacks = feedbacks.stream()
                .filter(obj -> obj.getTitle().equals(feedbackName))
                .peek(i -> i.setFeedbackComment(getLoggedInUser(), comments.getMessage())).collect(Collectors.toList());
        return findFeedbackByName(feedbackName);
    }

    @Override
    public Feedback setFeedbackStatus(String feedbackName, FeedbackStatus feedbackStatus) {
        feedbacks = feedbacks.stream()
                .filter(obj -> obj.getTitle().equals(feedbackName))
                .peek(i -> i.setFeedbackStatus(feedbackStatus)).collect(Collectors.toList());
        return findFeedbackByName(feedbackName);
    }

    @Override
    public Story setStoryPriority(String storyName, Priority priority) {
        stories = stories.stream()
                .filter(obj -> obj.getTitle().equals(storyName))
                .peek(i -> i.setPriority(priority)).collect(Collectors.toList());
        return findStoryByName(storyName);
    }

    @Override
    public Story setStorySize(String storyName, Size size) {
        stories = stories.stream()
                .filter(obj -> obj.getTitle().equals(storyName))
                .peek(i -> i.setSize(size)).collect(Collectors.toList());
        return findStoryByName(storyName);
    }

    @Override
    public Story setStoryStatus(String storyName, Status status) {
        stories = stories.stream()
                .filter(obj -> obj.getTitle().equals(storyName))
                .peek(i -> i.setStatus(status)).collect(Collectors.toList());
        return findStoryByName(storyName);
    }
    @Override
    public Story setStoryAssignee(String storyName, String assignee) {
        stories = stories.stream()
                .filter(obj -> obj.getTitle().equals(storyName))
                .peek(i -> i.setAssignee(assignee)).collect(Collectors.toList());
        return findStoryByName(storyName);
    }

    @Override
    public Story addStoryComment(String storyName, Comments comments) {
        stories = stories.stream()
                .filter(obj -> obj.getTitle().equals(storyName))
                .peek(i -> i.setStoryComment(getLoggedInUser(), comments.getMessage())).collect(Collectors.toList());
        return findStoryByName(storyName);
    }

    @Override
    public Bugs addBugComment(String bugName, Comments comments) {
        bugs = bugs.stream()
                .filter(obj -> obj.getTitle().equals(bugName))
                .peek(i -> i.setBugComment(getLoggedInUser(), comments.getMessage())).collect(Collectors.toList());
        return findBugByName(bugName);
    }

    @Override
    public Bugs setBugsPriority(String bugName, Priority priority) {
        bugs = bugs.stream()
                .filter(obj -> obj.getTitle().equals(bugName))
                .peek(i -> i.setPriority(priority)).collect(Collectors.toList());
        return findBugByName(bugName);
    }

    @Override
    public Bugs setBugsSeverity(String bugName, Severity severity) {
        bugs = bugs.stream()
                .filter(obj -> obj.getTitle().equals(bugName))
                .peek(i -> i.setSeverity(severity)).collect(Collectors.toList());
        return findBugByName(bugName);
    }

    @Override
    public Bugs setBugStatus(String bugName, BugsStatus bugsStatus) {
        bugs = bugs.stream()
                .filter(obj -> obj.getTitle().equals(bugName))
                .peek(i -> i.setStatus(bugsStatus)).collect(Collectors.toList());
        return findBugByName(bugName);
    }

    @Override
    public Bugs setBugAssignee(String bugName, String assignee) {
        bugs = bugs.stream()
                .filter(obj -> obj.getTitle().equals(bugName))
                .peek(i -> i.setAssignee(assignee)).collect(Collectors.toList());
        return findBugByName(bugName);
    }

    /** ----------- SETTER METHODS END ----------- */


    /**
     * ----------- LOGIN/LOGOUT METHODS START -----------
     */
    @Override
    public boolean hasLoggedInUser() {
        return loggedUser != null;
    }

    @Override
    public void login(Person person) {
        loggedUser = person;
    }

    @Override
    public void logout() {
        loggedUser = null;
    }

    /** ----------- LOGIN/LOGOUT METHODS END ----------- */


    /**
     * ----------- ADD/REMOVE METHODS START -----------
     */

    @Override
    public Team addPersonToTeam(String personName, String teamName) {
        Person personToAdd = findPersonByName(personName);
        findTeamByName(teamName).getMembers().add(personToAdd);
        return findTeamByName(teamName);
    }

    /** ----------- ADD/REMOVE METHODS END ----------- */
}
