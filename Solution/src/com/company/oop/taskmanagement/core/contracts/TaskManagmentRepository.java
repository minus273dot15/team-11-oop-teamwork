package com.company.oop.taskmanagement.core.contracts;


import com.company.oop.taskmanagement.models.contracts.Board;
import com.company.oop.taskmanagement.models.contracts.Person;
import com.company.oop.taskmanagement.models.contracts.Team;
import com.company.oop.taskmanagement.models.items.HistoryImpl;
import com.company.oop.taskmanagement.models.items.enums.*;
import com.company.oop.taskmanagement.models.items.contracts.*;

import java.util.List;

public interface TaskManagmentRepository {

    void logEvent(String event);

    List<Bugs> getBugs();
    List<Story> getStories();
    List<Feedback> getFeedbacks();
    List<Person> getPersons();
    List<Team> getTeams();
    public List<Board> getBoards();

    List<HistoryImpl> getHistory();

    Person getLoggedInUser();


    Bugs setBugAssignee(String bugName, String assignee);

    boolean hasLoggedInUser();
    void login(Person person);
    void logout();


    Comments createComment(Person author, String content);

    Feedback setFeedbackRating(String feedbackName, int rating);

    Feedback addFeedbackComment(String feedbackName, Comments comments);

    Feedback setFeedbackStatus(String feedbackName, FeedbackStatus feedbackStatus);

    Story setStoryPriority(String storyName, Priority priority);

    Story setStorySize(String storyName, Size size);

    Story setStoryStatus(String storyName, Status status);

    Story setStoryAssignee(String storyName, String assignee);

    Story addStoryComment(String storyName, Comments comments);

    Bugs addBugComment(String bugName, Comments comments);

    Bugs setBugsPriority(String bugName, Priority priority);

    Bugs setBugsSeverity(String bugName, Severity severity);

    Bugs setBugStatus(String bugName, BugsStatus bugsStatus);


    String getLoggedInUserName();

    Story findStoryById(int id);

    Person findPersonById(int id);

    Board findBoardById(int id);

    Team findTeamById(int id);

    Bugs findBugsById(int id);

    Feedback findFeedbackById(int id);

    Team findTeamByName(String teamToFind);

    Person findPersonByName(String personToFind);

    Board createBoard(String boardName, String teamIdToBeAddedTo);

    Team createTeam(String boardName);

    Story createStory(String title, String description, Priority priority, Size size, Status status, String assignee, String belongsToBoard);

    Board findBoardByName(String boardToFind);

    Feedback findFeedbackByName(String feedbackToFind);

    Bugs findBugByName(String bugToFind);

    Story findStoryByName(String storyToFind);

    Feedback createFeedback(String title, String description, int rating, FeedbackStatus status, String belongsToBoard);

    Person createNewPerson(String name, List<String> activityHistory, List<String> tasks);

    Bugs createNewBug(String title, String description, Priority priority, Severity severity, BugsStatus status, String assignee, List<String> stepsToReproduce, String belongsToBoard);

    Team addPersonToTeam(String personName, String teamName);

}
