package com.company.oop.taskmanagement.core;

import com.company.oop.taskmanagement.commands.adders.AddPersonToTeamCommand;
import com.company.oop.taskmanagement.commands.contracts.Command;
import com.company.oop.taskmanagement.commands.creation.*;
import com.company.oop.taskmanagement.commands.eggs.AsciiDrawString;
import com.company.oop.taskmanagement.commands.eggs.TicTacToe;
import com.company.oop.taskmanagement.commands.enums.CommandType;
import com.company.oop.taskmanagement.commands.listing.*;
import com.company.oop.taskmanagement.commands.login.LoginCommand;
import com.company.oop.taskmanagement.commands.login.LogoutCommand;
import com.company.oop.taskmanagement.commands.setters.*;
import com.company.oop.taskmanagement.commands.setters.SetBugsPriorityCommand;
import com.company.oop.taskmanagement.commands.setters.SetFeedbackRatingCommand;
import com.company.oop.taskmanagement.commands.setters.SetFeedbackStatusCommand;
import com.company.oop.taskmanagement.core.contracts.TaskManagmentRepository;
import com.company.oop.taskmanagement.utils.ParsingHelpers;
import com.company.oop.taskmanagement.core.contracts.CommandFactory;

public class CommandFactoryImpl implements CommandFactory {

    private static final String INVALID_COMMAND = "Invalid command name: %s!";

    public Command createCommandFromCommandName(String commandName, TaskManagmentRepository taskManagmentRepository) {
        CommandType commandType = ParsingHelpers.tryParseEnum(commandName, CommandType.class, String.format(INVALID_COMMAND, commandName));

        switch (commandType) {
            /**------- CREATE COMMANDS START ---------*/
            case CREATEFEEDBACK:
                return new CreateFeedbackCommand(taskManagmentRepository);
            case CREATEPERSON:
                return new CreateNewPersonCommand(taskManagmentRepository);
            case CREATESTORY:
                return new CreateStoryCommand(taskManagmentRepository);
            case CREATEBUG:
                return new CreateBugCommand(taskManagmentRepository);
            case CREATEBOARD:
                return new CreateNewBoard(taskManagmentRepository);
            case CREATETEAM:
                return new CreateNewTeamCommand(taskManagmentRepository);
            case CREATECOMMENT:
                return new CreateCommentCommand(taskManagmentRepository);
            /**------- CREATE COMMANDS END ---------*/


            /**------- SET COMMANDS START ---------*/
            case SETSTORYPRIORITY:
                return new SetStoryPriorityCommand(taskManagmentRepository);
            case SETSTORYSIZE:
                return new SetStorySizeCommand(taskManagmentRepository);
            case SETSTORYSTATUS:
                return new SetStoryStatusCommand(taskManagmentRepository);
            case BUGSETPRIORITY:
                return new SetBugsPriorityCommand(taskManagmentRepository);
            case BUGSETSEVERITY:
                return new SetBugSeverityCommand(taskManagmentRepository);
            case BUGSETSTATUS:
                return new SetBugStatusCommand(taskManagmentRepository);
            case FEEDBACKSETSTATUS:
                return new SetFeedbackStatusCommand(taskManagmentRepository);
            case FEEDBACKSETRATING:
                return new SetFeedbackRatingCommand(taskManagmentRepository);
            case SETBUGASSIGNEE:
                return new SetBugAssigneeCommand(taskManagmentRepository);
            case SETSTORYASSIGNEE:
                return new SetStoryAssigneeCommand(taskManagmentRepository);
            case UNASSIGNSTORY:
                return new UnassignStoryCommand(taskManagmentRepository);
            case UNASSIGNBUG:
                return new UnassignBugCommand(taskManagmentRepository);

            /**------- SET COMMANDS END ---------*/


            /**------- SHOW COMMANDS START ---------*/
            case SHOWALLPERSONS:
                return new ListAllPersonsCommand(taskManagmentRepository);
            case SHOWALLFEEDBACK:
                return new ListAllFeedbacksCommand(taskManagmentRepository);
            case SHOWALLFEEDBACKSORTEDBYRATING:
                return new ListAllFeedbackSrotedByRatingCommand(taskManagmentRepository);
            case SHOWFEEDBACKWITHRATING:
                return new ListAllFeedbacksWithRatingEqualsXCommand(taskManagmentRepository);
            case SHOWFEEDBACKWITHSTATUS:
                return new ListAllFeedbackWithStatusEqualsXCommand(taskManagmentRepository);
            case SHOWSTORYWITHSTATUS:
                return new ListAllStoriesWithStatusEqualsXCommand(taskManagmentRepository);
            case SHOWALLSTORIES:
                return new ListAllStoriesCommand(taskManagmentRepository);
            case SHOWALLSTORYSORTEDBYSIZE:
                return new ListAllStoriesSortedBySizeCommand(taskManagmentRepository);
            case SHOWALLSTORYSORTEDBYPRIORITY:
                return new ListAllStoriesSortedByPriorityCommand(taskManagmentRepository);
            case SHOWALLSTORYSORTEDBYTITLE:
                return new ListAllStoriesSortedByTitleCommand(taskManagmentRepository);
            case SHOWALLBUGS:
                return new ListAllBugsCommand(taskManagmentRepository);
            case SHOWALLFBUGSSORTEDBYTITLE:
                return new ListAllBugsSortedByTitleCommand(taskManagmentRepository);
            case SHOWALLBUGSSORTEDBYPRIORITY:
                return new ListAllBugsSortedByPriorityCommand(taskManagmentRepository);
            case SHOWALLBUGSSORTEDBYSEVERITY:
                return new ListAllBugsSortedBySeverityCommand(taskManagmentRepository);
            case SHOWALLFEEDBACKSORTEDBYTITLE:
                return new ListAllFeedbacksSortedByTitleCommand(taskManagmentRepository);
            case LISTALLFEEDBACKHISTORY:
                return new ListAllFeedbackHistoryCommand(taskManagmentRepository);
            case LISTALLBUGHISTORY:
                return new ListAllBugHistoryCommand(taskManagmentRepository);
            case LISTALLSTORYHISTORY:
                return new ListAllStoriesHistoryCommand(taskManagmentRepository);
            case LISTALLTASKSSORTBYTITLE:
                return new ListAllTasksSortByTitleCommand(taskManagmentRepository);
            case LISTALLBOARDSOFTEAM:
                return new ListAllBoardsOfTeamCommand(taskManagmentRepository);
            case LISTALLTEAMMEMBERS:
                return new ListAllTeamMembersCommand(taskManagmentRepository);



            /**------- SHOW COMMANDS END ---------*/

            /**------- ADD COMMANDS START ---------*/
            case ADDPERSONTOTEAM:
                return new AddPersonToTeamCommand(taskManagmentRepository);
            case LOGIN:
                return new LoginCommand(taskManagmentRepository);
            case LOGOUT:
                return new LogoutCommand(taskManagmentRepository);
            case LISTMYTASKS:
                return new ListMyTasksCommand(taskManagmentRepository);
            case TICTACTOE:
                return new TicTacToe();
            case PRINT:
                return new AsciiDrawString(taskManagmentRepository);
            default:
                throw new IllegalArgumentException(String.format(INVALID_COMMAND, commandName));
        }
    }

}