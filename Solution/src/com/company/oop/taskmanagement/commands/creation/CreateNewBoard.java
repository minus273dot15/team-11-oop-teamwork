package com.company.oop.taskmanagement.commands.creation;

import com.company.oop.taskmanagement.commands.contracts.Command;
import com.company.oop.taskmanagement.core.contracts.TaskManagmentRepository;
import com.company.oop.taskmanagement.models.contracts.Board;
import com.company.oop.taskmanagement.utils.ValidationHelper;

import java.util.List;

import static com.company.oop.taskmanagement.commands.CommandsConstants.*;

public class CreateNewBoard implements Command {
    public static final int EXPECTED_NUMBER_OF_ARGUMENTS = 2;//name, teamIdToBeAddedTo

    private final TaskManagmentRepository taskManagmentRepository;

    private String boardName, teamIdToBeAddedTo;

    public CreateNewBoard(TaskManagmentRepository taskManagmentRepository) {
        this.taskManagmentRepository = taskManagmentRepository;
    }

    @Override
    public String execute(List<String> parameters) {
        ValidationHelper.validateArgumentsCount(parameters, EXPECTED_NUMBER_OF_ARGUMENTS);
        parseParameters(parameters);
        if (!taskManagmentRepository.getTeams().contains(taskManagmentRepository.findTeamByName(teamIdToBeAddedTo)))
            throw new IllegalArgumentException(String.format(TEAM_DOES_NOT_EXISTS, teamIdToBeAddedTo));
        for (Board board : taskManagmentRepository.getBoards()) {
            if (board.getName().equals(boardName) && board.getBelongsToTeam().equals(teamIdToBeAddedTo))
                throw new IllegalArgumentException(String.format(BOARD_ALREADY_EXISTS, boardName, teamIdToBeAddedTo));
        }
        Board createdBoard = taskManagmentRepository.createBoard(boardName, teamIdToBeAddedTo);
        taskManagmentRepository.logEvent(String.format(FEEDBACK_CREATED_BOARD, createdBoard.getName(), createdBoard.getId()));
        return String.format(FEEDBACK_CREATED_BOARD, createdBoard.getName(), createdBoard.getId());
    }

    private void parseParameters(List<String> parameters) {
        boardName = String.valueOf(parameters.get(0));
        teamIdToBeAddedTo = String.valueOf(parameters.get(1));

    }
}
