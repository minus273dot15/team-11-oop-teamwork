package com.company.oop.taskmanagement.commands.creation;

import com.company.oop.taskmanagement.commands.contracts.Command;
import com.company.oop.taskmanagement.core.contracts.TaskManagmentRepository;
import com.company.oop.taskmanagement.models.contracts.Person;
import com.company.oop.taskmanagement.models.items.contracts.Comments;
import com.company.oop.taskmanagement.utils.ValidationHelper;

import java.util.List;

import static com.company.oop.taskmanagement.commands.CommandsConstants.*;
import static com.company.oop.taskmanagement.utils.ColorScheme.ANSI_RED;
import static com.company.oop.taskmanagement.utils.ColorScheme.ANSI_RESET;

public class CreateCommentCommand implements Command {


    public static final int EXPECTED_NUMBER_OF_ARGUMENTS = 2;

    private final TaskManagmentRepository taskManagmentRepository;

    private String targetType;

    public CreateCommentCommand(TaskManagmentRepository taskManagmentRepository) {
        this.taskManagmentRepository = taskManagmentRepository;
    }

    @Override
    public String execute(List<String> parameters) {
        ValidationHelper.validateArgumentsCount(parameters, EXPECTED_NUMBER_OF_ARGUMENTS);
        String target = parameters.get(0);
        String content = parameters.get(1);
        String author = taskManagmentRepository.getLoggedInUserName();
        return addComment(target, content, author);
    }

    private String addComment(String target, String content, String author) {
        Person authorAsPerson = taskManagmentRepository.findPersonByName(author);
        Comments comment = taskManagmentRepository.createComment(authorAsPerson, content);

        //find if story/bug/feedback
        if (!taskManagmentRepository.findBugByName(target).equals(String.format(ANSI_RED + NO_SUCH_BUG + ANSI_RESET, target))) {//it`s bug?
            targetType = "bug";
            taskManagmentRepository.addBugComment(target, comment);
            taskManagmentRepository.logEvent(String.format(COMMENT_ADDED_SUCCESSFULLY, author, targetType, target));
            return String.format(COMMENT_ADDED_SUCCESSFULLY, author, targetType, target);
        }
        if (!taskManagmentRepository.findFeedbackByName(target).equals(String.format(ANSI_RED + NO_SUCH_FEEDBACK + ANSI_RESET, target))) {//it`s feedback?
            targetType = "feedback";
            taskManagmentRepository.addFeedbackComment(target, comment);
            taskManagmentRepository.logEvent(String.format(COMMENT_ADDED_SUCCESSFULLY, author, targetType, target));
            return String.format(COMMENT_ADDED_SUCCESSFULLY, author, targetType, target);
        }

        if (!taskManagmentRepository.findStoryByName(target).equals(String.format(ANSI_RED + NO_SUCH_STORY + ANSI_RESET, target))) {//it`s story?
            targetType = "story";
            taskManagmentRepository.addStoryComment(target, comment);
            taskManagmentRepository.logEvent(String.format(COMMENT_ADDED_SUCCESSFULLY, author, targetType, target));
            return String.format(COMMENT_ADDED_SUCCESSFULLY, author, targetType, target);
        }
        return String.format(TARGET_NOT_FOUND, target);
    }
}
