package com.company.oop.taskmanagement.commands.creation;

import com.company.oop.taskmanagement.commands.contracts.Command;
import com.company.oop.taskmanagement.core.contracts.TaskManagmentRepository;
import com.company.oop.taskmanagement.models.items.enums.Priority;
import com.company.oop.taskmanagement.models.items.enums.Size;
import com.company.oop.taskmanagement.models.items.enums.Status;
import com.company.oop.taskmanagement.models.items.contracts.Story;
import com.company.oop.taskmanagement.utils.ValidationHelper;

import java.util.List;


import static com.company.oop.taskmanagement.commands.CommandsConstants.BOARD_DOES_NOT_EXISTS;
import static com.company.oop.taskmanagement.commands.CommandsConstants.STORY_CREATED_MESSAGE;

public class CreateStoryCommand implements Command {

    public static final int EXPECTED_NUMBER_OF_ARGUMENTS = 6;
    private final TaskManagmentRepository taskManagmentRepository;
    private String title;
    private String description;
    private Priority priority;
    private Size size;
    private Status status;
    private String assignee, belongsToBoard;

    public CreateStoryCommand(TaskManagmentRepository taskManagmentRepository) {
        this.taskManagmentRepository = taskManagmentRepository;
    }

    @Override
    public String execute(List<String> parameters) {
        ValidationHelper.validateArgumentsCount(parameters, EXPECTED_NUMBER_OF_ARGUMENTS);
        parseParameters(parameters);
        if (!taskManagmentRepository.getBoards().contains(taskManagmentRepository.findBoardByName(belongsToBoard)))
            throw new IllegalArgumentException(String.format(BOARD_DOES_NOT_EXISTS, belongsToBoard));
        Story createdStory = taskManagmentRepository.createStory(title, description, priority, size, status, assignee, belongsToBoard);
        taskManagmentRepository.logEvent("Story: " + String.format(STORY_CREATED_MESSAGE, createdStory.getId(), parameters.get(2), belongsToBoard));
        return String.format(STORY_CREATED_MESSAGE, createdStory.getId(), parameters.get(2), belongsToBoard);
    }

    private void parseParameters(List<String> parameters) {
        title = String.valueOf(parameters.get(0));
        description = String.valueOf(parameters.get(1));
        priority = Priority.valueOf(parameters.get(2));
        size = Size.valueOf(parameters.get(3));
        status = Status.valueOf(parameters.get(4));
        assignee = "Not assigned";
        belongsToBoard = String.valueOf(parameters.get(5));

    }
}
