package com.company.oop.taskmanagement.commands.creation;

import com.company.oop.taskmanagement.commands.contracts.Command;
import com.company.oop.taskmanagement.core.contracts.TaskManagmentRepository;
import com.company.oop.taskmanagement.models.contracts.Team;
import com.company.oop.taskmanagement.utils.ValidationHelper;

import java.util.List;

import static com.company.oop.taskmanagement.commands.CommandsConstants.*;

public class CreateNewTeamCommand implements Command {

    public static final int EXPECTED_NUMBER_OF_ARGUMENTS = 1;//Only Team name required on creation

    private final TaskManagmentRepository taskManagmentRepository;

    private String teamName;

    public CreateNewTeamCommand(TaskManagmentRepository taskManagmentRepository) {
        this.taskManagmentRepository = taskManagmentRepository;
    }

    @Override
    public String execute(List<String> parameters) {
        ValidationHelper.validateArgumentsCount(parameters, EXPECTED_NUMBER_OF_ARGUMENTS);
        parseParameters(parameters);
        taskManagmentRepository.getTeams().stream().filter(team -> team.getName().equals(teamName)).forEach(team -> {
            throw new IllegalArgumentException(String.format(TEAM_ALREADY_EXISTS, teamName));
        });
        Team createdTeam = taskManagmentRepository.createTeam(teamName);
        taskManagmentRepository.logEvent(String.format(TEAM_CREATED_MESSAGE, createdTeam.getName(), createdTeam.getId()));
        return String.format(TEAM_CREATED_MESSAGE, createdTeam.getName(), createdTeam.getId());
    }

    private void parseParameters(List<String> parameters) {
        teamName = String.valueOf(parameters.get(0));

    }
}
