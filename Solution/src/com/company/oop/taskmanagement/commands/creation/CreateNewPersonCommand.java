package com.company.oop.taskmanagement.commands.creation;

import com.company.oop.taskmanagement.commands.contracts.Command;
import com.company.oop.taskmanagement.core.contracts.TaskManagmentRepository;
import com.company.oop.taskmanagement.models.contracts.Person;
import com.company.oop.taskmanagement.utils.ValidationHelper;

import java.util.List;

import static com.company.oop.taskmanagement.commands.CommandsConstants.*;
import static com.company.oop.taskmanagement.utils.DummyArrayLists.dummyActivityHistory;
import static com.company.oop.taskmanagement.utils.DummyArrayLists.dummyTasks;

public class CreateNewPersonCommand implements Command {
    public static final int EXPECTED_NUMBER_OF_ARGUMENTS = 1;//only name required to create person

    private final TaskManagmentRepository taskManagmentRepository;

    private String personName;

    public CreateNewPersonCommand(TaskManagmentRepository taskManagmentRepository) {
        this.taskManagmentRepository = taskManagmentRepository;
    }

    @Override
    public String execute(List<String> parameters) {
        ValidationHelper.validateArgumentsCount(parameters, EXPECTED_NUMBER_OF_ARGUMENTS);
        parseParameters(parameters);
        taskManagmentRepository.getPersons().stream().filter(person -> person.getName().equals(personName)).forEach(person -> {
            throw new IllegalArgumentException(String.format(PERSON_ALREADY_EXISTS, personName));
        });
        Person createdPerson = taskManagmentRepository.createNewPerson(personName, dummyActivityHistory, dummyTasks);
        taskManagmentRepository.logEvent(String.format(PERSON_CREATED_MESSAGE, createdPerson.getName(), createdPerson.getId()));
        return String.format(PERSON_CREATED_MESSAGE, createdPerson.getName(), createdPerson.getId());
    }

    private void parseParameters(List<String> parameters) {
        personName = String.valueOf(parameters.get(0));

    }
}
