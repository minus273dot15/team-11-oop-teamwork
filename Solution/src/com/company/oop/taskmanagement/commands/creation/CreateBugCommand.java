package com.company.oop.taskmanagement.commands.creation;

import com.company.oop.taskmanagement.commands.contracts.Command;
import com.company.oop.taskmanagement.core.contracts.TaskManagmentRepository;
import com.company.oop.taskmanagement.models.items.enums.BugsStatus;
import com.company.oop.taskmanagement.models.items.enums.Priority;
import com.company.oop.taskmanagement.models.items.enums.Severity;
import com.company.oop.taskmanagement.models.items.contracts.Bugs;
import com.company.oop.taskmanagement.utils.ValidationHelper;

import java.util.Collections;
import java.util.List;

import static com.company.oop.taskmanagement.commands.CommandsConstants.BOARD_DOES_NOT_EXISTS;
import static com.company.oop.taskmanagement.commands.CommandsConstants.BUG_CREATED_MESSAGE;

public class CreateBugCommand implements Command {

    public static final int EXPECTED_NUMBER_OF_ARGUMENTS = 7;
    private final TaskManagmentRepository taskManagmentRepository;

    private String title, description, belongsToBoard;
    private Priority priority;
    private Severity severity;
    private BugsStatus status;
    private String assignee;
    private List<String> stepsToReproduce;

    public CreateBugCommand(TaskManagmentRepository taskManagmentRepository) {
        this.taskManagmentRepository = taskManagmentRepository;
    }

    @Override
    public String execute(List<String> parameters) {
        ValidationHelper.validateArgumentsCount(parameters, EXPECTED_NUMBER_OF_ARGUMENTS);
        parseParameters(parameters);
        if (!taskManagmentRepository.getBoards().contains(taskManagmentRepository.findBoardByName(belongsToBoard)))
            throw new IllegalArgumentException(String.format(BOARD_DOES_NOT_EXISTS, belongsToBoard));
        Bugs createdBug = taskManagmentRepository.createNewBug(title, description, priority, severity, status, assignee, stepsToReproduce, belongsToBoard);
        taskManagmentRepository.logEvent("Bug: " + String.format(BUG_CREATED_MESSAGE, createdBug.getTitle(), createdBug.getId(), belongsToBoard));
        return String.format(BUG_CREATED_MESSAGE, createdBug.getTitle(), createdBug.getId(), belongsToBoard);
    }

    private void parseParameters(List<String> parameters) {
        title = String.valueOf(parameters.get(0));
        description = String.valueOf(parameters.get(1));
        priority = Priority.valueOf(parameters.get(2));
        severity = Severity.valueOf(parameters.get(3));
        status = BugsStatus.valueOf(parameters.get(4));
        assignee = "Not assigned";
        stepsToReproduce = Collections.singletonList(parameters.get(5));
        belongsToBoard = String.valueOf(parameters.get(6));

    }
}
