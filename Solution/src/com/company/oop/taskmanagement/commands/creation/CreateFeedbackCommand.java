package com.company.oop.taskmanagement.commands.creation;

import com.company.oop.taskmanagement.commands.contracts.Command;
import com.company.oop.taskmanagement.core.contracts.TaskManagmentRepository;
import com.company.oop.taskmanagement.models.items.enums.FeedbackStatus;
import com.company.oop.taskmanagement.models.items.contracts.Feedback;
import com.company.oop.taskmanagement.utils.ParsingHelpers;
import com.company.oop.taskmanagement.utils.ValidationHelper;

import java.util.List;

import static com.company.oop.taskmanagement.commands.CommandsConstants.FEEDBACK_CREATED_MESSAGE;

public class CreateFeedbackCommand implements Command {

    public static final int EXPECTED_NUMBER_OF_ARGUMENTS = 4;

    private final TaskManagmentRepository taskManagmentRepository;

    private String title, description, belongsToBoard;
    private int rating;

    public CreateFeedbackCommand(TaskManagmentRepository taskManagmentRepository) {
        this.taskManagmentRepository = taskManagmentRepository;
    }

    @Override
    public String execute(List<String> parameters) {
        ValidationHelper.validateArgumentsCount(parameters, EXPECTED_NUMBER_OF_ARGUMENTS);
        parseParameters(parameters);
        Feedback createdFeedback = taskManagmentRepository.createFeedback(title, description, rating, FeedbackStatus.NEW, belongsToBoard);
        taskManagmentRepository.logEvent("Feedback: " +
                String.format(FEEDBACK_CREATED_MESSAGE, createdFeedback.getTitle(), createdFeedback.getRating(), createdFeedback.getId(), belongsToBoard));
        return String.format(FEEDBACK_CREATED_MESSAGE, createdFeedback.getTitle(), createdFeedback.getRating(), createdFeedback.getId(), belongsToBoard);
    }

    private void parseParameters(List<String> parameters) {
        title = String.valueOf(parameters.get(0));
        description = String.valueOf(parameters.get(1));
        rating = ParsingHelpers.tryParseInteger(parameters.get(2), "rating integer for Feedback");
        belongsToBoard = String.valueOf(parameters.get(3));
    }
}
