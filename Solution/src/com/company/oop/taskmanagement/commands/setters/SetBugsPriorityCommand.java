package com.company.oop.taskmanagement.commands.setters;

import com.company.oop.taskmanagement.models.items.contracts.Bugs;
import com.company.oop.taskmanagement.commands.contracts.Command;
import com.company.oop.taskmanagement.core.contracts.TaskManagmentRepository;
import com.company.oop.taskmanagement.models.items.enums.Priority;
import com.company.oop.taskmanagement.utils.ValidationHelper;


import java.util.List;

import static com.company.oop.taskmanagement.commands.CommandsConstants.SET_BUGS_PRIORITY_1;
import static com.company.oop.taskmanagement.commands.CommandsConstants.SET_BUGS_SEVERITY_1;
import static com.company.oop.taskmanagement.utils.ParsingHelpers.tryParseInteger;

public class SetBugsPriorityCommand implements Command {

    public static final int EXPECTED_NUMBER_OF_ARGUMENTS = 2;//id and priority

    private final TaskManagmentRepository taskManagmentRepository;

    private String bugName;
    private Priority priority;

    public SetBugsPriorityCommand(TaskManagmentRepository taskManagmentRepository) {
        this.taskManagmentRepository = taskManagmentRepository;
    }

    @Override
    public String execute(List<String> parameters) {
        ValidationHelper.validateArgumentsCount(parameters, EXPECTED_NUMBER_OF_ARGUMENTS);
        parseParameters(parameters);
        Bugs setBugsPriority = taskManagmentRepository.setBugsPriority(bugName, priority);
        taskManagmentRepository.logEvent("Bug: " + String.format(SET_BUGS_PRIORITY_1, setBugsPriority.getId(), setBugsPriority.getPriority()));
        return String.format(SET_BUGS_PRIORITY_1, setBugsPriority.getId(), setBugsPriority.getPriority());
    }

    private void parseParameters(List<String> parameters) {
        bugName = parameters.get(0);
        priority = Priority.valueOf(parameters.get(1));
    }
}
