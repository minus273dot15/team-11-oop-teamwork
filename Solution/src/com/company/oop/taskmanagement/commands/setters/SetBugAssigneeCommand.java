package com.company.oop.taskmanagement.commands.setters;

import com.company.oop.taskmanagement.commands.contracts.Command;
import com.company.oop.taskmanagement.core.contracts.TaskManagmentRepository;
import com.company.oop.taskmanagement.exceptions.ElementNotFoundException;
import com.company.oop.taskmanagement.models.items.contracts.Bugs;
import com.company.oop.taskmanagement.utils.ValidationHelper;

import java.util.List;

import static com.company.oop.taskmanagement.commands.CommandsConstants.*;

public class SetBugAssigneeCommand implements Command {
    public static final int EXPECTED_NUMBER_OF_ARGUMENTS = 2;//nameOfBug and assignee

    private final TaskManagmentRepository taskManagmentRepository;

    private String bugName, assignee;

    public SetBugAssigneeCommand(TaskManagmentRepository taskManagmentRepository) {
        this.taskManagmentRepository = taskManagmentRepository;
    }

    @Override
    public String execute(List<String> parameters) {
        ValidationHelper.validateArgumentsCount(parameters, EXPECTED_NUMBER_OF_ARGUMENTS);
        parseParameters(parameters);
        Bugs setBugsAssignee = taskManagmentRepository.setBugAssignee(bugName, assignee);

        taskManagmentRepository.logEvent("Bug: " + String.format(SET_BUG_ASSIGNEE, setBugsAssignee.getTitle(), setBugsAssignee.getAssignee()));
        return String.format(SET_BUG_ASSIGNEE, setBugsAssignee.getTitle(), setBugsAssignee.getAssignee());
    }

    private void parseParameters(List<String> parameters) {
        bugName = parameters.get(0);
        assignee = parameters.get(1);
        if (!taskManagmentRepository.getBugs().contains(taskManagmentRepository.findBugByName(bugName)))
            throw new ElementNotFoundException(String.format(BUG_DOES_NOT_EXISTS, bugName));
        if (!taskManagmentRepository.getPersons().contains(taskManagmentRepository.findPersonByName(assignee)))
            throw new ElementNotFoundException(String.format(PERSON_DOES_NOT_EXISTS, assignee));
    }
}
