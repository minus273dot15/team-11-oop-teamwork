package com.company.oop.taskmanagement.commands.setters;

import com.company.oop.taskmanagement.commands.contracts.Command;
import com.company.oop.taskmanagement.core.contracts.TaskManagmentRepository;
import com.company.oop.taskmanagement.models.items.enums.Status;
import com.company.oop.taskmanagement.models.items.contracts.Story;
import com.company.oop.taskmanagement.utils.ValidationHelper;

import java.util.List;

import static com.company.oop.taskmanagement.commands.CommandsConstants.SET_STORY_SIZE;
import static com.company.oop.taskmanagement.commands.CommandsConstants.SET_STORY_STATUS;

public class SetStoryStatusCommand implements Command {
    private final int EXPECTED_NUMBER_OF_ARGUMENTS = 2;

    private String storyName;
    Status status;
    private final TaskManagmentRepository taskManagmentRepository;

    public SetStoryStatusCommand(TaskManagmentRepository taskManagmentRepository) {
        this.taskManagmentRepository = taskManagmentRepository;
    }

    @Override
    public String execute(List<String> parameters) {
        ValidationHelper.validateArgumentsCount(parameters, EXPECTED_NUMBER_OF_ARGUMENTS);
        parseParameters(parameters);
        Story story = taskManagmentRepository.setStoryStatus(storyName, status);
        taskManagmentRepository.logEvent("Story: " + String.format(SET_STORY_STATUS, story.getId(), status));
        return String.format(SET_STORY_STATUS, story.getId(), status);
    }

    private void parseParameters(List<String> parameters) {
        storyName = parameters.get(0);
        status = Status.valueOf(parameters.get(1));
    }
}
