package com.company.oop.taskmanagement.commands.setters;

import com.company.oop.taskmanagement.commands.contracts.Command;
import com.company.oop.taskmanagement.core.contracts.TaskManagmentRepository;
import com.company.oop.taskmanagement.models.items.contracts.Feedback;
import com.company.oop.taskmanagement.utils.ValidationHelper;

import java.util.List;

import static com.company.oop.taskmanagement.commands.CommandsConstants.SET_BUGS_BUG_STATUS;
import static com.company.oop.taskmanagement.commands.CommandsConstants.SET_FEEDBACK_RATING;
import static com.company.oop.taskmanagement.utils.ParsingHelpers.tryParseInteger;

public class SetFeedbackRatingCommand implements Command {
    public static final int EXPECTED_NUMBER_OF_ARGUMENTS = 2;//id ,rating expected

    private final TaskManagmentRepository taskManagmentRepository;

    private int rating;
    private String feedbackName;

    public SetFeedbackRatingCommand(TaskManagmentRepository taskManagmentRepository) {
        this.taskManagmentRepository = taskManagmentRepository;
    }

    @Override
    public String execute(List<String> parameters) {
        ValidationHelper.validateArgumentsCount(parameters, EXPECTED_NUMBER_OF_ARGUMENTS);
        parseParameters(parameters);
        Feedback createdFeedback = taskManagmentRepository.setFeedbackRating(feedbackName, rating);
        taskManagmentRepository.logEvent("Feedback: " +String.format(SET_FEEDBACK_RATING, createdFeedback.getRating(), createdFeedback.getTitle(), createdFeedback.getId()));
        return String.format(SET_FEEDBACK_RATING, createdFeedback.getRating(), createdFeedback.getTitle(), createdFeedback.getId());
    }

    private void parseParameters(List<String> parameters) {
        feedbackName = parameters.get(0);
        rating = tryParseInteger(parameters.get(1), "Rating for setting feedback rating");
        if (rating > 10 ) rating = 10;
        if (rating < 0 ) rating = 0;

    }
}
