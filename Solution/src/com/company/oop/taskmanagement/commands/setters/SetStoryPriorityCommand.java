package com.company.oop.taskmanagement.commands.setters;

import com.company.oop.taskmanagement.commands.contracts.Command;
import com.company.oop.taskmanagement.core.contracts.TaskManagmentRepository;
import com.company.oop.taskmanagement.models.items.enums.Priority;
import com.company.oop.taskmanagement.models.items.contracts.Story;
import com.company.oop.taskmanagement.utils.ValidationHelper;


import java.util.List;

import static com.company.oop.taskmanagement.commands.CommandsConstants.SET_FEEDBACK_STATUS;
import static com.company.oop.taskmanagement.commands.CommandsConstants.STORY_PRIORITY_CHANGED_MESSAGE;


public class SetStoryPriorityCommand implements Command {

    private final int EXPECTED_NUMBER_OF_ARGUMENTS = 2;
    private String storyName;
    private Priority priority;
    private final TaskManagmentRepository taskManagmentRepository;

    public SetStoryPriorityCommand(TaskManagmentRepository taskManagmentRepository) {
        this.taskManagmentRepository = taskManagmentRepository;
    }

    @Override
    public String execute(List<String> parameters) {

        ValidationHelper.validateArgumentsCount(parameters, EXPECTED_NUMBER_OF_ARGUMENTS);
        parseParameters(parameters);
        Story story = taskManagmentRepository.setStoryPriority(storyName, priority);
        taskManagmentRepository.logEvent("Story: " + String.format(STORY_PRIORITY_CHANGED_MESSAGE, story.getId(), priority));
        return String.format(STORY_PRIORITY_CHANGED_MESSAGE, story.getId(), priority);
    }

    private void parseParameters(List<String> parameters) {
        storyName = parameters.get(0);
        priority = Priority.valueOf(parameters.get(1));
    }
}
