package com.company.oop.taskmanagement.commands.setters;

import com.company.oop.taskmanagement.commands.contracts.Command;
import com.company.oop.taskmanagement.core.contracts.TaskManagmentRepository;
import com.company.oop.taskmanagement.models.items.enums.FeedbackStatus;
import com.company.oop.taskmanagement.models.items.contracts.Feedback;
import com.company.oop.taskmanagement.utils.ParsingHelpers;
import com.company.oop.taskmanagement.utils.ValidationHelper;

import java.util.List;

import static com.company.oop.taskmanagement.commands.CommandsConstants.SET_FEEDBACK_RATING;
import static com.company.oop.taskmanagement.commands.CommandsConstants.SET_FEEDBACK_STATUS;

public class SetFeedbackStatusCommand implements Command {

    public static final int EXPECTED_NUMBER_OF_ARGUMENTS = 2;//id ,status expected

    private final TaskManagmentRepository taskManagmentRepository;

    private String feedbackName;
    private FeedbackStatus feedbackStatus;

    public SetFeedbackStatusCommand(TaskManagmentRepository taskManagmentRepository) {
        this.taskManagmentRepository = taskManagmentRepository;
    }

    @Override
    public String execute(List<String> parameters) {
        ValidationHelper.validateArgumentsCount(parameters, EXPECTED_NUMBER_OF_ARGUMENTS);
        parseParameters(parameters);
        Feedback createdFeedback = taskManagmentRepository.setFeedbackStatus(feedbackName, feedbackStatus);
        taskManagmentRepository.logEvent("Feedback: " + String.format(SET_FEEDBACK_STATUS, createdFeedback.getStatus().toString(), createdFeedback.getTitle(), createdFeedback.getId()));
        return String.format(SET_FEEDBACK_STATUS, createdFeedback.getStatus().toString(), createdFeedback.getTitle(), createdFeedback.getId());
    }

    private void parseParameters(List<String> parameters) {
        feedbackName = parameters.get(0);
        feedbackStatus = ParsingHelpers.tryParseEnum(parameters.get(1), FeedbackStatus.class, "Enum parse error on SetFeedbackStatusCommand");
    }
}
