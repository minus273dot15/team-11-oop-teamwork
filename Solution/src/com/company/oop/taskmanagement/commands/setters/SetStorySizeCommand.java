package com.company.oop.taskmanagement.commands.setters;

import com.company.oop.taskmanagement.commands.contracts.Command;
import com.company.oop.taskmanagement.core.contracts.TaskManagmentRepository;
import com.company.oop.taskmanagement.models.items.enums.Size;
import com.company.oop.taskmanagement.models.items.contracts.Story;
import com.company.oop.taskmanagement.utils.ValidationHelper;

import java.util.List;

import static com.company.oop.taskmanagement.commands.CommandsConstants.SET_STORY_SIZE;
import static com.company.oop.taskmanagement.commands.CommandsConstants.STORY_PRIORITY_CHANGED_MESSAGE;

public class SetStorySizeCommand implements Command {
    private final int EXPECTED_NUMBER_OF_ARGUMENTS = 2;
    private String storyName;
    private Size size;
    private final TaskManagmentRepository taskManagmentRepository;

    public SetStorySizeCommand(TaskManagmentRepository taskManagmentRepository) {
        this.taskManagmentRepository = taskManagmentRepository;
    }

    @Override
    public String execute(List<String> parameters) {
        ValidationHelper.validateArgumentsCount(parameters, EXPECTED_NUMBER_OF_ARGUMENTS);
        parseParameters(parameters);
        Story story = taskManagmentRepository.setStorySize(storyName, size);
        taskManagmentRepository.logEvent("Story: " + String.format(SET_STORY_SIZE, story.getId(), size));
        return String.format(SET_STORY_SIZE, story.getId(), size);
    }

    private void parseParameters(List<String> parameters) {
        storyName = parameters.get(0);
        size = Size.valueOf(parameters.get(1));
    }
}
