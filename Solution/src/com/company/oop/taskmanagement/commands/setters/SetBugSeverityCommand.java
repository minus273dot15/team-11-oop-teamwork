package com.company.oop.taskmanagement.commands.setters;

import com.company.oop.taskmanagement.commands.contracts.Command;
import com.company.oop.taskmanagement.core.contracts.TaskManagmentRepository;
import com.company.oop.taskmanagement.models.items.contracts.Bugs;
import com.company.oop.taskmanagement.models.items.enums.Priority;
import com.company.oop.taskmanagement.models.items.enums.Severity;
import com.company.oop.taskmanagement.utils.ValidationHelper;

import java.util.List;

import static com.company.oop.taskmanagement.commands.CommandsConstants.SET_BUGS_SEVERITY_1;
import static com.company.oop.taskmanagement.utils.ParsingHelpers.tryParseInteger;

public class SetBugSeverityCommand implements Command {

    public static final int EXPECTED_NUMBER_OF_ARGUMENTS = 2;//id and severity
    private final TaskManagmentRepository taskManagmentRepository;
    private String bugName;
    private Severity severity;

    public SetBugSeverityCommand(TaskManagmentRepository taskManagmentRepository) {
        this.taskManagmentRepository = taskManagmentRepository;
    }

    @Override
    public String execute(List<String> parameters) {
        ValidationHelper.validateArgumentsCount(parameters, EXPECTED_NUMBER_OF_ARGUMENTS);
        parseParameters(parameters);
        Bugs setBugsSeverity = taskManagmentRepository.setBugsSeverity(bugName, severity);
        taskManagmentRepository.logEvent("Bug: " + String.format(SET_BUGS_SEVERITY_1, setBugsSeverity.getId(), setBugsSeverity.getSeverity()));
        return String.format(SET_BUGS_SEVERITY_1, setBugsSeverity.getId(), setBugsSeverity.getSeverity());
    }

    private void parseParameters(List<String> parameters) {
        bugName = parameters.get(0);
        severity = Severity.valueOf(parameters.get(1));
    }
}
