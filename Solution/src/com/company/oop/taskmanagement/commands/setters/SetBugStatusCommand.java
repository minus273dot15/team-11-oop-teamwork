package com.company.oop.taskmanagement.commands.setters;

import com.company.oop.taskmanagement.commands.contracts.Command;
import com.company.oop.taskmanagement.core.contracts.TaskManagmentRepository;
import com.company.oop.taskmanagement.models.items.contracts.Bugs;
import com.company.oop.taskmanagement.models.items.enums.BugsStatus;
import com.company.oop.taskmanagement.utils.ValidationHelper;

import java.util.List;

import static com.company.oop.taskmanagement.commands.CommandsConstants.SET_BUGS_BUG_STATUS;
import static com.company.oop.taskmanagement.commands.CommandsConstants.SET_BUGS_PRIORITY_1;

public class SetBugStatusCommand implements Command {
    public static final int EXPECTED_NUMBER_OF_ARGUMENTS = 2;//id and BugStatus

    private final TaskManagmentRepository taskManagmentRepository;

    private String bugName;
    private BugsStatus bugStatus;

    public SetBugStatusCommand(TaskManagmentRepository taskManagmentRepository) {
        this.taskManagmentRepository = taskManagmentRepository;
    }
    @Override
    public String execute(List<String> parameters) {
        ValidationHelper.validateArgumentsCount(parameters, EXPECTED_NUMBER_OF_ARGUMENTS);
        parseParameters(parameters);
        Bugs setBugStatus = taskManagmentRepository.setBugStatus(bugName,bugStatus);
        taskManagmentRepository.logEvent("Bug: " + String.format(SET_BUGS_BUG_STATUS,setBugStatus.getId(), setBugStatus.getStatus()));
        return String.format(SET_BUGS_BUG_STATUS,setBugStatus.getId(), setBugStatus.getStatus());
    }

    private void parseParameters(List<String> parameters) {
        bugName = parameters.get(0);
        bugStatus = BugsStatus.valueOf(parameters.get(1));
    }
}

