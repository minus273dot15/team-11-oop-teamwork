package com.company.oop.taskmanagement.commands.setters;

import com.company.oop.taskmanagement.commands.contracts.Command;
import com.company.oop.taskmanagement.core.contracts.TaskManagmentRepository;
import com.company.oop.taskmanagement.models.items.contracts.Story;
import com.company.oop.taskmanagement.utils.ValidationHelper;

import java.util.List;

import static com.company.oop.taskmanagement.commands.CommandsConstants.*;

public class UnassignStoryCommand implements Command {

    public static final int EXPECTED_NUMBER_OF_ARGUMENTS = 1;//storyName

    private final TaskManagmentRepository taskManagmentRepository;

    private String storyName;

    public UnassignStoryCommand(TaskManagmentRepository taskManagmentRepository) {
        this.taskManagmentRepository = taskManagmentRepository;
    }

    @Override
    public String execute(List<String> parameters) {
        ValidationHelper.validateArgumentsCount(parameters, EXPECTED_NUMBER_OF_ARGUMENTS);
        parseParameters(parameters);

        Story setStoryAssignee = taskManagmentRepository.setStoryAssignee(storyName, "Not Assigned");

        taskManagmentRepository.logEvent("Story: " + String.format(SET_STORY_ASSIGNEE, setStoryAssignee.getTitle(), setStoryAssignee.getAssignee()));
        return String.format(SET_STORY_ASSIGNEE, setStoryAssignee.getTitle(), setStoryAssignee.getAssignee());
    }

    private void parseParameters(List<String> parameters) {
        storyName = parameters.get(0);
        if (!taskManagmentRepository.getStories().contains(taskManagmentRepository.findStoryByName(storyName)))
            throw new IllegalArgumentException(String.format(STORY_DOES_NOT_EXISTS, storyName));
    }
}
