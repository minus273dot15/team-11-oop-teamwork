package com.company.oop.taskmanagement.commands.setters;

import com.company.oop.taskmanagement.commands.contracts.Command;
import com.company.oop.taskmanagement.core.contracts.TaskManagmentRepository;
import com.company.oop.taskmanagement.models.items.contracts.Bugs;
import com.company.oop.taskmanagement.utils.ValidationHelper;

import java.util.List;

import static com.company.oop.taskmanagement.commands.CommandsConstants.*;

public class UnassignBugCommand implements Command {
    public static final int EXPECTED_NUMBER_OF_ARGUMENTS = 1;//storyName

    private final TaskManagmentRepository taskManagmentRepository;

    private String bugName;

    public UnassignBugCommand(TaskManagmentRepository taskManagmentRepository) {
        this.taskManagmentRepository = taskManagmentRepository;
    }

    @Override
    public String execute(List<String> parameters) {
        ValidationHelper.validateArgumentsCount(parameters, EXPECTED_NUMBER_OF_ARGUMENTS);
        parseParameters(parameters);

        Bugs setBugAssignee = taskManagmentRepository.setBugAssignee(bugName, "Not Assigned");

        taskManagmentRepository.logEvent("Bug: " + String.format(SET_BUG_ASSIGNEE, setBugAssignee.getTitle(), setBugAssignee.getAssignee()));
        return String.format(SET_BUG_ASSIGNEE, setBugAssignee.getTitle(), setBugAssignee.getAssignee());
    }

    private void parseParameters(List<String> parameters) {
        bugName = parameters.get(0);
        if (!taskManagmentRepository.getStories().contains(taskManagmentRepository.findBugByName(bugName)))
            throw new IllegalArgumentException(String.format(BUG_DOES_NOT_EXISTS, bugName));
    }
}
