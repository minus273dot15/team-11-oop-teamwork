package com.company.oop.taskmanagement.commands;

import static com.company.oop.taskmanagement.utils.ColorScheme.*;

public class CommandsConstants {

    public static final String JOIN_DELIMITER = "#################### \n";
    public static final String FEEDBACK_CREATED_MESSAGE = "Feedback \"%s\" with rating %d and FeedbackID %d was created on Board \"%s\"";
    public static final String PERSON_CREATED_MESSAGE = "Person \"%s\" with ID %d was created.";
    public static final String SET_FEEDBACK_RATING = "Rating %d for Feedback \"%s\" with FeedbackID %d was set.";
    public static final String SET_FEEDBACK_STATUS = "Status %s for Feedback \"%s\" with FeedbackID %d was set.";
    public static final String BUG_CREATED_MESSAGE = "Bug \"%s\" with BugID %d was created on board \"%s\".";
    public static final String TEAM_CREATED_MESSAGE = "Team \"%s\" with TeamID %d was created.";
    public static final String ADDPERSON_MESSAGE = "Person with name \"%s\" was added to Team \"%s\" with TeamID %d.";

    public static final String FEEDBACK_CREATED_BOARD = "Board with name \"%s\" and BoardID %d was created.";
    public static final String SET_BUGS_PRIORITY_1 = "Priority for Bug with BugID %d was set to %s";
    public static final String SET_BUGS_SEVERITY_1 = "Severity for Bug with BugID %d was set to %s";
    public static final String SET_BUGS_BUG_STATUS = "BugStatus for Bug with BugID %d was set to %s";
    public static final String SET_STORY_SIZE = "Size for Story with StoryID %d was set to %s.";
    public static final String SET_STORY_STATUS = "Status for Story with StoryID %d was set to \"%s\".";
    public static final String STORY_PRIORITY_CHANGED_MESSAGE = "Story with StoryID %d changed priority to %s";
    public static final String STORY_CREATED_MESSAGE = "Story with StoryID %d, Priority \"%s\" was created on board \"%s\".";
    public final static String NO_SUCH_TEAM = ANSI_RED + "There is no team named \"%s\"!" + ANSI_RESET;
    public final static String NO_SUCH_PERSON = ANSI_RED + "There is no person named \"%s\"!" + ANSI_RESET;
    public static final String NO_LOGGED_IN_USER = "There is no logged in user.";
    public final static String COMMENT_ADDED_SUCCESSFULLY = "%s added comment successfully to %s with name \"%s\"!";
    public static final String STORIES_HEADER = "------ Stories -----";
    public static final String BUGS_HEADER = "------ Bugs -----";
    public static final String FEEDBACKS_HEADER = "------ Feedbacks -----";
    public static final String SET_STORY_ASSIGNEE = "Story \"%s\" was assigned to %s!";
    public static final String SET_BUG_ASSIGNEE = "Bug \"%s\" was assigned to %s!";
    public static final String TARGET_NOT_FOUND = ANSI_RED + "Target \"%s\" was not recognized as a name for bug/story/feedback. Check name and try again?" + ANSI_RESET;
    public static final String BOARD_DOES_NOT_EXISTS = ANSI_RED + "Board %s does not exists and you can`t add tasks to it!" + ANSI_RESET;
    public static final String TEAM_DOES_NOT_EXISTS = ANSI_RED + "Team %s does not exists and you can`t add people to it!" + ANSI_RESET;
    public static final String STORY_DOES_NOT_EXISTS = ANSI_RED + "Story %s does not exists and you can`t assign/unassign people to it!" + ANSI_RESET;
    public static final String BUG_DOES_NOT_EXISTS = ANSI_RED + "Bug %s does not exists and you can`t assign/unassign people to it!" + ANSI_RESET;
    public static final String PERSON_DOES_NOT_EXISTS = ANSI_RED + "Person %s does not exists and you can`t assign/unassign tasks for him!" + ANSI_RESET;
    public final static String NO_SUCH_STORY = ANSI_RED + "There is no story named \"%s\"!" + ANSI_RESET;
    public final static String NO_SUCH_FEEDBACK = ANSI_RED + "There is no feedback named \"%s\"!" + ANSI_RESET;
    public static final String NO_SUCH_BOARD = ANSI_RED + "There is no board named \"%s\"!" + ANSI_RESET;
    public final static String NO_SUCH_BUG = ANSI_RED + "There is no bug named \"%s\"!" + ANSI_RESET;
    public static final String TEAM_ALREADY_EXISTS = ANSI_RED + "Team with name \"%s\" already exists! Choose different name!" + ANSI_RESET;
    public static final String PERSON_ALREADY_EXISTS = ANSI_RED + "Person with name \"%s\" already exists! Choose different name!" + ANSI_RESET;
    public static final String BOARD_ALREADY_EXISTS = ANSI_RED + "Board with name \"%s\" already exists for team with name %s! Choose different name!" + ANSI_RESET;
}