package com.company.oop.taskmanagement.commands;

import com.company.oop.taskmanagement.commands.contracts.Command;
import com.company.oop.taskmanagement.core.contracts.TaskManagmentRepository;

import java.util.List;

public abstract class CommandBase implements Command {
    private final TaskManagmentRepository taskManagmentRepository;

    public CommandBase(TaskManagmentRepository taskManagmentRepository) {
        this.taskManagmentRepository = taskManagmentRepository;
    }

    protected TaskManagmentRepository getTaskManagmentRepository() {
        return taskManagmentRepository;
    }
    @Override
    public abstract String execute(List<String> parameters);
}
