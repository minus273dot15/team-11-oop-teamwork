package com.company.oop.taskmanagement.commands.login;

import com.company.oop.taskmanagement.commands.CommandBase;
import com.company.oop.taskmanagement.core.contracts.TaskManagmentRepository;

import java.util.List;

import static com.company.oop.taskmanagement.utils.ColorScheme.ANSI_RED;
import static com.company.oop.taskmanagement.utils.ColorScheme.ANSI_RESET;

public class LogoutCommand extends CommandBase {
    public final static String USER_NOT_LOGGED = ANSI_RED + "User %s is logged in! Please log out first!" + ANSI_RESET;


    public LogoutCommand(TaskManagmentRepository taskManagmentRepository) {
        super(taskManagmentRepository);
    }

    public final static String LOGGED_OUT = "You are now interacting as anonymous";

    @Override
    public String execute(List<String> parameters) {
        //Not logged in?
        if (!getTaskManagmentRepository().hasLoggedInUser()) {
            throw new IllegalArgumentException(
                    String.format(USER_NOT_LOGGED, getTaskManagmentRepository().getLoggedInUser().getName())
            );
        }
        getTaskManagmentRepository().logout();
        return LOGGED_OUT;
    }
}

