package com.company.oop.taskmanagement.commands.login;

import com.company.oop.taskmanagement.commands.CommandBase;
import com.company.oop.taskmanagement.core.contracts.TaskManagmentRepository;
import com.company.oop.taskmanagement.models.contracts.Person;
import com.company.oop.taskmanagement.utils.ValidationHelper;

import java.util.List;

import static com.company.oop.taskmanagement.utils.ColorScheme.ANSI_RED;
import static com.company.oop.taskmanagement.utils.ColorScheme.ANSI_RESET;

public class LoginCommand extends CommandBase {

    private final static String USER_LOGGED_IN = "You are now interacting as %s!";
    private final static String WRONG_USERNAME = ANSI_RED + "Wrong username" + ANSI_RESET;
    public final static String USER_LOGGED_IN_ALREADY = ANSI_RED + "User %s is logged in! Please log out first!" + ANSI_RESET;

    public static final int EXPECTED_NUMBER_OF_ARGUMENTS = 1;

    public LoginCommand(TaskManagmentRepository taskManagmentRepository) {
        super(taskManagmentRepository);
    }

    @Override
    public String execute(List<String> parameters) {
        throwIfUserLoggedIn();
        ValidationHelper.validateArgumentsCount(parameters, EXPECTED_NUMBER_OF_ARGUMENTS);
        String username = parameters.get(0);
        return login(username);
    }

    private String login(String username) {
        Person userFound = getTaskManagmentRepository().findPersonByName(username);
        if (userFound.getName().isEmpty()) throw new IllegalArgumentException(WRONG_USERNAME);

        getTaskManagmentRepository().login(userFound);
        return String.format(USER_LOGGED_IN, username);
    }

    private void throwIfUserLoggedIn() {
        if (getTaskManagmentRepository().hasLoggedInUser()) {
            throw new IllegalArgumentException(
                    String.format(USER_LOGGED_IN_ALREADY, getTaskManagmentRepository().getLoggedInUser().getName())
            );
        }
    }
}
