package com.company.oop.taskmanagement.commands.adders;

import com.company.oop.taskmanagement.commands.contracts.Command;
import com.company.oop.taskmanagement.core.contracts.TaskManagmentRepository;
import com.company.oop.taskmanagement.models.contracts.Team;
import com.company.oop.taskmanagement.utils.ValidationHelper;

import java.util.List;

import static com.company.oop.taskmanagement.commands.CommandsConstants.*;

public class AddPersonToTeamCommand implements Command {

    public static final int EXPECTED_NUMBER_OF_ARGUMENTS = 2;
    private final TaskManagmentRepository taskManagmentRepository;
    private String teamName, personName;
    public AddPersonToTeamCommand(TaskManagmentRepository taskManagmentRepository) {
        this.taskManagmentRepository = taskManagmentRepository;
    }

    @Override
    public String execute(List<String> parameters) {
        ValidationHelper.validateArgumentsCount(parameters, EXPECTED_NUMBER_OF_ARGUMENTS);
        parseParameters(parameters);
        if (!taskManagmentRepository.getTeams().contains(taskManagmentRepository.findTeamByName(teamName)))
            throw new IllegalArgumentException(String.format(TEAM_DOES_NOT_EXISTS, teamName));
        Team createdTeam = taskManagmentRepository.addPersonToTeam(personName, teamName);
        taskManagmentRepository.logEvent(String.format(ADDPERSON_MESSAGE, personName, createdTeam.getName(), createdTeam.getId()));
        return String.format(ADDPERSON_MESSAGE, personName, createdTeam.getName(), createdTeam.getId());
    }

    private void parseParameters(List<String> parameters) {
        teamName = parameters.get(0);
        personName = parameters.get(1);
    }
}
