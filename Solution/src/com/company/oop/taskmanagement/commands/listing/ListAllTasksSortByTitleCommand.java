package com.company.oop.taskmanagement.commands.listing;

import com.company.oop.taskmanagement.commands.contracts.Command;
import com.company.oop.taskmanagement.core.contracts.TaskManagmentRepository;
import com.company.oop.taskmanagement.models.items.contracts.Bugs;
import com.company.oop.taskmanagement.models.items.contracts.Feedback;
import com.company.oop.taskmanagement.models.items.contracts.Story;
import com.company.oop.taskmanagement.models.items.enums.Status;
import com.company.oop.taskmanagement.utils.ListingHelpers;
import com.company.oop.taskmanagement.utils.ValidationHelper;

import java.util.List;

import static com.company.oop.taskmanagement.commands.CommandsConstants.*;
import static com.company.oop.taskmanagement.utils.AnimationHelpers.animateProgress;
import static com.company.oop.taskmanagement.utils.ColorScheme.ANSI_RED;
import static com.company.oop.taskmanagement.utils.ColorScheme.ANSI_RESET;
import static com.company.oop.taskmanagement.utils.ListingHelpers.*;

public class ListAllTasksSortByTitleCommand implements Command {

    public static final int EXPECTED_NUMBER_OF_ARGUMENTS = 0;//nothing expected and still disappointed
    private final List<Story> stories;
    private final List<Bugs> bugs;
    private final List<Feedback> feedbacks;
    private final TaskManagmentRepository taskManagmentRepository;

    public ListAllTasksSortByTitleCommand(TaskManagmentRepository taskManagmentRepository) {
        this.taskManagmentRepository = taskManagmentRepository;
        stories = taskManagmentRepository.getStories();
        bugs = taskManagmentRepository.getBugs();
        feedbacks = taskManagmentRepository.getFeedbacks();
    }

    public String execute(List<String> parameters) {
        ValidationHelper.validateArgumentsCount(parameters, EXPECTED_NUMBER_OF_ARGUMENTS);
        if (!taskManagmentRepository.hasLoggedInUser()) throw new IllegalArgumentException(NO_LOGGED_IN_USER);
        animateProgress();
        StringBuilder stringBuilder = new StringBuilder();
        if(stories.isEmpty() && bugs.isEmpty() & feedbacks.isEmpty()) return ANSI_RED+"There are no registered stories, bugs or feedbacks on your name."+ ANSI_RESET;
        stringBuilder.append(STORIES_HEADER + " \n");
        stringBuilder.append(storiesToStringSortedByTitle(stories));
        stringBuilder.append(BUGS_HEADER + " \n");
        stringBuilder.append(bugsToStringSortedByTitle(bugs));
        stringBuilder.append(FEEDBACKS_HEADER + " \n");
        stringBuilder.append(feedbacksToStringSortedByTitle(feedbacks));

        return stringBuilder.toString();
    }
    //TODO List all bug/feedback/story, stream sort by title in ListHelpers
}
