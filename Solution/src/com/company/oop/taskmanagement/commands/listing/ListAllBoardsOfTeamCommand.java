package com.company.oop.taskmanagement.commands.listing;

import com.company.oop.taskmanagement.commands.contracts.Command;
import com.company.oop.taskmanagement.core.contracts.TaskManagmentRepository;
import com.company.oop.taskmanagement.models.contracts.Board;
import com.company.oop.taskmanagement.utils.ListingHelpers;

import java.util.List;

import static com.company.oop.taskmanagement.utils.AnimationHelpers.animateProgress;
import static com.company.oop.taskmanagement.utils.ColorScheme.ANSI_RED;
import static com.company.oop.taskmanagement.utils.ColorScheme.ANSI_RESET;

    public class ListAllBoardsOfTeamCommand implements Command {
        private List<Board> boards;

        public ListAllBoardsOfTeamCommand(TaskManagmentRepository taskManagmentRepository){
            boards = taskManagmentRepository.getBoards();
        }

        public String execute(List<String> parameters) {
            animateProgress();
            if(boards.isEmpty()) return ANSI_RED + "There is no registered history of boards." + ANSI_RESET;
            return ListingHelpers.boardsOfTeamToString(boards);
        }

    //TODO Implement - list all boards for team X (same as list all Stories, but lists all boards and stream.filter by team in ListingHelpers method)
}
