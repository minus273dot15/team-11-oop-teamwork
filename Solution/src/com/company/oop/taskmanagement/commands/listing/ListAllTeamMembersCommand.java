package com.company.oop.taskmanagement.commands.listing;

import com.company.oop.taskmanagement.commands.contracts.Command;
import com.company.oop.taskmanagement.core.contracts.TaskManagmentRepository;
import com.company.oop.taskmanagement.models.contracts.Team;
import com.company.oop.taskmanagement.utils.ListingHelpers;

import java.util.List;

import static com.company.oop.taskmanagement.utils.AnimationHelpers.animateProgress;
import static com.company.oop.taskmanagement.utils.ColorScheme.ANSI_RED;
import static com.company.oop.taskmanagement.utils.ColorScheme.ANSI_RESET;

public class ListAllTeamMembersCommand implements  Command {
        private final List<Team> teams;

        public ListAllTeamMembersCommand(TaskManagmentRepository taskManagmentRepository){
            teams = taskManagmentRepository.getTeams();
        }

        public String execute(List<String> parameters) {
            animateProgress();
            if(teams.isEmpty()) return ANSI_RED + "There are no registered feedbacks." + ANSI_RESET;
            return ListingHelpers.teamsToString(teams);
        }

    //TODO ListAllTeamMembersCommand - Listing command, should be implemented(make listing helper, create command that calls the list helper)
}
