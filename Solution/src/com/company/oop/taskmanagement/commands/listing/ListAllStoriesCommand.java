package com.company.oop.taskmanagement.commands.listing;

import com.company.oop.taskmanagement.commands.contracts.Command;
import com.company.oop.taskmanagement.core.contracts.TaskManagmentRepository;
import com.company.oop.taskmanagement.models.items.contracts.Story;
import com.company.oop.taskmanagement.utils.ListingHelpers;

import java.util.List;

import static com.company.oop.taskmanagement.utils.AnimationHelpers.animateProgress;
import static com.company.oop.taskmanagement.utils.ColorScheme.ANSI_RED;
import static com.company.oop.taskmanagement.utils.ColorScheme.ANSI_RESET;

public class ListAllStoriesCommand implements Command {
    private final List<Story> stories;

    public ListAllStoriesCommand(TaskManagmentRepository taskManagmentRepository){
        stories = taskManagmentRepository.getStories();
    }

    public String execute(List<String> parameters) {
        animateProgress();
        if(stories.isEmpty()) return ANSI_RED+"There are no registered stories."+ ANSI_RESET;
        return ListingHelpers.storiesToString(stories);
    }
}
