package com.company.oop.taskmanagement.commands.listing;

import com.company.oop.taskmanagement.commands.contracts.Command;
import com.company.oop.taskmanagement.core.contracts.TaskManagmentRepository;
import com.company.oop.taskmanagement.models.items.contracts.Bugs;
import com.company.oop.taskmanagement.models.items.contracts.Feedback;
import com.company.oop.taskmanagement.models.items.contracts.Story;
import com.company.oop.taskmanagement.models.items.enums.Status;
import com.company.oop.taskmanagement.utils.ValidationHelper;

import java.util.List;

import static com.company.oop.taskmanagement.commands.CommandsConstants.*;
import static com.company.oop.taskmanagement.utils.AnimationHelpers.animateProgress;
import static com.company.oop.taskmanagement.utils.ColorScheme.ANSI_RED;
import static com.company.oop.taskmanagement.utils.ColorScheme.ANSI_RESET;
import static com.company.oop.taskmanagement.utils.ListingHelpers.bugsToStringFilteredByAssignee;
import static com.company.oop.taskmanagement.utils.ListingHelpers.storiesToStringFilteredByAssignee;

public class ListMyTasksCommand implements Command {

    public static final int EXPECTED_NUMBER_OF_ARGUMENTS = 0;//nothing expected and still disappointed
    private final List<Story> stories;
    private final List<Bugs> bugs;
    private final TaskManagmentRepository taskManagmentRepository;

    public ListMyTasksCommand(TaskManagmentRepository taskManagmentRepository) {
        this.taskManagmentRepository = taskManagmentRepository;
        stories = taskManagmentRepository.getStories();
        bugs = taskManagmentRepository.getBugs();
    }

    public String execute(List<String> parameters) {
        ValidationHelper.validateArgumentsCount(parameters, EXPECTED_NUMBER_OF_ARGUMENTS);
        if (!taskManagmentRepository.hasLoggedInUser()) throw new IllegalArgumentException(NO_LOGGED_IN_USER);
        StringBuilder stringBuilder = new StringBuilder();
        if(stories.isEmpty() && bugs.isEmpty()) return ANSI_RED+"There are no registered stories or bugs on your name."+ ANSI_RESET;
        animateProgress();
        stringBuilder.append(STORIES_HEADER + " \n");
        stringBuilder.append(storiesToStringFilteredByAssignee(stories, taskManagmentRepository.getLoggedInUserName()));
        stringBuilder.append(BUGS_HEADER + " \n");
        stringBuilder.append(bugsToStringFilteredByAssignee(bugs, taskManagmentRepository.getLoggedInUserName()));

        return stringBuilder.toString();
    }
}
