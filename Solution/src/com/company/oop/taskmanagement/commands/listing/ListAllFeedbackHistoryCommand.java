package com.company.oop.taskmanagement.commands.listing;

import com.company.oop.taskmanagement.commands.contracts.Command;
import com.company.oop.taskmanagement.core.contracts.TaskManagmentRepository;
import com.company.oop.taskmanagement.models.items.HistoryImpl;
import com.company.oop.taskmanagement.utils.ListingHelpers;

import java.util.List;

import static com.company.oop.taskmanagement.utils.AnimationHelpers.animateProgress;
import static com.company.oop.taskmanagement.utils.ColorScheme.ANSI_RED;
import static com.company.oop.taskmanagement.utils.ColorScheme.ANSI_RESET;

public class ListAllFeedbackHistoryCommand implements Command {
    private List<HistoryImpl> histories;

    public ListAllFeedbackHistoryCommand(TaskManagmentRepository taskManagmentRepository){
        histories = taskManagmentRepository.getHistory();
    }

    public String execute(List<String> parameters) {
        animateProgress();
        if(histories.isEmpty()) return ANSI_RED + "There is no registered history of feedbacks." + ANSI_RESET;
        return ListingHelpers.feedbacksHistoryToString(histories);
    }
}
