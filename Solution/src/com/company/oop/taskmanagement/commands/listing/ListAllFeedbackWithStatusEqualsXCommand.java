package com.company.oop.taskmanagement.commands.listing;

import com.company.oop.taskmanagement.commands.contracts.Command;
import com.company.oop.taskmanagement.core.contracts.TaskManagmentRepository;
import com.company.oop.taskmanagement.models.items.contracts.Feedback;
import com.company.oop.taskmanagement.models.items.enums.FeedbackStatus;
import com.company.oop.taskmanagement.utils.ListingHelpers;
import com.company.oop.taskmanagement.utils.ValidationHelper;

import java.util.List;

import static com.company.oop.taskmanagement.utils.AnimationHelpers.animateProgress;
import static com.company.oop.taskmanagement.utils.ColorScheme.ANSI_RED;
import static com.company.oop.taskmanagement.utils.ColorScheme.ANSI_RESET;

public class ListAllFeedbackWithStatusEqualsXCommand implements Command {
    public static final int EXPECTED_NUMBER_OF_ARGUMENTS = 1;//only target status to filter by expected
    private final List<Feedback> feedbacks;

    public ListAllFeedbackWithStatusEqualsXCommand(TaskManagmentRepository taskManagmentRepository){
        feedbacks = taskManagmentRepository.getFeedbacks();
    }

    public String execute(List<String> parameters) {
        ValidationHelper.validateArgumentsCount(parameters, EXPECTED_NUMBER_OF_ARGUMENTS);
        animateProgress();
        FeedbackStatus targetStatus = FeedbackStatus.valueOf(parameters.get(0));
        if(feedbacks.isEmpty()) return ANSI_RED+"There are no registered feedbacks."+ ANSI_RESET;
        return ListingHelpers.feedbacksToStringFilteredStatus(feedbacks, targetStatus);
    }
}
