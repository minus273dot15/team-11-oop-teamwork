package com.company.oop.taskmanagement.commands.listing;

import com.company.oop.taskmanagement.commands.contracts.Command;
import com.company.oop.taskmanagement.core.contracts.TaskManagmentRepository;
import com.company.oop.taskmanagement.models.items.contracts.Bugs;
import com.company.oop.taskmanagement.utils.ListingHelpers;

import java.util.List;

import static com.company.oop.taskmanagement.utils.AnimationHelpers.animateProgress;
import static com.company.oop.taskmanagement.utils.ColorScheme.ANSI_RED;
import static com.company.oop.taskmanagement.utils.ColorScheme.ANSI_RESET;

public class ListAllBugsCommand implements Command{
    private final List<Bugs> bugs;

    public ListAllBugsCommand(TaskManagmentRepository taskManagmentRepository){
        bugs = taskManagmentRepository.getBugs();
    }

    public String execute(List<String> parameters) {
        animateProgress();
        if(bugs.isEmpty()) return ANSI_RED+"There are no registered bugs."+ ANSI_RESET;
        return ListingHelpers.bugsToString(bugs);
    }
}
