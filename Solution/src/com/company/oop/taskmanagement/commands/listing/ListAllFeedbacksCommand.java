package com.company.oop.taskmanagement.commands.listing;

import com.company.oop.taskmanagement.commands.contracts.Command;
import com.company.oop.taskmanagement.core.contracts.TaskManagmentRepository;
import com.company.oop.taskmanagement.models.items.contracts.Feedback;
import com.company.oop.taskmanagement.utils.ListingHelpers;

import java.util.List;

import static com.company.oop.taskmanagement.utils.AnimationHelpers.animateProgress;
import static com.company.oop.taskmanagement.utils.ColorScheme.ANSI_RED;
import static com.company.oop.taskmanagement.utils.ColorScheme.ANSI_RESET;

public class ListAllFeedbacksCommand implements Command {

    private final List<Feedback> feedbacks;

    public ListAllFeedbacksCommand(TaskManagmentRepository taskManagmentRepository){
        feedbacks = taskManagmentRepository.getFeedbacks();
    }

    public String execute(List<String> parameters) {
        animateProgress();
        return ListingHelpers.feedbacksToString(feedbacks);
    }
}
