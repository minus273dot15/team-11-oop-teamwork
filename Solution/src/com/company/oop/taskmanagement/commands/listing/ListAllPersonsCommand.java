package com.company.oop.taskmanagement.commands.listing;

import com.company.oop.taskmanagement.commands.contracts.Command;
import com.company.oop.taskmanagement.core.contracts.TaskManagmentRepository;
import com.company.oop.taskmanagement.models.contracts.Person;
import com.company.oop.taskmanagement.utils.ListingHelpers;

import java.util.List;

import static com.company.oop.taskmanagement.utils.AnimationHelpers.animateProgress;
import static com.company.oop.taskmanagement.utils.AnimationHelpers.progressPercentage;

public class ListAllPersonsCommand implements Command {

    private final List<Person> personList;

    public ListAllPersonsCommand(TaskManagmentRepository taskManagmentRepository){
        personList = taskManagmentRepository.getPersons();
    }

    public String execute(List<String> parameters) {
        animateProgress();
        if(personList.isEmpty()) return "There are no registered persons.";
        return ListingHelpers.personsToString(personList);
    }
}
