package com.company.oop.taskmanagement.commands.eggs;

import com.company.oop.taskmanagement.commands.CommandBase;
import com.company.oop.taskmanagement.commands.contracts.Command;
import com.company.oop.taskmanagement.core.contracts.TaskManagmentRepository;
import com.company.oop.taskmanagement.utils.ValidationHelper;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.List;

public class AsciiDrawString implements Command {

    public static final int EXPECTED_NUMBER_OF_ARGUMENTS = 1;//expecting string to work on
    private final TaskManagmentRepository taskManagmentRepository;
    private String stringToDraw;

    private List<String> settings;


    public AsciiDrawString(TaskManagmentRepository taskManagmentRepository) {
        this.taskManagmentRepository = taskManagmentRepository;
    }

    public String execute(List<String> parameters) {
        ValidationHelper.validateArgumentsCount(parameters, EXPECTED_NUMBER_OF_ARGUMENTS);
        parseParameters(parameters);

        int width = 80, height = 12;
        BufferedImage bufferedImage = new BufferedImage(
                width, height,
                BufferedImage.TYPE_INT_RGB);
        Graphics graphics = bufferedImage.getGraphics();

        Graphics2D graphics2D = (Graphics2D) graphics;
        graphics2D.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING,
                RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
        graphics2D.drawString(stringToDraw, 4, 10);

        StringBuilder stringBuilder = new StringBuilder();

        for (int y = 0; y < height; y++) {

            for (int x = 0; x < width; x++) {
                stringBuilder.append(bufferedImage.getRGB(x, y) == -16777216 ? "8" : " ");
            }

            if (stringBuilder.toString().trim().isEmpty()) {
                continue;
            }
            stringBuilder.append("\n");
        }

        return stringBuilder.toString();
    }

    private void parseParameters(List<String> parameters) {
        stringToDraw = String.valueOf(parameters.get(0));
    }

}
