package com.company.oop.taskmanagement.utils;

import com.company.oop.taskmanagement.models.contracts.Board;
import com.company.oop.taskmanagement.models.contracts.Person;
import com.company.oop.taskmanagement.models.items.contracts.Bugs;
import com.company.oop.taskmanagement.models.items.contracts.Feedback;
import com.company.oop.taskmanagement.models.items.contracts.Story;

import java.util.ArrayList;
import java.util.List;

public class DummyArrayLists {
    public static final List<Feedback> dummyFeedbacks = new ArrayList<>();
    public static final List<Story> dummyStories = new ArrayList<>();
    public static final List<Bugs> dummyBugs = new ArrayList<>();
    public static final List<Person> dummyMembers= new ArrayList<>();
    public static final List<Board> dummyBoards = new ArrayList<>();
    public static List<String> dummyActivityHistory = new ArrayList<>();
    public static List<String> dummyTasks = new ArrayList<>();
    public static List<String> stepsToReproduce = new ArrayList<>();
}
