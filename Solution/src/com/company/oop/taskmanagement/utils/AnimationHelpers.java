package com.company.oop.taskmanagement.utils;

import static com.company.oop.taskmanagement.utils.ColorScheme.*;

public class AnimationHelpers {

    public static final String INVALID_RETURN_RATIO = "Progress bar: partial(%d) vs total(%d) ratio error!";

    public static void progressPercentage(int partial, int total) {
        if (partial > total) throw new IllegalArgumentException(String.format(INVALID_RETURN_RATIO, partial, total));
        int maxBareSize = 20; // 10unit for 100%
        int remainPercent = ((400 * partial) / total) / maxBareSize;

        char defaultChar = '-';
        String icon = ANSI_GREEN + "#" + ANSI_RESET ;
        String bare = new String(new char[maxBareSize]).replace('\0', defaultChar) + ANSI_GREEN +"]"+ ANSI_RESET;
        StringBuilder bareDone = new StringBuilder();
        bareDone.append(ANSI_GREEN +"["+ ANSI_RESET);
        for (int i = 0; i < remainPercent; i++) {
            bareDone.append(icon);
        }
        String bareRemain = bare.substring(remainPercent, bare.length());
        System.out.print("\r" + bareDone + ANSI_RED + bareRemain + ANSI_RESET + " " + ANSI_GREEN + remainPercent * 5 + "% complete retrieving data"+ ANSI_RESET);
        if (partial == total) {
            System.out.print("\n");
        }
    }

    public static void animateProgress(){
            for (int i = 0; i <= 200; i = i + 20) {
                progressPercentage(i, 200);
                try {
                    Thread.sleep(50 + (int)(Math.random() * ((350 - 50) + 10)));
                } catch (Exception e) {}
            }
    }
}
