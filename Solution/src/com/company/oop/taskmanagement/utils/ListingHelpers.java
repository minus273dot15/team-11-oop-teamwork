package com.company.oop.taskmanagement.utils;

import com.company.oop.taskmanagement.commands.CommandsConstants;
import com.company.oop.taskmanagement.models.contracts.Board;
import com.company.oop.taskmanagement.models.contracts.Person;
import com.company.oop.taskmanagement.models.contracts.Team;
import com.company.oop.taskmanagement.models.items.HistoryImpl;
import com.company.oop.taskmanagement.models.items.contracts.Bugs;
import com.company.oop.taskmanagement.models.items.contracts.Feedback;
import com.company.oop.taskmanagement.models.items.contracts.Story;
import com.company.oop.taskmanagement.models.items.contracts.Task;
import com.company.oop.taskmanagement.models.items.enums.*;

import java.util.Comparator;
import java.util.List;

import static com.company.oop.taskmanagement.utils.ColorScheme.ANSI_RED;
import static com.company.oop.taskmanagement.utils.ColorScheme.ANSI_RESET;

public class ListingHelpers {

    public static String storiesToString(List<Story> stories) {
        return stories.stream()
                .map(Object::toString)
                .reduce((s1, s2) -> s1 + "\n" + CommandsConstants.JOIN_DELIMITER + "\n" + s2)
                .orElse(ANSI_RED + "List of stories is empty" + ANSI_RESET);
    }

    public static String feedbacksToString(List<Feedback> feedbacks) {
        return feedbacks.stream()
                .map(Object::toString)
                .reduce((s1, s2) -> s1 + "\n" + CommandsConstants.JOIN_DELIMITER + "\n" + s2)
                .orElse(ANSI_RED + "List of feedbacks is empty" + ANSI_RESET);
    }
    public static String feedbacksHistoryToString(List<HistoryImpl> history) {
        return history.stream()
                .map(Object::toString)
                .filter(histories -> histories.contains("Feedback"))
                .reduce((s1, s2) -> s1 + "\n" + CommandsConstants.JOIN_DELIMITER + "\n" + s2)
                .orElse(ANSI_RED + "History of feedbacks is empty" + ANSI_RESET);
    }

    public static String storiesHistoryToString(List<HistoryImpl> history) {
        return history.stream()
                .map(Object::toString)
                .filter(histories -> histories.contains("Story"))
                .reduce((s1, s2) -> s1 + "\n" + CommandsConstants.JOIN_DELIMITER + "\n" + s2)
                .orElse(ANSI_RED + "History of stories is empty" + ANSI_RESET);
    }

    public static String bugsHistoryToString(List<HistoryImpl> history) {
        return history.stream()
                .map(Object::toString)
                .filter(histories -> histories.contains("Bug"))
                .reduce((s1, s2) -> s1 + "\n" + CommandsConstants.JOIN_DELIMITER + "\n" + s2)
                .orElse(ANSI_RED + "History of bugs is empty" + ANSI_RESET);
    }

    public static String personsToString(List<Person> personList) {
        return personList.stream()
                .map(Object::toString)
                .reduce((s1, s2) -> s1 + "\n" + CommandsConstants.JOIN_DELIMITER + "\n" + s2)
                .orElse(ANSI_RED + "List of persons is empty" + ANSI_RESET);
    }

    public static String bugsToString(List<Bugs> bugs) {
        return bugs.stream()
                .map(Object::toString)
                .reduce((s1, s2) -> s1 + "\n" + CommandsConstants.JOIN_DELIMITER + "\n" + s2)
                .orElse(ANSI_RED + "List of bugs is empty" + ANSI_RESET);
    }
    //feedbacksToStringFiltered
    public static String feedbacksToStringFiltered(List<Feedback> feedbacks, int targetRating) {
        return feedbacks.stream()
                .filter(feedback -> feedback.getRating() == targetRating)
                .map(Object::toString)
                .reduce((s1, s2) -> s1 + "\n" + CommandsConstants.JOIN_DELIMITER + "\n" + s2)
                .orElse(ANSI_RED + "List of feedbacks matching RATING: " + targetRating + " is empty" + ANSI_RESET);
    }
    public static String feedbacksToStringSortedByRating(List<Feedback> feedbacks) {
        return feedbacks.stream()
                .sorted(Comparator.comparingInt(Feedback::getRating))
                .map(Object::toString)
                .reduce((s1, s2) -> s1 + "\n" + CommandsConstants.JOIN_DELIMITER + "\n" + s2)
                .orElse(ANSI_RED + "List of feedbacks is empty" + ANSI_RESET);
    }

    public static String feedbacksToStringSortedByTitle(List<Feedback> feedbacks) {
        return feedbacks.stream()
                .sorted(Comparator.comparingInt(Feedback::getRating))
                .map(Object::toString)
                .reduce((s1, s2) -> s1 + "\n" + CommandsConstants.JOIN_DELIMITER + "\n" + s2)
                .orElse(ANSI_RED + "List of feedbacks is empty" + ANSI_RESET);
    }

    public static String feedbacksToStringFilteredStatus(List<Feedback> feedbacks, FeedbackStatus targetStatus) {
        return feedbacks.stream()
                .filter(feedback -> feedback.getStatus().equals(targetStatus))
                .map(Object::toString)
                .reduce((s1, s2) -> s1 + "\n" + CommandsConstants.JOIN_DELIMITER + "\n" + s2)
                .orElse(ANSI_RED + "List of feedbacks matching STATUS: " + targetStatus + " is empty" + ANSI_RESET);
    }
    public static String storiesToStringFiltered(List<Story> stories, Status status) {
        return stories.stream()
                .filter(story -> story.getStatus().equals(status))
                .map(Object::toString)
                .reduce((s1, s2) -> s1 + "\n" + CommandsConstants.JOIN_DELIMITER + "\n" + s2)
                .orElse(ANSI_RED + "List of stories matching STATUS: " + status + " is empty" + ANSI_RESET);
    }

    public static String storiesToStringSortedBySize(List<Story> stories) {
        return stories.stream()
                .sorted(Comparator.comparing(s -> s.getSize().ordinal()))
                .map(Object::toString)
                .reduce((s1, s2) -> s1 + "\n" + CommandsConstants.JOIN_DELIMITER + "\n" + s2)
                .orElse(ANSI_RED + "List of stories is empty" + ANSI_RESET);
    }

    public static String storiesToStringSortedByPriority(List<Story> stories) {
        return stories.stream()
                .sorted(Comparator.comparing(s -> s.getPriority().ordinal()))
                .map(Object::toString)
                .reduce((s1, s2) -> s1 + "\n" + CommandsConstants.JOIN_DELIMITER + "\n" + s2)
                .orElse(ANSI_RED + "List of stories is empty" + ANSI_RESET);
    }
    public static String storiesToStringSortedByTitle(List<Story> stories) {
        return stories.stream()
                .sorted(Comparator.comparing(Story::getTitle))
                .map(Object::toString)
                .reduce((s1, s2) -> s1 + "\n" + CommandsConstants.JOIN_DELIMITER + "\n" + s2)
                .orElse(ANSI_RED + "List of stories is empty" + ANSI_RESET);
    }
    public static String storiesToStringFilteredByAssignee(List<Story> stories, String targetAssignee) {
        return stories.stream()
                .filter(bug -> bug.getAssignee().equalsIgnoreCase(targetAssignee))
                .map(Object::toString)
                .reduce((s1, s2) -> s1 + "\n" + CommandsConstants.JOIN_DELIMITER + "\n" + s2)
                .orElse(ANSI_RED + "List of bugs matching you as assignee is empty \n" + ANSI_RESET);
    }

    public static String bugsToStringFiltered(List<Bugs> bugs, BugsStatus targetStatus) {
        return bugs.stream()
                .filter(bug -> bug.getStatus().equals(targetStatus))
                .map(Object::toString)
                .reduce((s1, s2) -> s1 + "\n" + CommandsConstants.JOIN_DELIMITER + "\n" + s2)
                .orElse(ANSI_RED + "List of bugs matching STATUS: " + targetStatus + " is empty \n" + ANSI_RESET);
    }
    public static String bugsToStringSortedByTitle(List<Bugs> bugs) {
        return bugs.stream()
                .sorted(Comparator.comparing(Bugs::getTitle))
                .map(Object::toString)
                .reduce((s1, s2) -> s1 + "\n" + CommandsConstants.JOIN_DELIMITER + "\n" + s2)
                .orElse(ANSI_RED + "List of bugs is empty \n" + ANSI_RESET);
    }
    public static String bugsToStringSortedByPriority(List<Bugs> bugs) {
        return bugs.stream()
                .sorted(Comparator.comparing(s -> s.getPriority().ordinal()))
                .map(Object::toString)
                .reduce((s1, s2) -> s1 + "\n" + CommandsConstants.JOIN_DELIMITER + "\n" + s2)
                .orElse(ANSI_RED + "List of bugs is empty \n" + ANSI_RESET);
    }
    public static String bugsToStringSortedBySeverity(List<Bugs> bugs) {
        return bugs.stream()
                .sorted(Comparator.comparing(s -> s.getSeverity().ordinal()))
                .map(Object::toString)
                .reduce((s1, s2) -> s1 + "\n" + CommandsConstants.JOIN_DELIMITER + "\n" + s2)
                .orElse(ANSI_RED + "List of bugs is empty \n" + ANSI_RESET);
    }
    public static String bugsToStringFilteredByAssignee(List<Bugs> bugs, String targetAssignee) {
        return bugs.stream()
                .filter(bug -> bug.getAssignee().equalsIgnoreCase(targetAssignee))
                .map(Object::toString)
                .reduce((s1, s2) -> s1 + "\n" + CommandsConstants.JOIN_DELIMITER + "\n" + s2)
                .orElse("List of bugs matching you as assignee is empty");
    }

    public static String teamsToString(List<Team> teams) {
        return teams.stream()
                .map(Object::toString)
                .reduce((s1, s2) -> s1 + "\n" + CommandsConstants.JOIN_DELIMITER + "\n" + s2)
                .orElse("List of teams is empty");
    }
    public static String boardsOfTeamToString(List<Board> boards) {
        return boards.stream()
//                .filter(team -> team.getBoards().equals //  Как да го филтрирам?
                .map(Object::toString) //тук трябва да се направи филтъра | stream.filter by team
                .reduce((s1, s2) -> s1 + "\n" + CommandsConstants.JOIN_DELIMITER + "\n" + s2)
                .orElse("List of boards is empty");
    }


//    public static <T extends Printable> String elementsToString(List<T> elements) {
//        List<String> result = new ArrayList<>();
//        for (T element : elements) {
//            result.add(element.toString());
//        }
//        return String.join(CommandsConstants.JOIN_DELIMITER + System.lineSeparator(), result).trim();
//    }

}
