package com.company.oop.taskmanagement.models;

import com.company.oop.taskmanagement.models.contracts.Person;
import com.company.oop.taskmanagement.utils.ValidationHelper;

import java.util.ArrayList;
import java.util.List;

import static com.company.oop.taskmanagement.utils.ColorScheme.ANSI_RESET;
import static com.company.oop.taskmanagement.utils.ColorScheme.ANSI_YELLOW;

public class PersonImpl implements Person {

    private final int id;
    private String name;
    private List<String> activityHistory;
    private List<String> tasks;

    public static final int NAME_MIN_LENGTH = 5;
    public static final int NAME_MAX_LENGTH = 15;
    public static final String NAME_ERROR_MESSAGE = "The Name cannot be less than 5 or more than 15 symbols.";



    public PersonImpl(int id, String name, List<String> activityHistory, List<String> tasks) {
        setName(name);
        this.id = id;
        this.activityHistory = activityHistory;
        this.tasks = tasks;
    }

    public void setName(String name) {
        ValidationHelper.validateStringLength(name,NAME_MIN_LENGTH,NAME_MAX_LENGTH,NAME_ERROR_MESSAGE);
        this.name = name;
    }
    @Override
    public String getName() {
        return name;
    }
    @Override
    public List<String> getActivityHistory() {
        return activityHistory = new ArrayList<>();
    }
    @Override
    public List<String> getTasks() {
        return tasks = new ArrayList<>();
    }

    @Override
    public String getAsString() {
        return  "---- Person ----\n" +
                ANSI_YELLOW + "PersonID:    " + ANSI_RESET + getId() + "\n" +
                ANSI_YELLOW + "Person name: " + ANSI_RESET + getName() + "\n";
    }

    @Override
    public String toString() {
        return getAsString();
    }

    @Override
    public int getId() {
        return id;
    }
}
