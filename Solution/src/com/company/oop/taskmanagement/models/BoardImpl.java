package com.company.oop.taskmanagement.models;

import com.company.oop.taskmanagement.models.contracts.Board;
import com.company.oop.taskmanagement.models.items.contracts.Bugs;
import com.company.oop.taskmanagement.models.items.contracts.Feedback;
import com.company.oop.taskmanagement.models.items.contracts.Story;
import com.company.oop.taskmanagement.utils.ValidationHelper;

import java.util.ArrayList;
import java.util.List;

import static com.company.oop.taskmanagement.utils.ColorScheme.ANSI_RESET;
import static com.company.oop.taskmanagement.utils.ColorScheme.ANSI_YELLOW;

public class BoardImpl implements Board {

    private String name, belongsToTeam;
    private List<String> activityHistory;
    private List<String> tasks;
    private int id, countMF = 0;
    private List<Bugs> boardBugs = new ArrayList<>();
    private List<Story> boardStories= new ArrayList<>();
    private List<Feedback> boardFeedbacks = new ArrayList<>();

    public static final int BOARD_NAME_MIN_LENGTH = 5;
    public static final int BOARD_NAME_MAX_LENGTH = 10;
    public static final String BOARD_NAME_ERROR_MESSAGE = "The Board name cannot be less than "
        + BOARD_NAME_MIN_LENGTH + " or more than "
        + BOARD_NAME_MAX_LENGTH + " symbols.";

    public BoardImpl(int id, String name, List<Feedback> boardFeedbacks, List<Story> boardStories, List<Bugs> boardBugs, String belongsToTeam) {
        setName(name);
        this.belongsToTeam = belongsToTeam;
        this.boardFeedbacks = boardFeedbacks;
        this.boardStories = boardStories;
        this.boardBugs = boardBugs;
        this.id = id;
        activityHistory = new ArrayList<>();
        tasks = new ArrayList<>();
    }

    public void setName(String name) {
        ValidationHelper.validateStringLength(name,BOARD_NAME_MIN_LENGTH,BOARD_NAME_MAX_LENGTH,BOARD_NAME_ERROR_MESSAGE);
        this.name = name;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public List<String> getActivityHistory() { return new ArrayList<>(activityHistory); }

    @Override
    public List<String> getTasks() { return new ArrayList<>(tasks); }

    @Override
    public List<Story> getStories() {
        return new ArrayList<>(boardStories);
    }

    @Override
    public List<Bugs> getBugs() {
        return new ArrayList<>(boardBugs);
    }

    @Override
    public List<Feedback> getFeedbacks() {
        return new ArrayList<>(boardFeedbacks);
    }

    @Override
    public String getAsString() {
        StringBuilder str = new StringBuilder();
        str.append(ANSI_YELLOW + "----- Board " + name + " -----" + ANSI_RESET);
        if (!boardBugs.isEmpty()) str.append("-/- Bugs -\\-");
        //printout each bug, yet it is stored in repo?
        getBugs().forEach(bugs -> {
            str.append(ANSI_YELLOW + "##### BUG " + countMF + "#####" + ANSI_RESET);
            str.append(ANSI_YELLOW + "Bug name: " + ANSI_RESET + bugs.getTitle());
            countMF++;
        });countMF = 0;
        //printout each bug, yet it is stored in repo?
        if (!boardStories.isEmpty()) str.append("-/- Stories -\\-");
        getStories().forEach(story -> {
            str.append(ANSI_YELLOW + "##### STORY " + countMF + "#####" + ANSI_RESET);
            str.append(ANSI_YELLOW + "Story name: " + ANSI_RESET + story.getTitle());
            countMF++;
        });countMF = 0;
        //printout each bug, yet it is stored in repo?
        if (!boardFeedbacks.isEmpty()) str.append("-/- Feedbacks -\\-");
        getFeedbacks().forEach(feedback -> {
            str.append(ANSI_YELLOW + "##### FEEDBACK " + countMF + "#####" + ANSI_RESET);
            str.append(ANSI_YELLOW + "Feedback name: " + ANSI_RESET + feedback.getTitle());
            countMF++;
        });
        return str.toString();
    }

    @Override
    public String toString(){
        return getAsString();
    }

    public String getBelongsToTeam() {
        return belongsToTeam;
    }

    @Override
    public int getId() {
        return id;
    }
}
