package com.company.oop.taskmanagement.models.contracts;

import java.util.List;

public interface Person extends Identifiable, Printable {
    String getName();

    List<String> getActivityHistory();

    List<String> getTasks();

    String getAsString();

}
