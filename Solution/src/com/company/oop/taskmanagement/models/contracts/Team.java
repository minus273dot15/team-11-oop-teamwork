package com.company.oop.taskmanagement.models.contracts;

import java.util.List;

public interface Team extends Identifiable {

    String getName();

    List<Person> getMembers();

    List<Board> getBoards();

    String getAsString();

    void setMembers(List<Person> members);


}

