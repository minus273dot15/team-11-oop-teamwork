package com.company.oop.taskmanagement.models.contracts;

import com.company.oop.taskmanagement.models.items.contracts.Bugs;
import com.company.oop.taskmanagement.models.items.contracts.Feedback;
import com.company.oop.taskmanagement.models.items.contracts.Story;

import java.util.List;

public interface Board extends Printable, Identifiable {
    String getName();

    List<String> getActivityHistory();

    List<String> getTasks();

    List<Story> getStories();
    List<Bugs> getBugs();
    List<Feedback> getFeedbacks();

    String getBelongsToTeam();

    String getAsString();

}
