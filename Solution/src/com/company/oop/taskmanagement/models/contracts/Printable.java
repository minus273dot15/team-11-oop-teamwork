package com.company.oop.taskmanagement.models.contracts;

public interface Printable {

    String toString();

}
