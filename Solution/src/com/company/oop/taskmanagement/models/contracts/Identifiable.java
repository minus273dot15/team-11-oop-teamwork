package com.company.oop.taskmanagement.models.contracts;

public interface Identifiable {

    int getId();

}
