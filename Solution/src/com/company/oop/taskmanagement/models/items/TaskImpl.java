package com.company.oop.taskmanagement.models.items;

import com.company.oop.taskmanagement.models.contracts.Person;
import com.company.oop.taskmanagement.models.items.contracts.Comments;
import com.company.oop.taskmanagement.models.items.contracts.History;
import com.company.oop.taskmanagement.models.items.contracts.Task;
import com.company.oop.taskmanagement.utils.ValidationHelper;

import java.util.ArrayList;
import java.util.List;

public abstract class TaskImpl implements Task {

    String description, title, belongsToBoard;
    int id;
    private List<Comments> comments = new ArrayList<Comments>();
    private List<History> histories = new ArrayList<History>();

    public static final int DESCRIPTION_MIN_LENGTH = 10;
    public static final int DESCRIPTION_MAX_LENGTH = 500;
    public static final String DESCRIPTION_ERR_MSG = "The description should be "
            + DESCRIPTION_MIN_LENGTH + " symbols minimum and"
            + DESCRIPTION_MAX_LENGTH + " symbols maximum.";

    public static final int TITLE_MIN_LENGTH = 10;
    public static final int TITLE_MAX_LENGTH = 50;
    public static final String TITLE_ERR_MSG = "The title should be "
            + TITLE_MIN_LENGTH + " symbols minimum and"
            + TITLE_MAX_LENGTH + " symbols maximum.";


    public TaskImpl(int id, String title, String description, String belongsToBoard) {
        setTitle(title);
        setId(id);
        setDescription(description);
        this.belongsToBoard = belongsToBoard;
    }

    public int getId() {
        return id;
    }

    private void setId(int id) {
        this.id = id;
    }

    public void setDescription(String description) {
        ValidationHelper.validateStringLength(description, DESCRIPTION_MIN_LENGTH, DESCRIPTION_MAX_LENGTH, DESCRIPTION_ERR_MSG);
        this.description = description;
    }

    public void setTitle(String title) {
        ValidationHelper.validateStringLength(title, TITLE_MIN_LENGTH, TITLE_MAX_LENGTH, TITLE_ERR_MSG);
        this.title = title;
    }

    @Override
    public List<Comments> getComments() {
        return new ArrayList<>(comments);
    }

    @Override
    public void addComment(Person author, String message) {
        Comments comment = new CommentsImpl(author, message);
        comments.add(comment);
    }
    @Override
    public void removeComment(int index) {
        if (index >= 0 && index < comments.size())
            comments.remove(index);
    }

    public String getAsString() {
        return "------ General Task ------";
    }

    @Override
    public String toString() {
        return getAsString();
    }
}
