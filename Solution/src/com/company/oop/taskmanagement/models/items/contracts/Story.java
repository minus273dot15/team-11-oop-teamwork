package com.company.oop.taskmanagement.models.items.contracts;

import com.company.oop.taskmanagement.models.contracts.Identifiable;
import com.company.oop.taskmanagement.models.contracts.Person;
import com.company.oop.taskmanagement.models.items.enums.Priority;
import com.company.oop.taskmanagement.models.items.enums.Size;
import com.company.oop.taskmanagement.models.items.enums.Status;

import java.util.List;

public interface Story extends Identifiable {
    int getId();
    String getTitle();
    String getDescription();
    Priority getPriority();
    String getBelongsToBoard();
    Size getSize();
    Status getStatus();
    String getAssignee();
    List<Comments> getComments();
    List<History> getHistory();
    void setStoryComment(Person author, String message);
    void setPriority(Priority priority);
    void setSize(Size size);
    void setStatus(Status status);
    void setAssignee(String assignee);

}
