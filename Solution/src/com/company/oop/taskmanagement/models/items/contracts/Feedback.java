package com.company.oop.taskmanagement.models.items.contracts;

import com.company.oop.taskmanagement.models.contracts.Identifiable;
import com.company.oop.taskmanagement.models.contracts.Person;
import com.company.oop.taskmanagement.models.items.enums.FeedbackStatus;

import java.util.List;

public interface Feedback extends Identifiable {
    String getDescription();
    List<History> getHistory();
    List<Comments> getComments();
    String getTitle();
    FeedbackStatus getStatus();
    int getRating();
    String getBelongsToBoard();
    void setRating(int rating);
    void setFeedbackStatus(FeedbackStatus feedbackStatus);
    void setFeedbackComment(Person author, String message);
}
