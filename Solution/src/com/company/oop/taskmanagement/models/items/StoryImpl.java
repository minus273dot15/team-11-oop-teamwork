package com.company.oop.taskmanagement.models.items;

import com.company.oop.taskmanagement.models.contracts.Person;
import com.company.oop.taskmanagement.models.items.contracts.Comments;
import com.company.oop.taskmanagement.models.items.contracts.History;
import com.company.oop.taskmanagement.models.items.contracts.Story;
import com.company.oop.taskmanagement.models.items.enums.Priority;
import com.company.oop.taskmanagement.models.items.enums.Size;
import com.company.oop.taskmanagement.models.items.enums.Status;

import java.util.ArrayList;
import java.util.List;

import static com.company.oop.taskmanagement.utils.ColorScheme.ANSI_RESET;
import static com.company.oop.taskmanagement.utils.ColorScheme.ANSI_YELLOW;

public class StoryImpl extends TaskImpl implements Story {


    private final int id;
    private Priority priority;
    private Size size;
    private Status status;
    private String assignee, belongsToBoard;
    private List<Comments> comments = new ArrayList<>();

    public StoryImpl(int id, String title, String description, Priority priority, Size size, Status status, String assignee, String belongsToBoard) {
        super(id, title, description, belongsToBoard);
        this.id = id; // с времето ще бъде премахнато, след като направим само да сетва id-тата като създава съответните обекти.
        this.belongsToBoard = belongsToBoard;
        setPriority(priority);
        this.size = size;
        this.status = status;
        this.assignee = "Not assigned";
    }

    @Override
    public String getBelongsToBoard() {
        return belongsToBoard;
    }

    @Override
    public int getId() {
        return id;
    }

    @Override
    public String getTitle() {
        return title;
    }

    @Override
    public String getDescription() {
        return description;
    }

    public void setPriority(Priority priority) {
        this.priority = priority;
    }

    public void setSize(Size size) { this.size = size; }

    public void setStatus(Status status) { this.status = status; }

    @Override
    public Priority getPriority() {
        return priority;
    }

    @Override
    public Size getSize() {
        return size;
    }

    @Override
    public Status getStatus() {
        return status;
    }

    @Override
    public String getAssignee() {
        return assignee;
    }

        @Override
    public List<Comments> getComments() {
        return null;
    }

    @Override
    public List<History> getHistory() {
        return null;
    }

    @Override
    public void setAssignee(String assignee) { this.assignee = assignee; }

    @Override
    public void setStoryComment(Person author, String message) {
        Comments comment = new CommentsImpl(author, message);
        comments.add(comment);
    }


    @Override
    public String getAsString() {
        StringBuilder resultSet = new StringBuilder();
        resultSet.append("---- Story Entity Listing ----\n");
        resultSet.append(ANSI_YELLOW); resultSet.append("StoryID:              "); resultSet.append(ANSI_RESET); resultSet.append(getId());resultSet.append("\n");
        resultSet.append(ANSI_YELLOW); resultSet.append("Story title:          "); resultSet.append(ANSI_RESET); resultSet.append(getTitle());resultSet.append("\n");
        resultSet.append(ANSI_YELLOW); resultSet.append("Story description:    "); resultSet.append(ANSI_RESET); resultSet.append(getDescription());resultSet.append("\n");
        resultSet.append(ANSI_YELLOW); resultSet.append("Story priority:       "); resultSet.append(ANSI_RESET); resultSet.append(getPriority());resultSet.append("\n");
        resultSet.append(ANSI_YELLOW); resultSet.append("Story size:           "); resultSet.append(ANSI_RESET); resultSet.append(getSize());resultSet.append("\n");
        resultSet.append(ANSI_YELLOW); resultSet.append("Story status:         "); resultSet.append(ANSI_RESET); resultSet.append(getStatus());resultSet.append("\n");
        resultSet.append(ANSI_YELLOW); resultSet.append("Story assignee:       "); resultSet.append(ANSI_RESET); resultSet.append(getAssignee());resultSet.append("\n");
        resultSet.append(ANSI_YELLOW); resultSet.append("Belongs to board:     "); resultSet.append(ANSI_RESET); resultSet.append(getBelongsToBoard());resultSet.append("\n");
        if (!comments.isEmpty()) {
            for (Comments comment : getComments())
            {
                resultSet.append("*** Comment for this Story *** \n");
                resultSet.append(ANSI_YELLOW); resultSet.append("Comment author:       "); resultSet.append(ANSI_RESET); resultSet.append(comment.getAuthor());resultSet.append("\n");
                resultSet.append(ANSI_YELLOW); resultSet.append("Comment content:      "); resultSet.append(ANSI_RESET); resultSet.append(comment.getMessage());resultSet.append("\n");
                resultSet.append("************************************");
            }
        }
        return resultSet.toString();
    }

    @Override
    public String toString() {
        return getAsString();
    }
}
