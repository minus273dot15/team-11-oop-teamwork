package com.company.oop.taskmanagement.models.items.enums;

public enum Size {
    SMALL,MEDIUM,LARGE;
    @Override
    public String toString() {
        switch (this) {
            case LARGE:
                return "LARGE";
            case MEDIUM:
                return "MEDIUM";
            case SMALL:
                return "SMALL";
            default:
                throw new IllegalArgumentException();
        }
    }
}
