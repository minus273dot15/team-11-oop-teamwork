package com.company.oop.taskmanagement.models.items.enums;

public enum Severity {

    CRITICAL, MAJOR, LOW
    ;
    @Override
    public String toString() {
        switch (this) {
            case CRITICAL:
                return "CRITICAL";
            case MAJOR:
                return "MAJOR";
            case LOW:
                return "LOW";
            default:
                throw new IllegalArgumentException();
        }
    }
}
