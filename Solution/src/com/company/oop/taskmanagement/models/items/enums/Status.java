package com.company.oop.taskmanagement.models.items.enums;

public enum Status {
    NOTDONE, INPROGRESS, DONE
;
    @Override
    public String toString() {
        switch (this) {
            case NOTDONE:
                return "NOTDONE";
            case INPROGRESS:
                return "INPROGRESS";
            case DONE:
                return "DONE";
            default:
                throw new IllegalArgumentException();
        }
    }
}
