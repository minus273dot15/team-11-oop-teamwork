package com.company.oop.taskmanagement.models.items.enums;

public enum BugsStatus {
    ACTIVE, FIXED
    ;
    @Override
    public String toString() {
        switch (this) {
            case ACTIVE:
                return "ACTIVE";
            case FIXED:
                return "FIXED";
            default:
                throw new IllegalArgumentException();
        }
    }
}
