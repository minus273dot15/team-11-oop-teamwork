package com.company.oop.taskmanagement.models.items;

import com.company.oop.taskmanagement.models.contracts.Person;
import com.company.oop.taskmanagement.models.items.contracts.Bugs;
import com.company.oop.taskmanagement.models.items.contracts.Comments;
import com.company.oop.taskmanagement.models.items.contracts.History;
import com.company.oop.taskmanagement.models.items.enums.BugsStatus;
import com.company.oop.taskmanagement.models.items.enums.Priority;
import com.company.oop.taskmanagement.models.items.enums.Severity;

import java.util.ArrayList;
import java.util.List;

import static com.company.oop.taskmanagement.utils.ColorScheme.ANSI_RESET;
import static com.company.oop.taskmanagement.utils.ColorScheme.ANSI_YELLOW;

public class BugsImpl extends TaskImpl implements Bugs {

    private final int id;
    private Priority priority;
    private Severity severity;
    private BugsStatus status;
    private String assignee;
    private String belongsToBoard;

    private List<Comments> comments = new ArrayList<>();

    private List<String> stepsToReproduce; // последно добавено

    public BugsImpl(int id, String title, String description, Priority priority,Severity severity, BugsStatus status, String assignee, List<String> stepsToReproduce, String belongsToBoard) { //List<String> stepsToReproduce последно добавено
        super(id, title, description, belongsToBoard);
        this.id = id;
        this.priority = priority;
        this.severity = severity;
        this.status = status;
        this.assignee = "Not assigned";
//        this.comments = new ArrayList<Comments>();
//        this.changeHistory = new ArrayList<History>();
        this.stepsToReproduce = new ArrayList<>(); //последно добавено
    }


    @Override
    public Severity getSeverity() {
        return severity;
    }
    @Override
    public int getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public Priority getPriority() {
        return priority;
    }

    @Override
    public BugsStatus getStatus() {
        return status;
    }

    @Override
    public String getAssignee() {
        return assignee;
    }

    @Override
    public List<String> getStepsToReproduce() {
        return stepsToReproduce = new ArrayList<>();
    }

    @Override
    public void setBugComment(Person author, String message) {
        Comments comment = new CommentsImpl(author, message);
        comments.add(comment);
    }

    @Override
    public String getAsString() {
        StringBuilder resultSet = new StringBuilder();
        resultSet.append("---- Bug Entity Listing ----\n");
        resultSet.append(ANSI_YELLOW); resultSet.append("BugID:                "); resultSet.append(ANSI_RESET); resultSet.append(getId());resultSet.append("\n");
        resultSet.append(ANSI_YELLOW); resultSet.append("Bug title:            "); resultSet.append(ANSI_RESET); resultSet.append(getTitle());resultSet.append("\n");
        resultSet.append(ANSI_YELLOW); resultSet.append("Bug description:      "); resultSet.append(ANSI_RESET); resultSet.append(getDescription());resultSet.append("\n");
        resultSet.append(ANSI_YELLOW); resultSet.append("Bug priority:         "); resultSet.append(ANSI_RESET); resultSet.append(getPriority());resultSet.append("\n");

        resultSet.append(ANSI_YELLOW); resultSet.append("Bug severity:         "); resultSet.append(ANSI_RESET); resultSet.append(getSeverity());resultSet.append("\n");
        resultSet.append(ANSI_YELLOW); resultSet.append("Bug status:           "); resultSet.append(ANSI_RESET); resultSet.append(getStatus());resultSet.append("\n");
        resultSet.append(ANSI_YELLOW); resultSet.append("Bug assignee:         "); resultSet.append(ANSI_RESET); resultSet.append(getAssignee());resultSet.append("\n");
        // тук трябва ли да добавя List-a? ---> Да, изтряскай го с един fore, ако наистина е List<String>, виж ред 100 до 106
        resultSet.append(ANSI_YELLOW); resultSet.append("Bug to board:         "); resultSet.append(ANSI_RESET); resultSet.append(getBelongsToBoard());resultSet.append("\n");
        if (!comments.isEmpty()) {
            for (Comments comment : getComments())
            {
                resultSet.append("*** Comment for this bug *** \n");
                resultSet.append(ANSI_YELLOW); resultSet.append("Comment author:       "); resultSet.append(ANSI_RESET); resultSet.append(comment.getAuthor());resultSet.append("\n");
                resultSet.append(ANSI_YELLOW); resultSet.append("Comment content:      "); resultSet.append(ANSI_RESET); resultSet.append(comment.getMessage());resultSet.append("\n");
                resultSet.append("************************************");
            }
        }
        return resultSet.toString();
    }


    @Override
    public String toString() {return getAsString();}

    @Override
    public List<Comments> getComments() {
        return null;
    }

    @Override
    public List<History> getHistory() {
        return null;
    }

    @Override
    public String getBelongsToBoard() {
        return belongsToBoard;
    }

    public void setPriority(Priority priority) {
        this.priority = priority;
    }

    public void setSeverity(Severity severity) {
        this.severity = severity;
    }

    public void setStatus(BugsStatus status) {
        this.status = status;
    }

    public void setAssignee(String assignee) {this.assignee = assignee;}
}
