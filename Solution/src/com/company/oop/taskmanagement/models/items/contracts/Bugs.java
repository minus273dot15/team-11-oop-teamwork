package com.company.oop.taskmanagement.models.items.contracts;

import com.company.oop.taskmanagement.models.contracts.Identifiable;
import com.company.oop.taskmanagement.models.contracts.Person;
import com.company.oop.taskmanagement.models.items.enums.BugsStatus;
import com.company.oop.taskmanagement.models.items.enums.Priority;
import com.company.oop.taskmanagement.models.items.enums.Severity;

import java.util.List;

public interface Bugs extends Identifiable {
    int getId();

    String getTitle();

    String getBelongsToBoard();

    String getDescription();

    Priority getPriority();

    Severity getSeverity();

    BugsStatus getStatus();

    String getAssignee();

    List<Comments> getComments();

    List<History> getHistory();

    List<String> getStepsToReproduce();

    void setPriority(Priority priority);
    void setSeverity(Severity severity);
    void setStatus(BugsStatus status);
    void setAssignee(String assignee);
    void setBugComment(Person author, String message);
}
