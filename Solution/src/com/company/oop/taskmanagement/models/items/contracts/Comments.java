package com.company.oop.taskmanagement.models.items.contracts;

import com.company.oop.taskmanagement.models.contracts.Person;
import com.company.oop.taskmanagement.models.contracts.Printable;

public interface Comments extends Printable {
    String getMessage();
    Person getAuthor();
    String getAsString();
}
