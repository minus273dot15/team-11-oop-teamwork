package com.company.oop.taskmanagement.models.items.contracts;

import com.company.oop.taskmanagement.models.contracts.Identifiable;
import com.company.oop.taskmanagement.models.contracts.Person;
import com.company.oop.taskmanagement.models.contracts.Printable;

import java.util.List;

public interface Task extends Printable, Identifiable, Commentable {
    List<Comments> getComments();

    List<History> getHistory();

    String getBelongsToBoard();

    void addComment(Person author, String message);

    void removeComment(int index);
}
