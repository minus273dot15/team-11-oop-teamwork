package com.company.oop.taskmanagement.models.items;

import com.company.oop.taskmanagement.models.contracts.Person;
import com.company.oop.taskmanagement.models.items.contracts.Comments;
import com.company.oop.taskmanagement.models.items.contracts.Feedback;
import com.company.oop.taskmanagement.models.items.contracts.History;
import com.company.oop.taskmanagement.models.items.enums.FeedbackStatus;
import com.company.oop.taskmanagement.utils.ValidationHelper;

import java.util.ArrayList;
import java.util.List;

import static com.company.oop.taskmanagement.utils.ColorScheme.*;

public class FeedbackImpl extends TaskImpl implements Feedback {

    private int rating;
    String belongsToBoard;
    private List<Comments> comments = new ArrayList<>();
    private List<History> histories = new ArrayList<>();

    private FeedbackStatus feedbackStatus;

    public static final int RATING_MIN_VALUE = 0;
    public static final int RATING_MAX_VALUE = 10;
    public static final String RATING_ERR_MSG = "The feedback rating should be "
            + RATING_MIN_VALUE + " minimum and"
            + RATING_MAX_VALUE + " maximum.";

    public FeedbackImpl(int id, String title, String description, int rating, FeedbackStatus status, String belongsToBoard) {
        super(id, title, description, belongsToBoard);
        setRating(rating);
        this.feedbackStatus = FeedbackStatus.NEW;//On create status should be NEW for all feedbacks - logicz!
        setBelongsToBoard(belongsToBoard);
    }

    private void setBelongsToBoard(String belongsToBoard) {
        this.belongsToBoard = belongsToBoard;
    }

    public String getBelongsToBoard() {
        return belongsToBoard;
    }

    public void setRating(int rating) {
        //assuming rating is from 0 to 10? Should have constraints
        ValidationHelper.validateValueInRange(rating, RATING_MIN_VALUE, RATING_MAX_VALUE, RATING_ERR_MSG);
        this.rating = rating;
    }

    public void setFeedbackStatus(FeedbackStatus feedbackStatus) {
        this.feedbackStatus = feedbackStatus;
    }

    @Override
    public void setFeedbackComment(Person author, String message) {
        Comments comment = new CommentsImpl(author, message);
        comments.add(comment);
    }

    public int getRating() {
        return rating;
    }
    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public String getTitle() {
        return title;
    }

    @Override
    public FeedbackStatus getStatus() {
        return feedbackStatus;
    }

     @Override
    public List<Comments> getComments() {
        return new ArrayList<>(comments);
    }

    @Override
    public List<History> getHistory() {
        return new ArrayList<>(histories);
    }


    @Override
    public String getAsString() {
        StringBuilder resultSet = new StringBuilder();
        resultSet.append("---- Feedback Entity Listing ----\n");
        resultSet.append(ANSI_YELLOW); resultSet.append("FeedbackID:           "); resultSet.append(ANSI_RESET); resultSet.append(getId());resultSet.append("\n");
        resultSet.append(ANSI_YELLOW); resultSet.append("Feedback title:       "); resultSet.append(ANSI_RESET); resultSet.append(getTitle());resultSet.append("\n");
        resultSet.append(ANSI_YELLOW); resultSet.append("Feedback rating:      "); resultSet.append(ANSI_RESET); resultSet.append(getRating());resultSet.append("\n");
        resultSet.append(ANSI_YELLOW); resultSet.append("Feedback description: "); resultSet.append(ANSI_RESET); resultSet.append(getDescription());resultSet.append("\n");
        resultSet.append(ANSI_YELLOW); resultSet.append("Feedback status:      "); resultSet.append(ANSI_RESET); resultSet.append(getStatus());resultSet.append("\n");
        resultSet.append(ANSI_YELLOW); resultSet.append("Belongs to board:     "); resultSet.append(ANSI_RESET); resultSet.append(getBelongsToBoard());resultSet.append("\n");
        if (!comments.isEmpty()) {
            for (Comments comment : getComments())
            {
                resultSet.append("*** Comment for this Feedback *** \n");
                resultSet.append(ANSI_YELLOW); resultSet.append("Comment author:       "); resultSet.append(ANSI_RESET); resultSet.append(comment.getAuthor());resultSet.append("\n");
                resultSet.append(ANSI_YELLOW); resultSet.append("Comment content:      "); resultSet.append(ANSI_RESET); resultSet.append(comment.getMessage());resultSet.append("\n");
                resultSet.append("************************************");
            }
        }
        return resultSet.toString();
    }

    @Override
    public String toString() {
        return getAsString();
    }
}
