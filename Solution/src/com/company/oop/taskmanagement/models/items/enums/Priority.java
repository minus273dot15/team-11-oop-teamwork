package com.company.oop.taskmanagement.models.items.enums;

public enum Priority {
    HIGH,MEDIUM,LOW;
    @Override
    public String toString() {
        switch (this) {
            case HIGH:
                return "HIGH";
            case MEDIUM:
                return "MEDIUM";
            case LOW:
                return "LOW";
            default:
                throw new IllegalArgumentException();
        }
    }
}


