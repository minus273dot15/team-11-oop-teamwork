package com.company.oop.taskmanagement.models.items;


import com.company.oop.taskmanagement.models.contracts.Person;
import com.company.oop.taskmanagement.models.items.contracts.Comments;

import static com.company.oop.taskmanagement.utils.ColorScheme.ANSI_RESET;
import static com.company.oop.taskmanagement.utils.ColorScheme.ANSI_YELLOW;

public class CommentsImpl implements Comments {

    private String message;
    private Person author;

    public CommentsImpl(Person author, String message) {
        setMessage(message);
        setAuthor(author);
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public String getMessage() {
        return message;
    }

    public void setAuthor(Person author) {
        this.author = author;
    }

    @Override
    public Person getAuthor() {
        return author;
    }

    @Override
    public String getAsString() {
        return "---- Comment ----\n" +
                ANSI_YELLOW + "Author:               " + ANSI_RESET + getAuthor() + "\n" +
                ANSI_YELLOW + "Message:              " + ANSI_RESET + getMessage() + "\n";
    }

    @Override
    public String toString() {
        return getAsString();
    }

}
