package com.company.oop.taskmanagement.models.items;

import com.company.oop.taskmanagement.models.contracts.Printable;
import com.company.oop.taskmanagement.models.items.contracts.History;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class HistoryImpl implements History, Printable {
    private final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MMMM-yyyy HH:mm:ss");

    private final String description;

    private final LocalDateTime timestamp;

    public HistoryImpl(String description) {
        if (description.isEmpty()) {
            throw new IllegalArgumentException("Description cannot be empty");
        }
        this.description = description;
        this.timestamp = LocalDateTime.now();
    }

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public String toString() { return getAsString(); }

    public String getAsString() {
        return String.format("[%s] %s", timestamp.format(formatter), description);
    }
}
