package com.company.oop.taskmanagement.models.items.contracts;

import java.util.List;

public interface Commentable {

    List<Comments> getComments();

}
