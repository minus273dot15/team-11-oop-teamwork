package com.company.oop.taskmanagement.models.items.contracts;

public interface History {
    String getDescription();
}