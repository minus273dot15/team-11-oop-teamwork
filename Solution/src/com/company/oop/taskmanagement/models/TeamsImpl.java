package com.company.oop.taskmanagement.models;
import com.company.oop.taskmanagement.models.contracts.Board;
import com.company.oop.taskmanagement.models.contracts.Person;
import com.company.oop.taskmanagement.models.contracts.Team;
import com.company.oop.taskmanagement.utils.ValidationHelper;

import java.util.ArrayList;
import java.util.List;

import static com.company.oop.taskmanagement.utils.ColorScheme.ANSI_RESET;
import static com.company.oop.taskmanagement.utils.ColorScheme.ANSI_YELLOW;

public class TeamsImpl implements Team {

    private String name;
    private List<Person> members;
    private List<Board> boards;
    private final int id;

    public static final int TEAM_NAME_MIN_LENGTH = 5;
    public static final int TEAM_NAME_MAX_LENGTH = 15;
    public static final String TEAM_NAME_ERROR_MESSAGE = "The TeamName cannot be less than 5 or more than 15 symbols.";


    public TeamsImpl(int id, String name, List<Person> members, List<Board> boards) {
        this.id = id;
        setName(name);
        this.members = members;
        this.boards = boards;
    }

    public void setName(String name) {
        ValidationHelper.validateStringLength(name,TEAM_NAME_MIN_LENGTH,TEAM_NAME_MAX_LENGTH, TEAM_NAME_ERROR_MESSAGE);
        this.name = name;
    }

    public void setMembers(List<Person> members) {
        this.members = members;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public List<Person> getMembers() {
        return members = new ArrayList<>();
    }

    @Override
    public List<Board> getBoards() {
        return boards = new ArrayList<>();
    }

    @Override
    public String getAsString() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("---- Team Entity Listing ----\n");
        stringBuilder.append(ANSI_YELLOW + "Team ID:              " + ANSI_RESET + getId() + "\n");
        stringBuilder.append(ANSI_YELLOW + "Team name:            " + ANSI_RESET + getName() + "\n");
        if (members.isEmpty())  stringBuilder.append(ANSI_YELLOW + "Team members:         " + ANSI_RESET + "No members\n");
        else {
            stringBuilder.append(ANSI_YELLOW + "Team members:         " + ANSI_RESET);
            getMembers().stream().map(person -> person.getName() + "; ").forEach(stringBuilder::append);
        }
        if (boards.isEmpty())  stringBuilder.append(ANSI_YELLOW + "Team Boards:         " + ANSI_RESET + "No boards\n");
        else {
            stringBuilder.append(ANSI_YELLOW + "Team Boards:         " + ANSI_RESET);
            getBoards().stream().map(board -> board.getName() + "; ").forEach(stringBuilder::append);
        }
        return  stringBuilder.toString();
    }

    public String toString(){ return getAsString();}

    @Override
    public int getId() {
        return id;
    }
}
